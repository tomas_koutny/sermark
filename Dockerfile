FROM ubuntu:16.04

# add the same user as used for development on host to make everything easier
RUN useradd tomas

# install packages needed to run bootstrap script
RUN apt-get -y update && apt-get install -y git vim autoconf automake libtool curl make g++ unzip

WORKDIR /home/tomas/projects/protobuf
RUN git clone https://github.com/google/protobuf.git .
RUN ./autogen.sh
RUN ./configure
RUN make
RUN make check
RUN make install
RUN ldconfig

RUN apt-get install -y cmake gdb
RUN apt-get install -y libboost-dev libboost-serialization-dev libssl-dev libcurl4-openssl-dev libcereal-dev
WORKDIR /home/tomas/projects
# switch initial user
USER tomas
