#include <string>
#include <iostream>
#include <chrono>
#include <thread>
#include "request/BidRequestParams.h"
#include "serializers/protobuf/ProtobufSerializer.h"
#include "serializers/boost/BoostSerializer.h"
#include "serializers/cereal/CerealSerializer.h"

std::chrono::duration<double, std::milli> test(std::function<void(int)> callback)
{
   constexpr int signleThreadIterationCount = 10000;
   constexpr int threadCount = 40;

   auto start = std::chrono::steady_clock::now();

   std::vector<std::thread> threads;
   for (int i = 0; i < threadCount; ++i)
   {
      threads.push_back(std::thread(callback, signleThreadIterationCount));
   }

   for (auto& th : threads)
   {
      th.join();
   }

   auto end = std::chrono::steady_clock::now();
   auto diff = end - start;

   return std::chrono::duration<double, std::milli>(diff);
}

int main()
{
   std::cout << "protobuf: " << test(ProtobufSerializer::perform).count() << " ms" << std::endl;
   std::cout << "boost: " << test(BoostSerializer::perform).count() << " ms" << std::endl;
   std::cout << "cereal: " << test(CerealSerializer::perform).count() << " ms" << std::endl;

   return 0;
}
