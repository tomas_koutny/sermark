#include "CerealSerializer.h"
#include "../../request/BidRequestParams.h"

#include <cereal/types/array.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/types/set.hpp>
#include <cereal/types/string.hpp>
#include <cereal/types/utility.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/archives/binary.hpp>
#include <sstream>

namespace cereal
{
   template<class Archive>
   void serialize(Archive & archive, BidRequestParams& br)
   {
      archive(
      br.m_primarySSP,
      br.m_bid_id,
      br.m_bidHash,
      br.m_app,
      br.m_site,
      br.m_device,
      br.m_user,
      br.m_blocked_advertisers,
      br.m_impressions,
      br.m_restrictions,
      br.m_staticIpHitExpected,
      br.m_blisPublisherId,
      br.m_tmax,
      br.m_sspInfo,
      br.m_video,
      br.m_smartPinConfidence,
      br.getRegulations(),
      br.m_pathRetFuncIds,
      br.m_pathRetargetingQueryDone,
      br.m_publisherGloballyBlacklisted,
      br.m_auctioneerUrl,
      br.m_applicableDPs,
      br.m_connId
      );
   }

   template<class Archive>
   void serialize(Archive & archive, App& app)
   {
      archive(
      app.m_app_name,
      app.m_app_domain_name,
      app.m_bundle,
      app.m_iab_categories,
      app.m_appStoreURL,
      app.m_version,
      app.m_paid
      );
   }

   template<class Archive>
   void serialize(Archive & archive, Site& site)
   {
      archive(
         site.m_site_name,
         site.m_domain_name,
         site.m_publisher_name,
         site.m_publisher_domain,
         site.m_extracted_domain,
         site.m_iab_categories,
         site.m_pageURL
      );
   }

   template<class Archive>
   void serialize(Archive & archive, IABCategory& category)
   {
      archive(
         category.m_bitset
      );
   }

   template<class Archive>
   void serialize(Archive & archive, Device& device)
   {
      archive(
         device.m_device_id,
         device.m_device_ip,
         device.m_regionalInfoSSP,
         device.m_device_user_agent,
         device.m_device_dpid,
         device.m_deviceIDType,
         device.m_deviceIDHashMethod,
         device.m_connectionType,
         device.m_carrier,
         device.m_jsSupported,
         device.m_device_os,
         device.m_device_type,
         device.m_coordinates,
         device.m_ip2GeoCoordinates
      );
   }

   template<class Archive>
   void serialize(Archive & archive, RegionalInfo& info)
   {
      archive(
      info.countryAlpha3,
      info.regionOrState,
      info.city,
      info.zip,
      info.metro
      );
   }

   template<class Archive>
   void serialize(Archive & archive, OptionalCoordinates& coordinates)
   {
      archive(
         coordinates.m_latitude,
         coordinates.m_longitude,
         coordinates.m_source,
         coordinates.m_isSet
      );
   }

   template<class Archive>
   void serialize(Archive & archive, User& user)
   {
      archive(
         user.m_uid
      );
   }

   template<class Archive>
   void serialize(Archive & archive, Impression& impression)
   {
      archive(
         impression.m_impression_id,
         impression.m_allowed_dimensions,
         impression.m_blockedAdTypes,
         impression.m_blockedCreativeAttributes,
         impression.m_bid_floor,
         impression.m_allowedAppAdTypes,
         impression.m_allowedSiteAdTypes,
         impression.m_blisOptimisationId,
         impression.m_onlySSL,
         impression.m_privateDeals,
         impression.m_privateDealIndex,
         impression.m_isInterstitial
      );
   }

   template<class Archive>
   void serialize(Archive & archive, Impression::PrivateDeal& deal)
   {
      archive(
         deal.m_id,
         deal.m_bidFloor
      );
   }

   template<class Archive>
   void serialize(Archive & archive, Restrictions& restrictions)
   {
      archive(
         restrictions.m_blocked_iab_categories
      );
   }

   template<class Archive>
   void serialize(Archive & archive, BlisIdSHA1& id)
   {
      archive(
         id.m_id
      );
   }

   template<class Archive>
   void serialize(Archive & archive, SSPInfo& sspInfo)
   {
      archive(
         sspInfo.m_id
      );
   }

   template<class Archive>
   void serialize(Archive & archive, blis::Video& video)
   {
      archive(
         video.m_min_duration,
         video.m_max_duration,
         video.m_max_skippable_duration,
         video.m_start_delay,
         video.m_skip_options,
         video.m_allowedVASTProtocolsBitSet,
         video.m_playbackMethods,
         video.m_width,
         video.m_height,
         video.m_position,
         video.m_linearity
      );
   }

   template<class Archive>
   void serialize(Archive & archive, Regulations& regs)
   {
      archive(
         regs.m_regulations
      );
   }
}

void CerealSerializer::perform(int count)
{
   BidRequestParams br = util::createBidRequest();
   std::string msg;
   for (int i = 0; i < count; ++i)
   {
      msg = serialize(br);

      deserialize(msg);
   }
}

std::string CerealSerializer::serialize(const BidRequestParams& br)
{
   std::stringstream ss;
   cereal::BinaryOutputArchive oarchive(ss);
   oarchive(br);
   return ss.str();
}

BidRequestParams CerealSerializer::deserialize(const std::string& msg)
{
   BidRequestParams br;
   std::stringstream ss(msg);
   cereal::BinaryInputArchive iarchive(ss);
   iarchive(br);

   return br;
}