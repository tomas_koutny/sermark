#pragma once

#include <string>

class BidRequestParams;

class ProtobufSerializer
{
public:
   static void perform(int count);

   static std::string serialize(const BidRequestParams& br);

   static BidRequestParams deserialize(const std::string& msg);
};