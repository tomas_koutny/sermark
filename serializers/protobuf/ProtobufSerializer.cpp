#include "ProtobufSerializer.h"

#include "s.pb.h"
#include "../../request/BidRequestParams.h"

void ProtobufSerializer::perform(int count)
{
   GOOGLE_PROTOBUF_VERIFY_VERSION;
   BidRequestParams br = util::createBidRequest();
   std::string msg;
   for (int i = 0; i < count; ++i)
   {
      msg = serialize(br);

      deserialize(msg);
   }
}

std::string ProtobufSerializer::serialize(const BidRequestParams& br)
{
   std::string msg;

   brp::BidRequestParams pbr;
   pbr.set_primarysspid(br.getPrimarySSPId());
   pbr.set_bidid(br.m_bid_id);
   pbr.set_bidhash(br.m_bidHash);
   pbr.set_userid(br.m_user->m_uid);
   pbr.set_staticiphitexpected(br.m_staticIpHitExpected);
   pbr.set_blispublishersha(std::string(br.m_blisPublisherId.m_id.begin(), br.m_blisPublisherId.m_id.end()));
   pbr.set_tmax(br.m_tmax);
   pbr.set_secondarysspid(br.getSecondaryId());
   pbr.set_smartpinconfidence(br.m_smartPinConfidence);
   pbr.set_regulationsbitfield(br.m_regulations.m_regulations);
   pbr.set_pathretargetingquerydone(br.m_pathRetargetingQueryDone);
   pbr.set_publishergloballyblacklisted(br.m_publisherGloballyBlacklisted);
   pbr.set_auctioneerurl(br.m_auctioneerUrl);
   pbr.set_connectionid(br.m_connId);

   for (const auto& ba : br.m_blocked_advertisers)
   {
      pbr.add_blockedadvertisers(ba);
   }

   for (const auto& cat : br.m_restrictions.GetRestrictions())
   {
      pbr.add_blockediabcategories(cat.toString());
   }

   for (RetargetingFunctionId rfid : br.m_pathRetFuncIds)
   {
      pbr.add_pathretargetingfunctionids(rfid);
   }

   for (DemandPartnerId dpid : br.m_applicableDPs)
   {
      pbr.add_applicabledpids(dpid);
   }

   {
      brp::App* app = pbr.mutable_app();
      auto& brapp = *br.m_app;
      app->set_name(brapp.m_app_name);
      app->set_domainname(brapp.m_app_domain_name);
      app->set_bundle(brapp.m_bundle);
      app->set_appstoreurl(brapp.m_appStoreURL);
      app->set_version(brapp.m_version);
      app->set_paid(brapp.m_paid > 0);

      for (const auto& cat : brapp.m_iab_categories)
      {
         app->add_iabcategories(cat.toString());
      }
   }

   {
      brp::Device* device = pbr.mutable_device();
      auto& brdevice = *br.m_device;
      device->set_id(brdevice.m_device_id);
      device->set_ipaddress(brdevice.m_device_ip);
      device->set_useragent(brdevice.m_device_user_agent);
      device->set_dpid(brdevice.m_device_dpid);
      device->set_deviceidtype(brdevice.m_deviceIDType);
      device->set_deviceidhashmethod(brdevice.m_deviceIDHashMethod);
      device->set_connectiontype(brdevice.m_connectionType);
      device->set_carrier(brdevice.m_carrier);
      device->set_jssupported(brdevice.m_jsSupported > 0);
      device->set_operatingsystem(brdevice.m_device_os);
      device->set_devicetype(brdevice.m_device_type);

      {
         brp::RegionalInfo* ri = device->mutable_regionalinfossp();
         auto& brri = brdevice.m_regionalInfoSSP;
         ri->set_countryalpha3(brri.countryAlpha3);
         ri->set_regionorstate(brri.regionOrState);
         ri->set_city(brri.city);
         ri->set_zip(brri.zip);
         ri->set_metro(brri.metro);
      }

      {
         brp::Coordinates* coords = device->mutable_coordinates();
         auto& brcoords = brdevice.m_coordinates;
         coords->set_set(brcoords.m_isSet);
         coords->set_latitude(brcoords.m_latitude);
         coords->set_longitude(brcoords.m_longitude);
         coords->set_source(brcoords.m_source);
      }

      {
         brp::Coordinates* ip2geoCoords = device->mutable_ip2geocoordinates();
         auto& brip2geocoords = brdevice.m_ip2GeoCoordinates;
         ip2geoCoords->set_set(brip2geocoords.m_isSet);
         ip2geoCoords->set_latitude(brip2geocoords.m_latitude);
         ip2geoCoords->set_longitude(brip2geocoords.m_longitude);
         ip2geoCoords->set_source(brip2geocoords.m_source);
      }
   }

   for (auto& brimpPtr : br.m_impressions)
   {
      auto& brimp = *brimpPtr;
      brp::Impression* imp = pbr.add_impressions();
      imp->set_id(brimp.m_impression_id);
      imp->set_blockedcreativeattributes(brimp.m_blockedCreativeAttributes);
      imp->set_bidfloor(brimp.m_bid_floor);
      imp->set_blisoptimisationid(brimp.m_blisOptimisationId);
      imp->set_sslonly(brimp.m_onlySSL);
      imp->set_privatedealindex(brimp.m_privateDealIndex);
      imp->set_isinterstitial(brimp.m_isInterstitial);

      for (auto& brdim : brimp.m_allowed_dimensions)
      {
         brp::Dimensions* dim = imp->add_alloweddimensions();
         dim->set_width(brdim.first);
         dim->set_height(brdim.second);
      }

      for (auto& at : brimp.m_blockedAdTypes)
      {
         imp->add_blockedadtypes(at);
      }

      for (auto& at : brimp.m_allowedAppAdTypes)
      {
         imp->add_allowedappadtypes(at);
      }

      for (auto& at : brimp.m_allowedSiteAdTypes)
      {
         imp->add_allowedsiteadtypes(at);
      }

      for (auto& brpd : brimp.m_privateDeals)
      {
         brp::PrivateDeal* pd = imp->add_privatedeals();
         pd->set_id(brpd.m_id);
         pd->set_bidfloor(brpd.m_bidFloor);
      }
   }

   return pbr.SerializeAsString();
}

BidRequestParams ProtobufSerializer::deserialize(const std::string& msg)
{
   brp::BidRequestParams pbr;
   pbr.ParseFromString(msg);

   BidRequestParams br;

   br.m_primarySSP = static_cast<SSPId>(pbr.primarysspid());
   br.m_bid_id = pbr.bidid();
   br.m_bidHash = pbr.bidhash();
   br.m_staticIpHitExpected = pbr.staticiphitexpected();
   br.m_tmax = pbr.tmax();
   br.m_sspInfo.m_id = static_cast<SSPId>(pbr.secondarysspid());
   br.m_smartPinConfidence = pbr.smartpinconfidence();
   br.m_regulations.m_regulations = pbr.regulationsbitfield();
   br.m_pathRetargetingQueryDone = pbr.pathretargetingquerydone();
   br.m_publisherGloballyBlacklisted = pbr.publishergloballyblacklisted();
   br.m_auctioneerUrl = pbr.auctioneerurl();
   br.m_connId = pbr.connectionid();

   {
      const std::string& str = pbr.blispublishersha();
      std::copy(str.begin(), str.end(), br.m_blisPublisherId.m_id.data());
   }

   for (const std::string& ba : pbr.blockedadvertisers())
   {
      br.m_blocked_advertisers.insert(ba);
   }

   for (const std::string& cat : pbr.blockediabcategories())
   {
      br.m_restrictions.m_blocked_iab_categories.insert(IABCategory::fromString(cat));
   }

   for (int32_t rfid : pbr.pathretargetingfunctionids())
   {
      br.m_pathRetFuncIds.insert(rfid);
   }

   for (int32_t dpid : pbr.applicabledpids())
   {
      br.m_applicableDPs.insert(static_cast<DemandPartnerId>(dpid));
   }

   br.m_user.reset(new User());
   br.m_user->m_uid = pbr.userid();

   {
      br.m_app.reset(new App());
      auto& brapp = *br.m_app;
      const auto& app = pbr.app();
      brapp.m_app_name = app.name();
      brapp.m_app_domain_name = app.domainname();
      brapp.m_bundle = app.bundle();
      brapp.m_appStoreURL = app.appstoreurl();
      brapp.m_version = app.version();
      brapp.m_paid = app.paid();

      for (const std::string& cat : app.iabcategories())
      {
         brapp.m_iab_categories.insert(IABCategory::fromString(cat));
      }
   }

   {
      br.m_device.reset(new Device());
      const brp::Device& device = pbr.device();
      auto& brdevice = *br.m_device;

      brdevice.m_device_id = device.id();
      brdevice.m_device_ip = device.ipaddress();
      brdevice.m_device_user_agent = device.useragent();
      brdevice.m_device_dpid = device.dpid();
      brdevice.m_deviceIDType = static_cast<Device::DeviceIDType>(device.deviceidtype());
      brdevice.m_deviceIDHashMethod = static_cast<Device::DeviceIDHashMethod>(device.deviceidhashmethod());
      brdevice.m_connectionType = static_cast<BlisConnectionType>(device.connectiontype());
      brdevice.m_carrier = device.carrier();
      brdevice.m_jsSupported = device.jssupported();
      brdevice.m_device_os = device.operatingsystem();
      brdevice.m_device_type = static_cast<Device::DeviceType>(device.devicetype());

      {
         const brp::RegionalInfo& ri = device.regionalinfossp();
         auto& brri = brdevice.m_regionalInfoSSP;
         brri.countryAlpha3 = ri.countryalpha3();
         brri.regionOrState = ri.regionorstate();
         brri.city = ri.city();
         brri.zip = ri.zip();
         brri.metro = ri.metro();
      }

      {
         const brp::Coordinates& coords = device.coordinates();
         auto& brcoords = brdevice.m_coordinates;
         brcoords.m_isSet = coords.set();
         brcoords.m_latitude = coords.latitude();
         brcoords.m_longitude = coords.longitude();
         brcoords.m_source = static_cast<LocationSource>(coords.source());
      }

      {
         const brp::Coordinates& coords = device.ip2geocoordinates();
         auto& brcoords = brdevice.m_ip2GeoCoordinates;
         brcoords.m_isSet = coords.set();
         brcoords.m_latitude = coords.latitude();
         brcoords.m_longitude = coords.longitude();
         brcoords.m_source = static_cast<LocationSource>(coords.source());
      }
   }

   for (const brp::Impression& imp : pbr.impressions())
   {
      std::unique_ptr<Impression> brimpPtr(new Impression());
      auto& brimp = *brimpPtr;
      brimp.m_impression_id = imp.id();
      brimp.m_blockedCreativeAttributes = static_cast<CreativeAttributeBitField>(imp.blockedcreativeattributes());
      brimp.m_bid_floor = imp.bidfloor();
      brimp.m_blisOptimisationId = imp.blisoptimisationid();
      brimp.m_onlySSL = imp.sslonly();
      brimp.m_privateDealIndex = imp.privatedealindex();
      brimp.m_isInterstitial = imp.isinterstitial();

      for (const brp::Dimensions& dim : imp.alloweddimensions())
      {
         brimp.m_allowed_dimensions.emplace_back(std::make_pair(dim.width(), dim.height()));
      }

      for (uint32_t at : imp.blockedadtypes())
      {
         brimp.m_blockedAdTypes.insert(static_cast<BlisAdType>(at));
      }

      for (uint32_t at : imp.allowedappadtypes())
      {
         brimp.m_allowedAppAdTypes.insert(static_cast<BlisAdType>(at));
      }

      for (uint32_t at : imp.allowedsiteadtypes())
      {
         brimp.m_allowedSiteAdTypes.insert(static_cast<BlisAdType>(at));
      }

      for (const brp::PrivateDeal& pd : imp.privatedeals())
      {
         brimp.m_privateDeals.emplace_back(pd.id(), pd.bidfloor());
      }
   }

   return br;
}