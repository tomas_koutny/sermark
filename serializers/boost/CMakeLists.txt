add_library(s_boost STATIC
   BoostSerializer
)

target_link_libraries(s_boost
   s_request
   boost_serialization
)