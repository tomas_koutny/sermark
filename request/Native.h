#pragma once

#include "BlisTypes.h"
#include "NativeCreative.h"
#include <boost/variant.hpp>
#include <exception>

class DataSlotDetail
{
public:
   DataSlotDetail(BlisNativeDataAssetType type, int length)
   : m_type(type),
     m_length(length)
   {
   }

   BlisNativeDataAssetType getType() const
   {
      return m_type;
   }

   bool hasLengthConstraint() const
   {
      return m_length > 0;
   }

   int getLength() const
   {
      return m_length;
   }

private:
   BlisNativeDataAssetType m_type;
   int m_length;
};

class ImageSlotDetail
{
public:
   ImageSlotDetail(
      BlisNativeImageAssetType type,
      unsigned int width,
      bool widthIsMin,
      unsigned int height,
      bool heightIsMin,
      ImageMimeTypeBitField mimeTypes = IMAGE_MIME_TYPE_NONE
   )
   : m_type(type),
     m_mimeTypes(mimeTypes),
     m_width(width),
     m_height(height),
     m_widthIsMin(widthIsMin),
     m_heightIsMin(heightIsMin)
   {
   }

   BlisNativeImageAssetType getType() const
   {
      return m_type;
   }

   bool hasMimeTypeConstraint() const
   {
      return m_mimeTypes != IMAGE_MIME_TYPE_NONE;
   }

   ImageMimeTypeBitField getMimeTypes() const
   {
      return m_mimeTypes;
   }

   unsigned int getWidth() const
   {
      return m_width;
   }

   unsigned int getHeight() const
   {
      return m_height;
   }

   bool isWidthMinimal() const
   {
      return m_widthIsMin;
   }

   bool isHeightMinimal() const
   {
      return m_heightIsMin;
   }

private:
   BlisNativeImageAssetType m_type;
   ImageMimeTypeBitField m_mimeTypes;
   unsigned int m_width;
   unsigned int m_height;
   bool m_widthIsMin;
   bool m_heightIsMin;
};

class NativeAssetSlot
{
public:

   NativeAssetSlot(int id, bool required, DataSlotDetail&& detail)
   : m_id(id),
     m_required(required),
     m_detail(std::move(detail))
   {}

   NativeAssetSlot(int id, bool required, ImageSlotDetail&& detail)
   : m_id(id),
     m_required(required),
     m_detail(std::move(detail))
   {}

   int getId() const
   {
      return m_id;
   }

   bool isRequired() const
   {
      return m_required;
   }

   bool isData() const
   {
      return m_detail.which() == 0;
   }

   const DataSlotDetail& getDataDetail() const
   {
      try
      {
         return boost::get<DataSlotDetail>(m_detail);
      }
      catch(...)
      {
         throw std::runtime_error("NativeAssetSlot is not a data type.");
      }
   }

   bool isImage() const
   {
      return m_detail.which() == 1;
   }

   const ImageSlotDetail& getImageDetail() const
   {
      try
      {
         return boost::get<ImageSlotDetail>(m_detail);
      }
      catch(...)
      {
         throw std::runtime_error("NativeAssetSlot is not an image type.");
      }
   }

private:
   int m_id;
   bool m_required;
   boost::variant<DataSlotDetail, ImageSlotDetail> m_detail;
};
