#pragma once

#include <stdexcept>

#include "Util.h"
#include "Video.h"


namespace AdX
{
//
// Contents of creative_attributes.txt
//
enum CreativeAttributes
{
   CreativeType_Text = 1,
   CreativeType_Image_RichMedia = 2,
   VideoType_AbodeFlashFLV = 3,
   VideoType_RealPlayer = 4,
   VideoType_Quicktime = 5,
   VideoType_WindowsMedia = 6,
   Tagging_IsTagged = 7,
   CookieTargeting_IsCookieTargeted = 8,
   UserInterestTargeting_IsUserInterestTargeted = 9,
   ExpandingDirection_Up = 13,
   ExpandingDirection_Down = 14,
   ExpandingDirection_Left = 15,
   ExpandingDirection_Right = 16,
   ExpandingDirection_UpLeft = 17,
   ExpandingDirection_UpRight = 18,
   ExpandingDirection_DownLeft = 19,
   ExpandingDirection_DownRight = 20,
   CreativeType_HTML = 21,
   CreativeType_VastVideo = 22,
   ExpandingDirection_UpOrDown = 25,
   ExpandingDirection_LeftOrRight = 26,
   ExpandingDirection_AnyDiagonal = 27,
   ExpandingAction_RolloverToExpand = 28,
   InstreamVastVideoType_VpaidFlash = 30,
   MraidType_Mraid_1_0 = 32,
   RichMediaCapabilityType_HTML4 = 33,
   RichMediaCapabilityType_Flash = 34,
   RichMediaCapabilityType_HTML5 = 39,
   RichMediaCapabilityType_HTML5_BasicSVG = 40,
   RichMediaCapabilityType_HTML5_SVGFilters = 41,
   RichMediaCapabilityType_HTML5_SVGFonts = 42,
   RichMediaCapabilityType_HTML5_LargeExpandable = 43,
   InstreamVastVideoType_SkippableInstreamVideo = 44,
   RichMediaCapabilityType_SSL = 47,
   RichMediaCapabilityType_NonSSL = 48,
   RichMediaCapabilityType_NonFlash = 50,
   RichMediaCapabilityType_Interstitial = 51
};

//
// Contents of ad_sensitive_categories.txt
//
enum AdSensitiveCategories
{
   NoSensitiveCategories = 0,
   Politics = 3,
   Dating = 4,
   Religion = 5,
   VideoGames = 7,
   RingtonesAndDownloadables = 8,
   GetRichQuick = 10,
   WeightLoss = 18,
   CosmeticProceduresAndBodyModification = 19,
   DrugsAndSupplements = 23,
   SexualAndReproductiveHealth = 24,
   ConsumerLoans = 27,
   FreeGiftsQuizzesAndSurveys = 28,
   MisleadingClaims = 29,
   BlackMagicAstrologyAndEsoteric = 30,
   ReferencesToSexAndSexuality = 31
};

//
// Contents of ad-restricted-categories.txt
//
enum RestrictedCategory
{
   RestrictedCategoryNone = 0,
   RestrictedCategoryAlcohol = 33
};

//
// AdX's ids for certain vendors/adservers that need to be declared
// in the bid response when serving an expandable banner.
// Values are taken from vendors.txt
//
enum VendorId
{
   VENDOR_FLASHTALKING = 316
};


inline CreativeAttributes ConvertFromBlisMimeType(unsigned int blisMimeType)
{
   switch (blisMimeType)
   {
   case BLIS_AD_TYPE_TEXT:
      return CreativeType_HTML;
      
   case BLIS_AD_TYPE_IMAGE:
      return CreativeType_Image_RichMedia;
      
   case BLIS_AD_TYPE_JAVASCRIPT:
      return CreativeType_Image_RichMedia;
      
   case BLIS_AD_TYPE_MRAID:
      return MraidType_Mraid_1_0;
      
   case BLIS_AD_TYPE_VAST:
      return CreativeType_VastVideo;
   }

   throw std::invalid_argument(
      "Cannot convert Blis mime_type " +
      std::to_string(blisMimeType) +
      " to an AdX creative_attribute"
   );
}

//
// Decrypt an encrypted payload in an AdX bid request.
// Taken from: https://code.google.com/p/privatedatacommunicationprotocol
//
inline std::string decryptByteArray(const std::string& ciphertext, const std::string& encryptionKey, const std::string& integrityKey)
{
   static const std::size_t SIG_SIZE = 4;
   static const std::size_t IV_SIZE = 16;

   const std::size_t outputSize = ciphertext.size() - (IV_SIZE + SIG_SIZE);
   if (outputSize > ciphertext.size())
   {
      throw std::invalid_argument("Corrupt payload");
   }
   
   std::string output;
   output.resize(outputSize);

   // The encrypted payload has a structure:
   // [IV (16 bytes)][payload (variable length)][SIGNATURE (4 bytes)]
   const char* ivPtr = ciphertext.data();
   const char* payloadPtr = ivPtr + IV_SIZE;
   const char* sigPtr = payloadPtr + outputSize;
   auto outputPtr = output.begin();

   SHA1Digest digest;
   bool extendIV = true;
   std::size_t bytesRemaining = outputSize;
   std::string iv(ivPtr, IV_SIZE);

   while (bytesRemaining > 0)
   {
      blis::SHA1(encryptionKey, iv, digest);
      
      std::size_t limit = std::min(bytesRemaining, (std::size_t)SHA1_DIGEST_SIZE);
      for (std::size_t i = 0; i < limit; ++i)
      {
         *outputPtr = *payloadPtr ^ digest[i];
         ++outputPtr;
         ++payloadPtr;
      }
            
      bytesRemaining -= limit;
      if (bytesRemaining > 0)
      {
         if (!extendIV)
         {
            char& last_byte = *iv.rbegin();
            ++last_byte;
            if (last_byte == '\0')
            {
               extendIV = true;
            }
         }
      
         if (extendIV)
         {
            extendIV = false;
            iv.push_back('\0');
         }
      }
   }

   // Verify the integrity.
   std::string check;
   check.append(output);
   check.append(ivPtr, IV_SIZE);
   
   blis::SHA1(integrityKey, check, digest);   
   if (memcmp(sigPtr, digest.data(), SIG_SIZE) != 0)
   {
      throw std::invalid_argument("Decryption integrity check failed");
   }
   
   return output;
}

enum VideoPosition
{
   ADX_VIDEO_POST_ROLL = -2,
   ADX_VIDEO_MID_ROLL = -1,
   ADX_VIDEO_PRE_ROLL = 0
};

inline blis::Video::VideoPosition convert(VideoPosition position)
{
   switch (position)
   {
   case AdX::VideoPosition::ADX_VIDEO_PRE_ROLL:
      return blis::Video::VideoPosition::BLIS_VIDEO_PRE_ROLL;
   case AdX::VideoPosition::ADX_VIDEO_MID_ROLL:
      return blis::Video::VideoPosition::BLIS_VIDEO_MID_ROLL;
   case AdX::VideoPosition::ADX_VIDEO_POST_ROLL:
      return blis::Video::VideoPosition::BLIS_VIDEO_POST_ROLL;
   }

   return blis::Video::VideoPosition::BLIS_VIDEO_POSITION_UNKNOWN;
}

// IDFA
bool buildAdvertiserId(const std::string& rawString, std::string& advertiserId);

bool readKey(const std::string& key, std::string& output);

std::string getPartialIPv6Address(const std::string& bytes);

}  // namespace AdX

