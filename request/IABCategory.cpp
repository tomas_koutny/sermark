#include "IABCategory.h"
#include <cstring>
#include <sstream>
#include <boost/lexical_cast.hpp>

#include "Util.h"

IABCategory IABCategory::fromString(const std::string& str)
{
   IABCategory iabCategory;
   if (IABCategory::fromString(str, iabCategory))
   {
      return iabCategory;
   }

   throw InvalidIABCategoryException("Invalid input string: " + str);
}

bool IABCategory::fromString(const std::string &str, IABCategory& iabCategory)
{
   // supplied string doesn't begin with "IAB"
   if (!blis::beginsWith(str, "IAB"))
   {
      return false;
   }

   // try to find, if any, the dash
   size_t dashIndex = str.find('-');
   try
   {
      if (dashIndex != std::string::npos)
      {
         // Tier 2 Category
         unsigned int category =
               boost::lexical_cast<unsigned int>(
                  str.substr(3,dashIndex - 3));
         unsigned int subCategory =
               boost::lexical_cast<unsigned int>(
                  str.substr(dashIndex+1,str.length() - dashIndex + 1));

         if ((category > 255) || (subCategory > 255))
         {
            return false;
         }

         iabCategory = IABCategory(category, subCategory);
         return true;
      }
      else
      {
         // Tier 1 Category
         unsigned int category =
               boost::lexical_cast<unsigned int>(str.substr(3, str.length()));

         if (category > 255)
         {
            return false;
         }

         iabCategory = IABCategory(category);
         return true;
      }
   }
   catch (const std::exception&)
   {
      return false;
   }
}

IABCategory::IABCategory()
:  m_bitset(0)
{}

IABCategory::IABCategory(unsigned int category)
:  m_bitset(category << 8)
{}

IABCategory::IABCategory(unsigned int category, unsigned int subCategory)
:  m_bitset((category << 8) + subCategory)
{}

IABCategoryType IABCategory::getType() const
{
   if (m_bitset & 0xFF)
   {
      return IAB_TIER2;
   }
   return IAB_TIER1;
}

bool IABCategory::contains(const IABCategory& iabCategory) const
{
   if ((getType() == IAB_TIER2) && (iabCategory.getType() == IAB_TIER2))
   {
      return (m_bitset == iabCategory.m_bitset);
   }
   else
   {
      return ((m_bitset & 0xFF00) == (iabCategory.m_bitset & 0xFF00));
   }
}

std::string IABCategory::toString() const
{
   std::ostringstream ss;
   ss << "IAB" << ((m_bitset & 0xFF00) >> 8);
   if (getType() == IAB_TIER2)
   {
      ss << "-" << (m_bitset & 0x00FF);
   }

   return ss.str();
}
