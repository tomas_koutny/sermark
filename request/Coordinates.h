#pragma once

enum LocationSource
{
   LOCATION_SOURCE_UNKNOWN = 0,
   LOCATION_SOURCE_ISP = 1,
   LOCATION_SOURCE_SSP = 2,
   LOCATION_SOURCE_ADX_COOKIE_MATCHING = 3,
   LOCATION_SOURCE_IPSCALE = 4,
   LOCATION_SOURCE_REALTIME_REDIS = 5
};

struct Coordinates
{
   Coordinates(double latitude = 0.0f, double longitude = 0.0f)
   :  m_latitude(latitude),
      m_longitude(longitude)
   {
   }

   double m_latitude;
   double m_longitude;
};


struct OptionalCoordinates : public Coordinates
{
   OptionalCoordinates()
   :  m_isSet(false),
      m_source(LOCATION_SOURCE_UNKNOWN)
   {
   }

   void clear()
   {
      m_latitude = m_longitude = 0.0f;
      m_isSet = false;
      m_source = LOCATION_SOURCE_UNKNOWN;
   }

   bool m_isSet;
   LocationSource m_source;
};
