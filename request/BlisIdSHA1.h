#pragma once

#include <array>
#include <stdexcept>

#include "BlisTypes.h"
#include "Util.h"


class BlisIdSHA1
{
public:

   enum
   {
      REQUIRED_PACKED_LENGTH = SHA1_DIGEST_SIZE,
      REQUIRED_UNPACKED_LENGTH = SHA1_DIGEST_SIZE * 2
   };

   BlisIdSHA1() noexcept
   :  m_id{0}
   {
   }

   BlisIdSHA1(const SHA1Digest& data) noexcept
   {
      m_id = data;
   }
   
   BlisIdSHA1(StringRef unpackedHexData)
   {
      m_id = BlisIdSHA1::pack(unpackedHexData);
   }

   BlisIdSHA1& operator=(StringRef unpackedHexData)
   {
      m_id = BlisIdSHA1::pack(unpackedHexData);
      return *this;
   }

   bool operator==(const BlisIdSHA1& other) const noexcept
   {
      return m_id == other.m_id;
   }
   
   bool operator==(StringRef unpackedHexData) const noexcept
   {
      try
      {
         return *this == BlisIdSHA1(unpackedHexData);
      }
      catch (...)
      {
      }
      
      return false;
   }
   
   bool operator==(const SHA1Digest& other) const noexcept
   {
      return m_id == other;
   }
   
   const SHA1Digest& getBytes() const noexcept
   {
      return m_id;
   }
   
   std::string toString() const
   {
      return blis::UnpackHex(StringRef(reinterpret_cast<const char*>(m_id.begin()), REQUIRED_PACKED_LENGTH), false);
   }

private:

   static SHA1Digest pack(StringRef data)
   {
      if (data.length() != REQUIRED_UNPACKED_LENGTH)
      {
         throw std::invalid_argument("incorrect data length for BlisIdSHA1: (" + data.to_string() + ")");
      }

      SHA1Digest temp;

      auto outputIter = temp.begin();
      auto inputIter = data.begin();
      
      while (inputIter != data.end())
      {
         char a = *inputIter++;
         char b = *inputIter++;
         
         *outputIter++ = (blis::HexCharToBits(a) << 4) | blis::HexCharToBits(b);
      }

      return temp;
   }

public:
   SHA1Digest m_id;
};


inline bool operator<(const BlisIdSHA1& a, const BlisIdSHA1& b)
{
   return a.m_id < b.m_id;
}


namespace std
{

template <>
struct hash<BlisIdSHA1>
{
   std::size_t operator()(const BlisIdSHA1& id) const
   {
      return *reinterpret_cast<const std::size_t*>(id.getBytes().begin());
   }  
};

}  // namespace std
