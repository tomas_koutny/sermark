#include <string.h>  // memcpy.
#include <stdexcept>
#include <iostream>

#include "IspPersonInfoStatic.h"


IspPersonInfoStatic::IspPersonInfoStatic()
:  m_Age(IspPersonInfoStatic::DEFAULT_AGE),
   m_Gender('U')
{
}


unsigned int IspPersonInfoStatic::GetAge() const
{
   return m_Age;
}


void IspPersonInfoStatic::SetAge(uint8_t age)
{
   m_Age = age;
}


char IspPersonInfoStatic::GetGender() const
{
   return m_Gender;
}


void IspPersonInfoStatic::SetGender(uint8_t gender)
{
   m_Gender = gender;
}
