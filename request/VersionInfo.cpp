#include "VersionInfo.h"
#include <cstring>
#include <boost/lexical_cast.hpp>

#include "Util.h"


VersionInfo::VersionInfo()
{
   reset();
}


VersionInfo::VersionInfo(unsigned int major, unsigned int minor, unsigned int micro)
:  m_major(major),
   m_minor(minor),
   m_micro(micro),
   m_isSet(true)
{
}


VersionInfo::VersionInfo(const std::string& version)
:  VersionInfo()
{
   if (!version.empty())
   {
      parse(version);
   }
}


void VersionInfo::reset()
{
   m_major = 0;
   m_minor = 0;
   m_micro = 0;
   m_isSet = false;
}

void VersionInfo::fromString(const std::string& version)
{
   if (!version.empty())
   {
      parse(version);
   }
}

std::string VersionInfo::toString() const
{
   std::string output;
   
   if (isSet())
   {
      output.append(std::to_string(getMajor()));
      output.push_back('.');
      output.append(std::to_string(getMinor()));
      output.push_back('.');
      output.append(std::to_string(getMicro()));
   }
   
   return output;
}


bool VersionInfo::getValue(const char* from, const char* to, std::size_t& length)
{
   if (from >= to)
   {
      return false;
   }
   
   length = to - from;
   const char* dotPtr = std::strchr(from, '.');
   if ((dotPtr != nullptr) && (dotPtr < to))
   {
      length = (dotPtr - from);
   }
   
   return (dotPtr != nullptr) || (length > 0);
}


void VersionInfo::parse(const std::string& version)
{
   reset();
   
   if (version.empty() || version == ".")
   {
      return;
   }
   
   const char* start = version.c_str();
   const char* end = start + version.length();
   std::size_t startOffset = 0;

   // Handle Blackberry versions such as "bb10".
   if (blis::beginsWith(version, "bb"))
   {
      startOffset = 2;
   }
   else if (blis::beginsWith(version, "v"))   // e.g "v1.2.3"
   {
      startOffset = 1;
   }
      
   // Trim values such as "2.1-update1" to "2.1".
   std::size_t lastChar = version.find_first_not_of("0123456789.", startOffset);
   if (lastChar != std::string::npos)
   {
      end = start + lastChar;
   }

   std::size_t length;
   
   start += startOffset;
   // Major: Required
   // Minor: Optional
   // Micro: Optional
   if (getValue(start, end, length))
   {
      try
      {
         m_major = boost::lexical_cast<unsigned int>(start, length);
      }
      catch (...)
      {
         reset();
         return;
      }
      
      try
      {
         start += length + 1;
         if (getValue(start, end, length))
         {
            m_minor = boost::lexical_cast<unsigned int>(start, length);
   
            start += length + 1;
            if (getValue(start, end, length))
            {
               m_micro = boost::lexical_cast<unsigned int>(start, length);
            }
         }
      }
      catch (...)
      {
         // minor and micro are optional so if they can't be parsed then ignore.
      }
      
      m_isSet = true;
   }
}
