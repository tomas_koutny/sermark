#pragma once

#include <boost/utility/string_ref.hpp>

//
// string_ref isn't part of standard C++ yet; when it is use that instead of boost's version.
//
template <typename CharType>
using BasicStringRef = boost::basic_string_ref<CharType>;

using StringRef = boost::string_ref;
using WStringRef = boost::wstring_ref;

