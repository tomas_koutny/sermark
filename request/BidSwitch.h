#pragma once

namespace BidSwitch
{

enum SkipOptions
{
   ALLOW_SKIPPABLE = 0,
   REQUIRE_SKIPPABLE = 1,
   DENY_SKIPPABLE = 2
};


enum ExpandingDirection
{
   EXPANDING_LEFT = 1,
   EXPANDING_RIGHT = 2,
   EXPANDING_UP = 3,
   EXPANDING_DOWN = 4,
   EXPANDING_FULLSCREEN = 5
};

}  // namespace BidSwitch
