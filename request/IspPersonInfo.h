#pragma once

#include "IspPersonInfoStatic.h"
#include "IspPersonInfoDynamic.h"


enum IspPersonInfoSource
{
   ISP_PERSON_INFO_SOURCE_NONE = 0,
   ISP_PERSON_INFO_SOURCE_BSKYB = 2
};

enum PersonInfoSourceType
{
   PERSON_SOURCE_NOT_REQUIRED = 0,
   PERSON_SOURCE_NO_SOURCE = 1,
   PERSON_SOURCE_ONLINE_ISP_CUSTOMER = 2,
   PERSON_SOURCE_SSP = 3,
   PERSON_SOURCE_HM_RETARGETED_ISP_CUSTOMER = 4,
   PERSON_SOURCE_BLIS_RETARGETED_ISP_CUSTOMER = 5   
};

class IspPersonInfo
{
public:

   static const char DEFAULT_GENDER = 'U';
   enum
   {
      DEFAULT_AGE = IspPersonInfoStatic::DEFAULT_AGE
   };
   
   IspPersonInfo(IspPersonInfoSource source = ISP_PERSON_INFO_SOURCE_NONE,
                 PersonInfoSourceType type = PERSON_SOURCE_NOT_REQUIRED)
   :  m_source(source), m_source_type(type)
   {
   }

   bool HasLocationInfo() const;
   
   double GetLatitude() const;
   void SetLatitude(double latitude);
   
   double GetLongitude() const;
   void SetLongitude(double longitude);
   
   bool HasVenueId() const;
   unsigned int GetVenueId() const;
   void SetVenueId(unsigned int venueId);
   
   bool HasVenueOwnerId() const;
   unsigned int GetVenueOwnerId() const;
   void SetVenueOwnerId(unsigned int venueOwnerId);

   unsigned int GetAge() const;
   void SetAge(uint8_t age);
   
   char GetGender() const;
   void SetGender(char gender);

   void ResetDynamicInfo();
   
   IspPersonInfoSource getSource() const;
   void setSource(IspPersonInfoSource source);

   PersonInfoSourceType getSourceType() const;
   void setSourceType(PersonInfoSourceType source_type);
   
   friend std::ifstream& operator>>(std::ifstream& stream, IspPersonInfo& info);
   friend std::ostream& operator<<(std::ostream& stream, const IspPersonInfo& info);

   IspPersonInfoStatic m_static;
   IspPersonInfoDynamic m_dynamic;

private:

   IspPersonInfoSource m_source;
   PersonInfoSourceType m_source_type;
};


inline std::ifstream& operator>>(std::ifstream& stream, IspPersonInfo& info)
{
   stream >> info.m_static;
   
   return stream;
}


inline std::ostream& operator<<(std::ostream& stream, const IspPersonInfo& info)
{
   stream << info.m_static;
   stream << info.m_dynamic;
   
   return stream;
}

