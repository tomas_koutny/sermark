#pragma once

#include <stdint.h>
#include <cstddef>
#include <fstream>

//
// Contains information about a customer that can change constantly.
//
class IspPersonInfoDynamic
{
public:

   IspPersonInfoDynamic();

   bool HasVenueId() const;
   unsigned int GetVenueId() const;
   void SetVenueId(uint32_t venueId);

   bool HasVenueOwnerId() const;
   unsigned int GetVenueOwnerId() const;
   void SetVenueOwnerId(uint32_t venueOwnerId);

   bool HasLocationInfo() const;

   double GetLatitude() const;
   void SetLatitude(double latitude);

   double GetLongitude() const;
   void SetLongitude(double longitude);

   void Reset();

   friend std::ostream& operator<<(std::ostream& stream, const IspPersonInfoDynamic& info);

   double m_Latitude;
   double m_Longitude;

private:

   uint8_t m_Flags;
   uint32_t m_VenueId;
   uint32_t m_VenueOwnerId;
};

inline std::ostream& operator<<(std::ostream& stream, const IspPersonInfoDynamic& info)
{
   bool locInfo = info.HasLocationInfo();
   
   stream << "|" << (locInfo ? std::to_string(info.GetLatitude()) : "");
   stream << "|" << (locInfo ? std::to_string(info.GetLongitude()) : "");
   stream << "|" << (locInfo ? std::to_string(info.GetVenueId()) : "");
   stream << "|" << (locInfo ? std::to_string(info.GetVenueOwnerId()) : "");
   
   return stream;
}
