#pragma once

#include <stdint.h>

namespace CPU
{

//
// Reads the Timestamp Counter of the current core.
// This is a serialising instruction which means it has a
// negative effect on instruction execution parallelism.
//
static inline uint64_t rdtscp()
{
   uint64_t rax;
   uint64_t rdx;
   uint32_t aux;  // Returned value is OS specific. Ignore.

   // AT&T syntax needs to die.
   __asm__ __volatile__(
      "rdtscp\n" : "=a"(rax), "=d"(rdx), "=c"(aux) ::
   );

   return (rdx << 32) + rax;
}

//
// This instruction improves the performance and power efficiency
// of busy-spin loops. It only has an effect on CPUs that support
// Hyper-Threading. On CPUs that do not support Hyper-Threading the
// machine code is identical to "rep; nop", which does nothing.
//
static inline void pause()
{
   __asm__ __volatile__(
      "pause\n"
   );
}


//
// Functions to convert from big endian values.
//
template <typename T>
inline T toLittleEndian(T value);

template <>
inline uint32_t toLittleEndian(uint32_t value)
{
#ifdef __GNUC__
   return __builtin_bswap32(value);
#else
#error "CPU::toLittleEndian not implemented for this compiler"
#endif
}

template <>
inline uint64_t toLittleEndian(uint64_t value)
{
#ifdef __GNUC__
   return __builtin_bswap64(value);
#else
#error "CPU::toLittleEndian not implemented for this compiler"
#endif
}


} // namespace CPU
