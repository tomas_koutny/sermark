#include <iostream>

#include "IspPersonInfo.h"


bool IspPersonInfo::HasLocationInfo() const
{
   return m_dynamic.HasLocationInfo();
}


double IspPersonInfo::GetLatitude() const
{
   return m_dynamic.GetLatitude();
}


void IspPersonInfo::SetLatitude(double latitude)
{
   m_dynamic.SetLatitude(latitude);
}


double IspPersonInfo::GetLongitude() const
{
   return m_dynamic.GetLongitude();
}


void IspPersonInfo::SetLongitude(double longitude)
{
   m_dynamic.SetLongitude(longitude);
}


bool IspPersonInfo::HasVenueId() const
{
   return m_dynamic.HasVenueId();
}


unsigned int IspPersonInfo::GetVenueId() const
{
   return m_dynamic.GetVenueId();
}


void IspPersonInfo::SetVenueId(unsigned int venueId)
{
   return m_dynamic.SetVenueId(venueId);
}


bool IspPersonInfo::HasVenueOwnerId() const
{
   return m_dynamic.HasVenueOwnerId();
}


unsigned int IspPersonInfo::GetVenueOwnerId() const
{
   return m_dynamic.GetVenueOwnerId();
}


void IspPersonInfo::SetVenueOwnerId(unsigned int venueOwnerId)
{
   m_dynamic.SetVenueOwnerId(venueOwnerId);
}


unsigned int IspPersonInfo::GetAge() const
{
   return m_static.GetAge();
}


void IspPersonInfo::SetAge(uint8_t age)
{
   return m_static.SetAge(age);
}


char IspPersonInfo::GetGender() const
{
   return m_static.GetGender();
}


void IspPersonInfo::SetGender(char gender)
{
   m_static.SetGender(gender);
}


void IspPersonInfo::ResetDynamicInfo()
{
   m_dynamic.Reset();
}


IspPersonInfoSource IspPersonInfo::getSource() const
{
   return m_source;
}


void IspPersonInfo::setSource(IspPersonInfoSource source)
{
   m_source = source;
}

PersonInfoSourceType IspPersonInfo::getSourceType() const
{
   return m_source_type;
}

void IspPersonInfo::setSourceType(PersonInfoSourceType type) 
{
   m_source_type = type;
}
