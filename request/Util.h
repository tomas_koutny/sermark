#pragma once

#include <stdint.h>
#include <sys/time.h>
#include <fstream>
#include <stdexcept>
#include <cstring>
#include <vector>
#include <sstream>
#include <openssl/hmac.h>
#include <openssl/evp.h>
#include <openssl/sha.h>
#include <boost/algorithm/string.hpp>
#include <boost/tokenizer.hpp>
#include <curl/curl.h>
#include <pthread.h>

#include "BlisTypes.h"


namespace blis
{
   inline int64_t Now()
   {
      // This code is the same as apr_time_now().
      struct timeval tv;
      gettimeofday(&tv, nullptr);
      
      return (tv.tv_sec * 1000000LL) + tv.tv_usec;
   }

   //
   // Returns the number of hours that have passed since a Unix timestamp.
   //
   inline double hoursSince(int64_t timestamp)
   {
      return double(Now() - timestamp) / double(MICROS_PER_HOUR);
   }

   //
   // Reads a binary type from a stream.
   //
   template <typename T>
   inline void ReadBinary(std::ifstream& stream, T* data)
   {
      stream.read(reinterpret_cast<char*>(data), sizeof(T));
   }
   
   
   //
   // Undoes the PHP pack("H*", xyz) function. i.e. converts "\xab" into "ab".
   //
   inline std::string UnpackHex(StringRef data, bool uppercase = false)
   {
      const char* table = "0123456789abcdef0123456789ABCDEF";
      
      const uint8_t* tablePtr = reinterpret_cast<const uint8_t*>(table);
      if (uppercase)
      {
         tablePtr += 16;
      }
      
      std::string ret;
      ret.resize(data.length() * 2);
      
      std::size_t index = 0;
      for (std::size_t i = 0; i < data.length(); ++i)
      {
         uint8_t octet = data[i];
        
         uint8_t upperNibble = (octet >> 4);
         uint8_t lowerNibble = (octet & 0x0f);
         
         ret[index++] = tablePtr[upperNibble];
         ret[index++] = tablePtr[lowerNibble];
      }
     
      return ret;
   }
   
   inline std::string UnpackHex(const std::string& data, std::size_t length, bool uppercase = false)
   {
      return blis::UnpackHex(StringRef{data.data(), length}, uppercase);
   }

   inline std::string UnpackHex(const char* data, std::size_t length, bool uppercase = false)
   {
      return blis::UnpackHex(StringRef{data, length}, uppercase);
   }

   //
   // Returns the 4-bit binary representation of a hex character.
   //
   inline uint8_t HexCharToBits(char c)
   {
      c &= 0xff;
      
      if ((c >= '0') && (c <= '9'))
      {
         return c - '0';
      }
      
      if ((c >= 'a') && (c <= 'f'))
      {
         return (c - 'a') + 10;
      }
      
      if ((c >= 'A') && (c <= 'F'))
      {
         return (c - 'A') + 10;
      }
      
      throw std::invalid_argument(std::string("invalid hex character: ") + c);
   }

   //
   // Converts "abcde" to "\x0a\xbc\xde".
   // It might be useful to provide an in-place version of this too.
   //
   inline std::string PackHex(const std::string& data)
   {
      const auto ToHex = [] (char a, char b) -> uint8_t
      {
         return (HexCharToBits(a) << 4) | HexCharToBits(b);
      };
      
      std::size_t dataSize = data.size();
      std::size_t len = (dataSize + 1) / 2;
      
      std::string ret;
      ret.resize(len);
      
      std::size_t start = 0;
      std::size_t dest = 0;
       
      if (dataSize & 0x01)
      {
         ret[dest++] = ToHex('0', data[0]);
         ++start;
      }
      
      for (std::size_t i = start; i < dataSize; i += 2)
      {
         ret[dest++] = ToHex(data[i], data[i + 1]);
      }
      
      return ret;
   }

   //
   // Probably not the most efficient but it's how it currently works.
   //
   inline std::size_t ReplaceAll(std::string& aString, const std::string& aWhat, const std::string& aWith)
   {
      std::size_t replacements = 0;

      if (aString.empty() || aWhat.empty())
      {
         return replacements;
      }
      
      std::size_t start = 0;
      std::size_t withLength = aWith.length();
      std::size_t whatLength = aWhat.length();
      
      while ((start = aString.find(aWhat, start)) != std::string::npos)
      {
         aString.replace(start, whatLength, aWith);
         start += withLength;
         ++replacements;
      }

      return replacements;
   }
   
   
   inline void ReplaceN(std::string& text, StringRef what, const std::string& with, std::size_t n)
   {
      if (text.empty() || what.empty())
      {
         return;
      }
      
      std::size_t start = 0;
      std::size_t withLength = with.length();
      std::size_t whatLength = what.length();
      
      while (n && ((start = text.find(what.data(), start, what.size())) != std::string::npos))
      {
         text.replace(start, whatLength, with);
         start += withLength;
         --n;
      }
   }

   //
   //
   //
   inline std::size_t ReplaceOnce(std::string& aString, const std::string& aWhat, const std::string& aWith)
   {
      std::size_t replacements = 0;

      if (aString.empty() || aWhat.empty())
      {
         return replacements;
      }
      
      std::size_t start = aString.find(aWhat, 0);
      
      if (start != std::string::npos)
      {
         aString.replace(start, aWhat.length(), aWith);
         ++replacements;
      }

      return replacements;
   }

   //
   // Returns true if str begins with trailer.
   //
   inline bool beginsWith(const std::string& str, const std::string& trailer)
   {
      if (trailer.length() == 0)
      {
         return false;
      }

      return (strncmp(str.c_str(), trailer.c_str(), trailer.length()) == 0);
   }

   //
   // Computes the SHA1 digest of some data.
   //
   inline void SHA1(const std::string& key, const std::string& data, SHA1Digest& output)
   {
      unsigned int padSize = SHA1_DIGEST_SIZE;
      
      if (!::HMAC(EVP_sha1(), key.data(), key.size(), (const uint8_t*)data.data(), data.size(), output.data(), &padSize))
      {
         throw std::runtime_error("Failed to compute SHA1 digest");
      }
   }
   
   
   inline std::string SHA1(const std::string& data)
   {
      SHA1Digest digest;
      
      ::SHA1(reinterpret_cast<const uint8_t*>(data.c_str()), data.length(), digest.begin());
      return blis::UnpackHex(
         std::string(reinterpret_cast<const char*>(digest.begin()), SHA1_DIGEST_SIZE),
         SHA1_DIGEST_SIZE
      );
   }
   
   
   template <typename T, typename F>
   inline void ForEachSetBit(T flags, F callback)
   {
      T mask = static_cast<T>(1);
      
      while (flags != 0)
      {
         if ((flags & mask) != 0)
         {
            callback(mask);
         }
         
         flags &= ~mask;
         mask <<= 1;
      }
   }


   inline bool isCharUnloggable(char c)
   {
      return !std::isprint(c);
   }

   //
   // Replaces the TAB character with another.
   //
   inline void untab(std::string& text, char replacement = ' ')
   {
      for (char& c : text)
      {
         if (isCharUnloggable(c))
         {
            c = replacement;
         }
      }
   }

   //
   // Replaces the unwanted characters with another.
   //
   inline void tidyAndTrim(std::string& text, 
                    char replacement = ' ',
                    std::size_t max_length = 500)
   {
      if (text.length() > max_length)
      {
         text.resize(max_length);
      }
      
      blis::untab(text, replacement);
   }

   //
   // Chops n characters from the end of string.
   // TODO: Investigate using std::string::pop_back() for C++11.
   //
   inline std::string& truncate(std::string& text, std::size_t n = 1)
   {
      if (!text.empty())
      {
         text.resize(text.length() - n);
      }

      return text;
   }


   //
   // Extracts the characters that occur between two marker strings.
   // If the first marker is empty then the characters will be returned from the start of the string.
   // If the second marker is empty them the characters will be returned from the first marker to the end of the string.
   //
   inline std::string extract(const std::string& text, const std::string& marker1, const std::string& marker2)
   {
      std::string output;

      std::size_t pos1 = (marker1.empty() ? 0 : text.find(marker1));
      if (pos1 == std::string::npos)
      {
         return output;
      }

      pos1 += marker1.length();

      std::size_t pos2 = (marker2.empty() ? text.length() : text.rfind(marker2));
      if ((pos2 == std::string::npos) || (pos2 < pos1))
      {
         return output;
      }

      std::size_t length = pos2 - pos1;
      output = text.substr(pos1, length);
      return output;
   }

   // This function works in a very similar way to 'extract', but instead of searching 'marker2'
   // from the end (in reverse order), it search for it in regular order _after_ the first marker.
   inline std::string extractAfter(const std::string& text, const std::string& marker1,
                                   const std::string& marker2)
   {
      std::string output;

      std::size_t pos1 = (marker1.empty() ? 0 : text.find(marker1));
      if (pos1 == std::string::npos)
      {
         return output;
      }

      pos1 += marker1.length();

      std::size_t pos2 = (marker2.empty() ? text.length() : text.find(marker2, pos1));
      if (pos2 == std::string::npos)
      {
         return output;
      }

      std::size_t length = pos2 - pos1;
      output = text.substr(pos1, length);
      return output;
   }
   
   
   inline bool isInvalidId(const std::string& id)
   {
      if (id.empty())
      {
         return true;
      }
      
      static const std::array<const char*, 6> badIds = { {
         "0",
         "null",
         "(null)",
         "NONEUDID",
         "00000000-0000-0000-0000-000000000000",
         "UNKNOWN"
      } };
      
      for (const char* badId : badIds)
      {
         if (id == badId)
         {
            return true;
         }
      }

      return false;      
   }
   

   template <typename T, typename S>
   inline void appendToVector(std::vector<T>& output, const S& input)
   {
      // TODO: appending an array to itself is UB.
      output.insert(output.end(), input.begin(), input.end());
   }
   
   
   inline std::string xmlEncode(const std::string& text)
   {
      std::string output;
      
      for (auto c : text)
      {
         switch (c)
         {
         case '&': output.append("&amp;"); break;
         case '<': output.append("&lt;"); break;
         case '>': output.append("&gt;"); break;
         case '\"': output.append("&quot;"); break;
         case '\'': output.append("&apos;"); break;
         default:
            output.push_back(c);
         }
      }
      
      return output;
   }
   
   
   inline std::string& trim(std::string& text)
   {
      static const auto predicate = [](char c)
      {
         return (c == ' ') || isCharUnloggable(c);
      };

      boost::algorithm::trim_if(text, predicate);
      return text;
   }
   
   
   inline std::string& toLowercase(std::string& text)
   {
      boost::algorithm::to_lower(text); // This only works for ASCII.
      return text;
   }

   inline std::string& toUppercase(std::string& text)
   {
      boost::algorithm::to_upper(text); // This only works for ASCII.
      return text;
   }

   inline std::string& normalizeString(std::string& text)
   {
      blis::toLowercase(blis::trim(text));
      return text;
   }
   
   inline std::string normalizedString(const std::string& text)
   {
      std::string temp = text;
      blis::normalizeString(temp);
      
      return temp;
   }
   
   
   inline std::string& replaceChar(std::string& text, char oldValue, char newValue)
   {
      std::replace(std::begin(text), std::end(text), oldValue, newValue);
      return text;
   }
   
   
   inline bool findInsensitive(const std::string& where, const std::string& what)
   {
      auto it = std::search(where.begin(), where.end(), what.begin(), what.end(), [] (char c1, char c2)
      {
         return std::toupper(c1) == std::toupper(c2);
      });
      
      return (it != where.end());
   }


   inline void splitString(const std::string& s, char delim, std::vector<std::string>& elems)
   {
       std::stringstream ss(s);
       std::string item;
       
       while (std::getline(ss, item, delim))
       {
           elems.push_back(item);
       }
   }


   inline std::vector<std::string> splitString(const std::string& s, char delim)
   {
       std::vector<std::string> elems;
       splitString(s, delim, elems);
       
       return elems;
   }
   
   template<class T>
   inline std::string getCommaSeparatedString(const T& container)
   {
      std::stringstream ss;
      const char* delim = "";
      for (const auto& v : container)
      {
         ss << delim;
         ss << v;
         delim = ",";
      }

      return ss.str();
   }
   
   template <typename F>
   inline void tokenise(const std::string& text, F callback, const char* delim = "\t")
   {
      if (!delim || (::strlen(delim) == 0))
      {
         throw std::invalid_argument("tokenising with invalid delimiter");
      }
      
      if (text.empty())
      {
         return;
      }
      
      boost::char_separator<char> sep(delim, "", boost::keep_empty_tokens);
      boost::tokenizer<boost::char_separator<char>> tokens(text, sep);
      
      std::size_t index = 0;
      for (const std::string& token : tokens)
      {
         callback(index++, token);
      }
   }


   template<class T, class U>
   inline T randomElementWeightBased(T begin, T end, U weightFunc)
   {
      if (begin == end)
      {
         // empty
         return end;
      }

      T iter = begin;

      unsigned totalWeight = weightFunc(*iter);
      ++iter;

      if (iter == end)
      {
         // it has only one element
         return totalWeight > 0 ? begin : end;
      }

      for (; iter != end; ++iter)
      {
         totalWeight += weightFunc(*iter);
      }

      if (totalWeight == 0)
      {
         return end;
      }

      const unsigned val = rand() % totalWeight;
      unsigned sum = 0;

      for (iter = begin; iter != end; ++iter)
      {
         sum += weightFunc(*iter);
         if (sum > val)
         {
            return iter;
         }
      }

      return end;
   }
   
   //
   // This algorithm is copied from OpenJDK's source code.
   //
   inline int32_t getJavaHash(const StringRef& sr)
   {
      if (sr.empty())
      {
         return 0;
      }
      
      // Keep hash as unsigned value to prevent undefined behaviour from int overflow.
      uint32_t hash = 0;
      for (const unsigned char c : sr)
      {
         hash = 31u * hash + c;
      }
      
      return static_cast<int32_t>(hash);
   }

   inline bool isHashControlGroup(int32_t javaHash, int seed, int threshold)
   {
      // cast from signed to unsigned int is required, because result on int+int overflow is undefined.
      const uint32_t hashCode = static_cast<uint32_t>(javaHash);

      // cast back to signed int to be consistent with Java.
      const int dm = static_cast<int32_t>(hashCode + seed) % 100;
      return std::abs(dm) < threshold;
   }

   template<typename S1, typename S2>
   inline bool compare(const S1& s1, const S2& s2)
   {
      return s1 == s2;
   }

   inline bool compare(const char* s1, const char* s2)
   {
      return !strcmp(s1, s2);
   }

   template<typename String>
   inline ImageMimeType convertToImageMime(const String& mimeType)
   {
      if (compare(mimeType, "image/jpg") || compare(mimeType, "image/jpeg"))
      {
         return IMAGE_MIME_TYPE_JPG;
      }

      if (compare(mimeType, "image/png"))
      {
         return IMAGE_MIME_TYPE_PNG;
      }

      if (compare(mimeType, "image/gif"))
      {
         return IMAGE_MIME_TYPE_GIF;
      }

      throw std::invalid_argument("Unknown MIME type " + std::string(mimeType));
   }

   inline bool compare(int x, int y, bool yCanBeBigger)
   {
      if (yCanBeBigger)
      {
         return !(x > y);
      }
      else
      {
         return x == y;
      }
   }

   template <typename BitField, typename Bit>
   inline bool isInBitField(BitField bf, Bit b) noexcept
   {
      return (bf & b) != 0;
   }


   inline bool verifyUtf8(const std::string& utf8)
   {
      const auto countCodeUnits = [] (uint8_t c) -> std::size_t
      {
         if ((c & 0x80) == 0x00) return 1;   // 0xxx xxxx -> 1
         if ((c & 0xe0) == 0xc0) return 2;   // 110x xxxx -> 2
         if ((c & 0xf0) == 0xe0) return 3;   // 1110 xxxx -> 3
         if ((c & 0xf8) == 0xf0) return 4;   // 1111 0xxx -> 4
         // if ((c & 0xfc) == 0xf8) return 5;   // 1111 10xx -> 5 (not in standard)
         // if ((c & 0xfe) == 0xfc) return 6;   // 1111 110x -> 6 (not in standard)
   
         return 0;
      };
   
      for (std::size_t i = 0; i < utf8.length(); )
      {
         uint8_t c = utf8[i];
   
         std::size_t codeUnits = countCodeUnits(c);
         if (codeUnits == 0)
         {
            return false;
         }
   
         if ((i + codeUnits) > utf8.length())
         {
            return false;
         }
   
         for (std::size_t cu = 1; cu < codeUnits; ++cu)
         {
            // All code units should be of the form 10xx xxxx
            if ((utf8[i + cu] & 0xc0) != 0x80)
            {
               return false;
            }
         }
   
         i += codeUnits;
      }
   
      return true;
   }

   //
   // An incredibly expensive way of url decoding some text.
   //
   inline std::string urlDecode(const std::string& s)
   {
      if (s.empty())
      {
         return s;
      }
      
      std::string output;
      
      CURL* curl = ::curl_easy_init();
      if (curl)
      {
         int dataLen = 0;
         char* data = ::curl_easy_unescape(curl, s.c_str(), s.length(), &dataLen);
         if (data)
         {
            output.assign(data, dataLen);
            ::curl_free(data);
         }
         
         ::curl_easy_cleanup(curl);
      }
      
      if (output.empty())
      {
         throw std::runtime_error("urlDecode failed");
      }
      
      return output;      
   }

   //
   // UrlEncode a string. Taken from httpd source code.
   //   
   inline void urlEncode(StringRef src, std::string& output, bool spaceAsPlus = false)
   {
      static const char* TABLE = "0123456789ABCDEF";
      
      if (!src.empty())
      {
         for (char sc : src)
         {
            unsigned char c = static_cast<unsigned char>(sc);
            
            // std::isalnum uses locales when there's really no need.
            if ((c < 0x80) && (std::isalnum(c) || (c == '-') || (c == '.') || (c == '_') || (c == '~')))
            {
               output.push_back(sc);
            }
            else if (spaceAsPlus && (c == ' '))
            {
               output.push_back('+');
            }
            else
            {
               output.push_back('%');
               output.push_back(TABLE[c >> 4]);
               output.push_back(TABLE[c & 0x0f]);
            }
         }
      }
   }
   
   
   inline std::string urlEncode(StringRef src, bool spaceAsPlus = false)
   {
      std::string output;
      output.reserve((src.length() * 3) / 2);
      
      blis::urlEncode(src, output, spaceAsPlus);
      
      return output;
   }


   inline bool extractEnvVarValue(const char* name, std::string& dest)
   {
      const char* envVar = std::getenv(name);
      if (envVar != nullptr)
      {
         dest = envVar;
         return true;
      }

      return false;
   }

   inline bool extractEnvVarValue(const std::string& name, std::string& dest)
   {
      return extractEnvVarValue(name.c_str(), dest);
   }
   

   inline void append(std::string& dest, StringRef src)
   {
      dest.append(src.data(), src.length());
   }
   
   
   inline void setThreadName(const std::string& name)
   {
      if (name.length() <= 16)
      {
         (void)::pthread_setname_np(::pthread_self(), name.c_str());
      }
   }
   
   
   inline bool isNumeric(StringRef text)
   {
      for (char c : text)
      {
         if ((c < '0') || (c > '9'))
         {
            return false;
         }
      }
      
      // An empty string is not numeric.
      return !text.empty();
   }
   
   
   struct MillisecondTimer
   {
      MillisecondTimer(unsigned int& result) noexcept
      :  m_result(result),
         m_start(blis::Now())
      {
      }
      
      ~MillisecondTimer() noexcept
      {
         m_result = static_cast<unsigned int>((blis::Now() - m_start) / 1000);
      }
      
      unsigned int& m_result;
      int64_t m_start;
   };
}  // namespace blis
