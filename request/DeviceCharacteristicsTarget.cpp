#include "DeviceCharacteristicsTarget.h"
#include "Util.h"


DeviceCharacteristicsTarget::DeviceCharacteristicsTarget()
:  m_hasTargets(false),
   m_hasMakeAndModel(false),
   m_versionOperator(OP_NONE),
   m_deviceTypeBitField(Device::DeviceType::ALL)
{
}
   
   
DeviceCharacteristicsTarget::DeviceCharacteristicsTarget(
   const std::string& make,
   const std::string& model,
   const std::string& os,
   const std::string& osVersion,
   const std::string& osVersionOperator,
   Device::DeviceTypeBitField devTypeBits
)
:  m_hasTargets(false),
   m_hasMakeAndModel(false),
   m_make(make),
   m_model(model),
   m_os(os),
   m_osVersion(osVersion),
   m_versionOperator(toOperator(osVersionOperator)),
   m_deviceTypeBitField(devTypeBits)
{
   blis::normalizeString(m_make);
   blis::normalizeString(m_model);
   blis::normalizeString(m_os);
   
   m_hasMakeAndModel = !m_make.empty() || !m_model.empty();
   m_hasTargets = m_hasMakeAndModel || !m_os.empty() || m_osVersion.isSet() || m_deviceTypeBitField != Device::DeviceType::ALL;
}
   
   
bool DeviceCharacteristicsTarget::hasTargets() const
{
   return m_hasTargets;
}


bool DeviceCharacteristicsTarget::matchesMake(const std::string& make) const
{
   return compareTarget(m_make, make);
}


bool DeviceCharacteristicsTarget::matchesModel(const std::string& model) const
{
   return compareTarget(m_model, model);
}


bool DeviceCharacteristicsTarget::matchesOS(const std::string& os) const
{
   return compareTarget(m_os, os);
}


bool DeviceCharacteristicsTarget::matchesOSVersion(const std::string& osVersion) const
{
   VersionInfo vi(osVersion);
   return matchesOSVersion(vi);
}

bool DeviceCharacteristicsTarget::matchesDeviceType(Device::DeviceType deviceType) const
{
   if (m_deviceTypeBitField == Device::DeviceType::ALL)
   {
      // Special case if "devicetype" is missing in the bidrequest, but
      // Banner doesn't want to verify it.
      return true;
   }

   if (deviceType == Device::DeviceType::NONE && m_hasMakeAndModel)
   {
      // Special case if "devicetype" is missing in the bidrequest, but
      // we allow it because it will be verified using device 'make' and 'model'.
      return true;
   }

   return blis::isInBitField(m_deviceTypeBitField, deviceType);
}


bool DeviceCharacteristicsTarget::matchesOSVersion(const VersionInfo& other) const
{
   if (m_osVersion.isSet() == false)
   {
      return true;
   }
   
   if (other.isSet() == false)
   {
      return false;
   }
   
   switch (m_versionOperator)
   {
   case OP_NONE:
      return true;
      
   case OP_EQUAL_TO:
      return m_osVersion == other;
      
   case OP_LESS_THAN:
      return m_osVersion < other;
      
   case OP_GREATER_THAN:
      return m_osVersion > other;
      
   case OP_LESS_THAN_OR_EQUAL:
      return m_osVersion <= other;
      
   case OP_GREATER_THAN_OR_EQUAL:
      return m_osVersion >= other;
   }
   
   return false;
}


DeviceCharacteristicsTarget::VersionOperator DeviceCharacteristicsTarget::toOperator(const std::string& op)
{
   if (op.empty()) return OP_NONE;

   // These are the enum values in the schema.
   if (op == "equal") return OP_EQUAL_TO;
   if (op == "less") return OP_GREATER_THAN;
   if (op == "greater") return OP_LESS_THAN;
   if (op == "less_equal") return OP_GREATER_THAN_OR_EQUAL;
   if (op == "greater_equal") return OP_LESS_THAN_OR_EQUAL;
   
   throw std::invalid_argument("unknown DeviceCharacteristics operator");
}


bool DeviceCharacteristicsTarget::compareTarget(const std::string& field, const std::string& value) const
{
   if (!field.empty())
   {
      return field == value;
   }
   
   // There is no targeting on the field, so it matches.
   return true;
}

