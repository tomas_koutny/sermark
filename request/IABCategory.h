#pragma once

#include <cstdint>
#include <string>
#include <stdexcept>
#include <iosfwd>

class InvalidIABCategoryException : public std::runtime_error
{
public:
   InvalidIABCategoryException(const std::string& msg)
   :  std::runtime_error(msg)
   {}
};

enum IABCategoryType
{
   IAB_TIER1 = 0,
   IAB_TIER2 = 1
};

class IABCategory
{
public:
   /// \brief converts \c str in a valid \c IABCategory object
   /// \throws InvalidIABCategoryException if \c str cannot be converted
   /// correctly
   static IABCategory fromString(const std::string& str);

   /// \brief converts \c str in a valid \c IABCategory object
   /// \return false if \c str cannot be converted correctly
   static bool fromString(const std::string &str, IABCategory& iabCategory);

   IABCategory();
   explicit IABCategory(unsigned int category);
   IABCategory(unsigned int category, unsigned int subCategory);

   bool contains(const IABCategory& iabCategory) const;

   IABCategoryType getType() const;

   std::string toString() const;

   // so it can go into a std::map
   bool operator<(const IABCategory& rhs) const
   {
      return (m_bitset < rhs.m_bitset);
   }

   bool operator!=(const IABCategory& rhs) const
   {
      return m_bitset != rhs.m_bitset;
   }
   
   bool operator==(const IABCategory& rhs) const
   {
      return m_bitset == rhs.m_bitset;
   }

   // bits: 0.. (subcategory) ..7 | 8.. (category) ..15
   uint16_t m_bitset;
};

inline
std::ostream& operator<<(std::ostream& out, const IABCategory& iabCategory)
{
   return out << iabCategory.toString();
}
