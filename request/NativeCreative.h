#pragma once

#include <string>
#include "BlisTypes.h"

class NativeImageAsset
{
public:
   NativeImageAsset()
   : m_type(BLIS_NATIVE_IMAGE_ASSET_TYPE_UNKNOWN),
     m_mimeType(IMAGE_MIME_TYPE_NONE),
     m_width(0),
     m_height(0)
   {
   }

   NativeImageAsset(BlisNativeImageAssetType type, ImageMimeType mimeType, unsigned int width, unsigned int height, std::string&& url)
   : m_type(type),
     m_mimeType(mimeType),
     m_width(width),
     m_height(height),
     m_url(std::move(url))
   {}

   BlisNativeImageAssetType getType() const
   {
      return m_type;
   }

   ImageMimeType getMimeType() const
   {
      return m_mimeType;
   }

   unsigned int getWidth() const
   {
      return m_width;
   }

   unsigned int getHeight() const
   {
      return m_height;
   }

   const std::string& getUrl() const
   {
      return m_url;
   }

   bool isValid() const
   {
      return !m_url.empty();
   }

// public because of boost::serialization
public:
   BlisNativeImageAssetType m_type;
   ImageMimeType m_mimeType;
   unsigned int m_width;
   unsigned int m_height;
   std::string m_url;
};

class NativeDataAsset
{
public:
   NativeDataAsset()
   : m_type(BLIS_NATIVE_DATA_ASSET_TYPE_UNKNOWN)
   {
   }

   NativeDataAsset(BlisNativeDataAssetType type, std::string&& value)
   : m_type(type),
     m_value(std::move(value))
   {
   }

   bool isTitle() const
   {
      return m_type == BLIS_NATIVE_DATA_ASSET_TYPE_TITLE;
   }

   BlisNativeDataAssetType getType() const
   {
      return m_type;
   }

   const std::string& getValue() const
   {
      return m_value;
   }

   int getLength() const
   {
      return m_value.size();
   }

   bool isValid() const
   {
      return !m_value.empty();
   }

// public because of boost::serialization
public:
   BlisNativeDataAssetType m_type;
   std::string m_value;
};
