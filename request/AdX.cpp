#include "AdX.h"

#include <sstream>
#include <iomanip>

#include <boost/tokenizer.hpp>

namespace AdX
{

bool buildAdvertiserId(const std::string& rawString, std::string& advertiserId)
{
   static bool dashes[] =
   {
      false, false, false, true,
      false, true,
      false, true,
      false, true,
      false, false, false, false, false, false
   };

   if (rawString.size() != 16)
   {
      return false;
   }

   std::ostringstream ss;
   size_t counter = 0;
   for (const unsigned char& c : rawString)
   {
      ss << std::uppercase
         << std::setw(2) << std::setfill('0')
         << std::hex << (unsigned int)c;

      if (dashes[counter])
      {
         ss << "-";
      }
      ++counter;
   }
   ss.str().swap(advertiserId);
   return true;
}

bool readKey(const std::string& key, std::string& output)
{
   typedef boost::char_separator<char> Separator;
   typedef boost::tokenizer<Separator> Tokenizer;

   if (key.empty()) { return false; }

   Separator sep(",");
   Tokenizer tokens(key, sep);

   char tempKey[32];
   size_t index = 0;
   for (auto token : tokens)
   {
      unsigned int c;
      std::istringstream hex_string(token);
      hex_string >> std::hex >> c;

      // Take only first 32 hex chars
      if (index < 32)
      {
         tempKey[index] = c;
      }

      ++index;
   }

   if (index != 32)
   {
      return false;
   }
   else
   {
      std::string(tempKey, 32).swap(output);
      return true;
   }
}

std::string getPartialIPv6Address(const std::string& bytes)
{
   std::stringstream ss;
   for (std::size_t i = 0, n = bytes.size(); i < n; ++i)
   {
      if (i > 0 && i % 2 == 0)
      {
         ss << ":";
      }

      const unsigned int byte = static_cast<unsigned char>(bytes[i]);

      ss << std::hex << std::setfill('0') << std::setw(2) << byte;
   }

   return ss.str();
}

}  // namespace AdX
