#pragma once

class PMPDeal
{
public:
   enum Type
   {
      WIN_NOT_GUARANTEED,
      WIN_GUARANTEED,
   };

   enum Targeting
   {
      TARGETING_BE_ONLY,
      TARGETING_DSP_ONLY,
      TARGETING_DSP_AND_BE,
   };

   PMPDeal()
   : m_id("")
   {
   }

   PMPDeal(std::string&& id, SSPId sspId, Type type, Targeting targeting)
   : m_id(std::move(id)),
     m_sspId(sspId),
     m_type(type),
     m_targeting(targeting)
   {}

   const std::string& getId() const
   {
      return m_id;
   }

   SSPId getSSPId() const
   {
      return m_sspId;
   }

   bool isGuaranteedToWin() const
   {
      return m_type == WIN_GUARANTEED;
   }

   bool isSupposedlyFixedPrice() const
   {
      return m_type == WIN_GUARANTEED;
   }

   bool isExchangeTargeted() const
   {
      return (m_targeting == TARGETING_BE_ONLY || m_targeting == TARGETING_DSP_AND_BE);
   }

   bool isDSPTargeted() const
   {
      return (m_targeting == TARGETING_DSP_ONLY || m_targeting == TARGETING_DSP_AND_BE);
   }

   bool isEqual(const std::string& pmpDealId, SSPId sspId) const
   {
      return ((m_id == pmpDealId) && (m_sspId == sspId));
   }

   bool isValid() const
   {
      return !m_id.empty();
   }

private:
   std::string m_id;
   SSPId m_sspId;
   Type m_type;
   Targeting m_targeting;
};


class PMPSeat
{
public:
   PMPSeat(const std::string& id = "", bool dspWide = false)
   : m_id(id),
     m_dspWide(dspWide)
   {}

   const std::string& getId() const
   {
      return m_id;
   }

   bool isDspWide() const
   {
      return m_dspWide;
   }

   bool isValid() const
   {
      return !m_id.empty();
   }

private:
   std::string m_id;
   bool m_dspWide;
};
