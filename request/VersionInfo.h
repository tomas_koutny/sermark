#pragma once

#include <string>

class VersionInfo
{
public:
   VersionInfo();
   VersionInfo(unsigned int major, unsigned int minor, unsigned int micro);
   VersionInfo(const std::string& version);
   
   void reset();

   void fromString(const std::string& version);
   
   unsigned int getMajor() const
   {
      return m_major;
   }
   
   unsigned int getMinor() const
   {
      return m_minor;
   }
   
   unsigned int getMicro() const
   {
      return m_micro;
   }
   
   bool isSet() const
   {
      return m_isSet;
   }
   
   std::string toString() const;

   friend bool operator==(const VersionInfo& a, const VersionInfo& b);
   friend bool operator!=(const VersionInfo& a, const VersionInfo& b);
   friend bool operator<=(const VersionInfo& a, const VersionInfo& b);
   friend bool operator>=(const VersionInfo& a, const VersionInfo& b);
   friend bool operator<(const VersionInfo& a, const VersionInfo& b);
   friend bool operator>(const VersionInfo& a, const VersionInfo& b);
   
private:
   static bool getValue(const char* from, const char* to, std::size_t& length);
   void parse(const std::string& version);
      
   template <typename Comparator>
   static bool compare(const VersionInfo& a, const VersionInfo& b, Comparator comp)
   {
      if (!a.isSet() || !b.isSet())
      {
         return false;
      }
      
      if (a.getMajor() != b.getMajor()) return comp(a.getMajor(), b.getMajor());
      if (a.getMinor() != b.getMinor()) return comp(a.getMinor(), b.getMinor());
      return comp(a.getMicro(), b.getMicro());
   }

   unsigned int m_major;
   unsigned int m_minor;
   unsigned int m_micro;
   bool m_isSet;
};


inline bool operator==(const VersionInfo& a, const VersionInfo& b)
{
   return (a.isSet() == b.isSet()) && (a.getMajor() == b.getMajor()) && (a.getMinor() == b.getMinor()) && (a.getMicro() == b.getMicro());
}

inline bool operator!=(const VersionInfo& a, const VersionInfo& b)
{
   return (a.getMajor() != b.getMajor()) || (a.getMinor() != b.getMinor()) || (a.getMicro() != b.getMicro());
}

inline bool operator<=(const VersionInfo& a, const VersionInfo& b)
{
   return VersionInfo::compare(a, b, std::less_equal<unsigned int>());
}

inline bool operator>=(const VersionInfo& a, const VersionInfo& b)
{
   return VersionInfo::compare(a, b, std::greater_equal<unsigned int>());
}

inline bool operator<(const VersionInfo& a, const VersionInfo& b)
{
   return VersionInfo::compare(a, b, std::less<unsigned int>());
}

inline bool operator>(const VersionInfo& a, const VersionInfo& b)
{
   return VersionInfo::compare(a, b, std::greater<unsigned int>());
}
