#pragma once

#include "BlisTypes.h"

class SSPInfo
{
public:
   SSPInfo();
   SSPInfo(SSPId id, const std::string& name);
   
   void reset();
   
   SSPId getId() const;
   void setId(SSPId id, bool useDefaults = false);
   
   const std::string& getName() const;
   void setName(const std::string& name);
   
   BlisHttpCode getNoBidHttpCode() const;
   void setNoBidHttpCode(BlisHttpCode code);
   
   void setNoBidResponseBody(const std::string& body);
   const std::string& getNoBidResponseBody() const;
   
   void setImpressionIsWin(bool impressionIsWin);
   bool impressionIsWin() const;
   
   void setSeatId(const std::string& seatId);
   const std::string& getSeatId() const;
   bool hasSeatId() const;
   
   void setBlisExchangeAllowed(bool allowed);
   bool isBlisExchangeAllowed() const;
   
   void setActive(bool active);
   bool isActive() const;
   
   void setTMax(int tmax);
   int getTMax() const;

   void setUseFloorInBidHash(bool value);
   bool isSetFloorInBidHash() const;
  
public: // for serialization 
   SSPId m_id;
private:
   std::string m_name;
   BlisHttpCode m_noBidHttpCode;
   std::string m_noBidResponseBody;
   bool m_impressionIsWin;
   std::string m_seatId;
   bool m_blisExchangeAllowed;
   bool m_active;
   int m_tmax;
   bool m_useFloorInBidHash;
};
