#pragma once

#include <string>
#include <set>
#include <vector>
#include <cstdint>

#include <boost/lexical_cast.hpp>

#include "SSPInfo.h"
#include "Device.h"
#include "Impression.h"
#include "Video.h"
#include "Restrictions.h"
#include "User.h"
#include "ImpressionSource.h"
#include "RegionalInfo.h"
#include "Regulations.h"
#include "IspPersonInfo.h"
#include "BlisIdSHA1.h"
#include "BlisTypes.h"

typedef std::vector<std::unique_ptr<Impression>> ImpressionsContainer;

class BidRequestParams
{
public:

   enum RequestStatus
   {
      REQUEST_STATUS_OK = 0,
      REQUEST_STATUS_UNKNOWN_SSP,
      REQUEST_STATUS_NO_BID_ID,
      REQUEST_STATUS_NO_DEVICE_OBJECT,
      REQUEST_STATUS_NO_DEVICE_COUNTRY,
      REQUEST_STATUS_NO_IMPRESSION_SOURCE,
      REQUEST_STATUS_NO_IMPRESSIONS,
      REQUEST_STATUS_NO_BUNDLE
   };
   
   
   BidRequestParams()
   :  m_timestamp(0),
      m_primarySSP(SSP_UNKNOWN), 
      m_ispCustomerLookupStatus(0),
      m_staticIpHitExpected(false),
      m_tmax(200),
      m_smartPinConfidence(0.0f),
      m_seatIdRequired(false),
      m_pathRetargetingQueryDone(false),
      m_ipRetargetingQueryDone(false),
      m_publisherGloballyBlacklisted(false)
   {
   }
   
   BidRequestParams(const SSPInfo& info, int64_t timestamp)
   :  m_sspInfo(info),
      m_timestamp(timestamp),
      m_primarySSP(m_sspInfo.getId()), 
      m_ispCustomerLookupStatus(0),
      m_staticIpHitExpected(false),
      m_tmax(200),
      m_smartPinConfidence(0.0f),
      m_seatIdRequired(false),
      m_pathRetargetingQueryDone(false),
      m_ipRetargetingQueryDone(false),
      m_publisherGloballyBlacklisted(false)
   {
   }
   
   SSPId getSecondaryId() const
   {
      return m_sspInfo.getId();
   }
   
   void generateBlisIds()
   {
      static const std::string EMPTY_STRING;
      
      generateBlisPublisherId();
      
      const std::string& country = m_device ? m_device->m_regionalInfoSSP.countryAlpha3 : EMPTY_STRING;
      for (const auto& imp : m_impressions)
      {
         imp->generateBlisOptimisationId(m_blisPublisherIdString, country);
      }
   }
   
   void generateBlisPublisherId()
   {
      // Make sure all fields used for id generation are in the bid/nobid logs.
      
      std::string key;
      
      key.reserve(1024);

      key += std::to_string(m_sspInfo.getId());
      
      if (m_app)
      {
         key += m_app->m_bundle;    // BidLogField: 49, NoBidLogField: 42
                  
         if (!m_impressions.empty())
         {
            const Impression& imp = *m_impressions[0];
            key += imp.getDisplayManager();
            key += imp.getDisplayManagerVersion().toString();
         }
      }
      else if (m_site)
      {
         key += blis::normalizedString(m_site->m_extracted_domain);   // BidLogField: 6, NoBidLogField: 6
      }
      
      m_blisPublisherIdString = blis::SHA1(key);
      m_blisPublisherId = m_blisPublisherIdString;
   }
   
   
   const BlisIdSHA1& getBlisPublisherId() const
   {
      return m_blisPublisherId;
   }
   
   
   const std::string& getBlisPublisherIdString() const
   {
      return m_blisPublisherIdString;
   }
   
   
   bool isComplete() const noexcept
   {
      return check() == REQUEST_STATUS_OK;
   }


   RequestStatus check() const noexcept
   {
      if (m_primarySSP == SSP_UNKNOWN) return REQUEST_STATUS_UNKNOWN_SSP;
      if (m_bid_id.empty()) return REQUEST_STATUS_NO_BID_ID;
      if (!m_device) return REQUEST_STATUS_NO_DEVICE_OBJECT;
      if (m_device->m_regionalInfoSSP.countryAlpha3.empty()) return REQUEST_STATUS_NO_DEVICE_COUNTRY;
      if (!m_app && !m_site) return REQUEST_STATUS_NO_IMPRESSION_SOURCE;
      if (m_impressions.empty()) return REQUEST_STATUS_NO_IMPRESSIONS;
      if (m_app && m_app->m_bundle.empty()) return REQUEST_STATUS_NO_BUNDLE;
      
      return REQUEST_STATUS_OK;
   }


   RequestTarget getRequestTarget() const
   {
      for (const auto& imp : m_impressions)
      {
         if (imp->isNative())
         {
            return RT_DSP_ONLY;
         }
         
         bool dspDealPresent = false;
         std::size_t bePMPBOIndex = NO_CHOSEN_INDEX;
         const auto& bidOptions = imp->getBidOptions();
         for (std::size_t i = 0; i < bidOptions.size(); ++i)
         {
            const auto& bidOption = bidOptions[i];
   
            if (bidOption.isPMP())
            {
               if (bidOption.isExchangeTargeted())
               {
                  // Record the index of the very first BE PMP deal.
                  if (bePMPBOIndex == NO_CHOSEN_INDEX)
                  {
                     bePMPBOIndex = i;
                     imp->setChosenBidOptionIndex(bePMPBOIndex);
                  }
               }

               if (bidOption.isDSPTargeted())
               {
                  // A DSP deal.
                  dspDealPresent = true;
               }
            }
         }

         // There is a BE PMP deal present.
         if (bePMPBOIndex != NO_CHOSEN_INDEX)
         {         
            return dspDealPresent ? RT_DSP_PMP_THEN_BE_PMP : RT_BE_PMP_ONLY;
         }
         
         // No BE PMP deal present.
         if (dspDealPresent)
         {
            return imp->isOMPEnabled() ? RT_DSP_PMP_THEN_BE_OMP : RT_DSP_ONLY;
         }
         else
         {
            return imp->isOMPEnabled() ? RT_DSP_OMP_THEN_BE_OMP : RT_NONE;
         }
      }
      
      return RT_NONE;
   }

   bool isSSP(SSPId sspId) const
   {
      return m_sspInfo.getId() == sspId;
   }

   bool isPrimarySSP(SSPId sspId) const
   {
      return m_primarySSP == sspId;
   }

   SSPId getPrimarySSPId() const
   {
      return m_primarySSP;
   }

   void setSeatIdRequired(bool required)
   {
      m_seatIdRequired = required;
   }

   bool isSeatIdRequired() const
   {
      return m_seatIdRequired;
   }

   const Regulations& getRegulations() const
   {
      return m_regulations;
   }
   
   Regulations& getRegulations()
   {
      return m_regulations;
   }
   
   void addImpression(std::unique_ptr<Impression>&& impression)
   {
      impression->setIndex(m_impressions.size());
      m_impressions.push_back(std::move(impression));
   }

   bool isUserAgeUnder13() const
   {
      if (m_user && !m_user->m_age.empty())
      {
         unsigned int age = boost::lexical_cast<unsigned int>(m_user->m_age);
         return age < 13;
      }

      return false;
   }

   const std::string& getLookupId() const
   {
      static const std::string EMPTY_STRING;
      if (m_app) 
      {
         return m_device ? m_device->getLookupId() : EMPTY_STRING;
      }
      else if (m_site)
      {
         return m_user ? m_user->getLookupId() : EMPTY_STRING;
      }
      return EMPTY_STRING;
   }

   ImpressionSource& getImpressionSource() const
   {
      if (m_site)
      {
         return *(m_site.get());
      }
      else if (m_app)
      {
         return *(m_app.get());
      }
   
      // OpenRTB spec requires only one of these both not both.
      throw std::runtime_error("BidRequest has neither Site nor App");
   }

   UnownedPtr<Impression> getImpressionByIndex(std::size_t index) const
   {
      for (auto& imp : m_impressions)
      {
         if (imp->getIndex() == index)
         {
            return UnownedPtr<Impression>(imp.get());
         }
      }

      std::string error = "BidRequestParams::getImpressionByIndex: Index " +  std::to_string(index) +
                          " is out of scope. Bid request " + m_bid_id + " only has " + std::to_string(m_impressions.size()) + " impressions.";
      throw std::runtime_error(error.c_str());
   }

   const std::string& getBidID() const
   {
      return m_bid_id;
   }

   int64_t getBidTime() const
   {
      return m_timestamp;
   }

   const std::string& getPublisherName() const
   {
      static const std::string EMPTY_STRING;
      if (m_site)
      {
         return m_site->m_pageURL;
      }
      else if (m_app)
      {
         return m_app->m_app_name;
      }
      return EMPTY_STRING;
   }

   const std::string& getPublisherRef() const
   {
      static const std::string EMPTY_STRING;
      if (m_site)
      {
         return m_site->m_refURL;
      }

      return EMPTY_STRING;
   }

   const std::string& getPublisher() const
   {
      static const std::string EMPTY_STRING;
      if (m_site)
      {
         return m_site->m_extracted_domain;
      }
      else if (m_app)
      {
         return m_app->m_app_name;
      }
      return EMPTY_STRING;
   }

   const std::string& getImpressionSourceType() const
   {
      static const std::string EMPTY_STRING;
      static const std::string TYPE_APP = "app";
      static const std::string TYPE_WEB = "www";

      if (m_site)
      {
         return TYPE_WEB;
      }
      else if (m_app)
      {
         return TYPE_APP;
      }
      return EMPTY_STRING;
   }

   bool isFromApp() const
   {
      return (m_app != nullptr);
   }

   bool isFromSite() const
   {
      return (m_site != nullptr);
   }

   bool isVideo() const
   {
      return (m_video != nullptr);
   }

   void getIABCategories(std::string& iab_categories) const
   {
      if (m_site)
      {
         m_site->GetCategoriesAsString(iab_categories);
      }
      else if (m_app)
      {
         m_app->GetCategoriesAsString(iab_categories);
      }
   }

   void getISPCustomerInfo(IspPersonInfoSource& ispCustomerSource,
                           std::string& ispCustomerId,
                           std::string& venueOwnerId,
                           std::string& venueId,
                           std::string& onlineUserCacheLookupResult) const
   {
      if (!m_ispCustomer)
      {
         return;
      }

      ispCustomerSource = m_ispCustomer->getSource();

      if (ispCustomerSource == ISP_PERSON_INFO_SOURCE_NONE)
      {
         return;
      }

      ispCustomerId = m_ispCustomerId;

      if (m_ispCustomer->HasVenueOwnerId())
      {
         venueOwnerId = std::to_string(m_ispCustomer->GetVenueOwnerId());
      }

      if (m_ispCustomer->HasVenueId())
      {
         venueId = std::to_string(m_ispCustomer->GetVenueId());
      }

      onlineUserCacheLookupResult = std::to_string(m_ispCustomerLookupStatus);
   }

   const std::string& getRequestSDK() const
   {
      static const std::string SDK_ADMOB = "admob";
      static const std::string SDK_MOPUB = "mopub";
      static const std::string SDK_MOBILE_WEB = "mobile-web";

      if (getSecondaryId() == SSP_MOPUB_23)
      {
         return SDK_MOPUB;
      }

      if (getSecondaryId() == SSP_ADX)
      {
         return isFromApp() ? SDK_ADMOB : SDK_MOBILE_WEB;
      }

      return SDK_MOBILE_WEB;
   }

   bool isAgeUnder13() const
   {
      if (isUserAgeUnder13())
      {
         return true;
      }

      if (m_ispCustomer)
      {
         return m_ispCustomer->GetAge() < 13;
      }

      return false;
   }

   bool isISPEnriched() const
   {
      if (m_device && m_device->m_coordinates.m_isSet &&
          m_device->m_coordinates.m_source == LOCATION_SOURCE_ISP)
      {
         return true;
      }
      return false;
   }

   bool isIPScaleEnriched() const
   {
      return m_staticIpHitExpected;
   }

   bool isSmartPinEnriched() const
   {
      if (m_smartPinConfidence > 0.0f &&
          m_device && m_device->m_coordinates.m_isSet &&
          m_device->m_coordinates.m_source == LOCATION_SOURCE_SSP)
      {
         return true;
      }
      return false;
   }

   bool isEnriched() const
   {
      return isISPEnriched() || isIPScaleEnriched() || isSmartPinEnriched();
   }
   
   const std::string& getPrimarySiteListKey() const
   {
      return getPublisher();
   }
   
   const std::string& getSecondarySiteListKey() const
   {
      static const std::string EMPTY;
      return m_app ? m_app->m_bundle : EMPTY;
   }

   bool isNativeDisplayManager() const
   {
      SSPId ssp = getSecondaryId();
      if (ssp != SSP_MOPUB_23 && ssp != SSP_NEXAGE_23)
      {
         return true;
      }

      for (auto& imp : m_impressions)
      {
         if (ssp == SSP_NEXAGE_23)
         {
            const char* const BURSTLY = "burstly";
            if (::strncasecmp(imp->getDisplayManager().c_str(), BURSTLY, strlen(BURSTLY)) == 0)
            {
               return false;
            }
         }
         else if (ssp == SSP_MOPUB_23)
         {
            const char* const MOPUB = "mopub";
            if (::strncasecmp(imp->getDisplayManager().c_str(), MOPUB, strlen(MOPUB)) != 0)
            {
               return false;
            }
         }
      }

      return true; 
   }

   const std::string getPathRetargetingFuncIds() const
   {
      return serializeRetFuncIds(m_pathRetFuncIds);
   }

   const std::string getIPRetargetingFuncIds() const
   {
      return serializeRetFuncIds(m_ipRetFuncIds);
   }

   void addApplicableDP(DemandPartnerId dpId)
   {
      m_applicableDPs.insert(dpId);
   }

   const std::set<DemandPartnerId>& getApplicableDPs() const
   {
      return m_applicableDPs;
   }

   void setConnId(const std::string& connId)
   {
      m_connId = connId;
   }

   const std::string& getConnId() const
   {
      return m_connId;
   }

   SSPInfo m_sspInfo;
   int64_t m_timestamp;
   SSPId m_primarySSP;
   std::unique_ptr<App> m_app;
   std::unique_ptr<Site> m_site;
   std::unique_ptr<Device> m_device;
   std::unique_ptr<User> m_user;
   std::unique_ptr<blis::Video> m_video;

   std::unique_ptr<IspPersonInfo> m_ispCustomer;
   std::string m_ispCustomerId;
   uint32_t m_ispCustomerLookupStatus;

   Restrictions m_restrictions;
   // don't add new elements into m_impression directly, use addImpression
   ImpressionsContainer m_impressions;
   std::set<std::string> m_blocked_advertisers;
   std::string m_bidHash;
   std::string m_bid_id;
   bool m_staticIpHitExpected;
   BlisIdSHA1 m_blisPublisherId;
   std::string m_blisPublisherIdString;
   int m_tmax;
   float m_smartPinConfidence;
   bool m_seatIdRequired;
   Regulations m_regulations;

   // Blis enriched parts of BidRequest
   std::set<RetargetingFunctionId> m_pathRetFuncIds;
   bool m_pathRetargetingQueryDone;
   std::set<RetargetingFunctionId> m_ipRetFuncIds; 
   bool m_ipRetargetingQueryDone;
   bool m_publisherGloballyBlacklisted;
   std::string m_auctioneerUrl;
   std::set<DemandPartnerId> m_applicableDPs;
   std::string m_connId;

private: 
   static std::string serializeRetFuncIds(const std::set<RetargetingFunctionId>& setIds)
   {
      auto serialize([](const std::set<RetargetingFunctionId>& setIDs, std::string& retFuncStr)
      {
         if (!setIDs.empty())
         {
            std::stringstream ss;
            for (auto id : setIDs)
            {
               ss << id << ",";
            }
            retFuncStr = ss.str();
            blis::truncate(retFuncStr, 1);
         }
      }); 

      std::string retFuncStr;
      serialize(setIds, retFuncStr);
      return retFuncStr;
   }
};

namespace util
{
   inline BidRequestParams createBidRequest() {
      SSPInfo sspInfo(SSP_NEXAGE_23, "Nexage 2.3");
      sspInfo.reset();
      BidRequestParams br(sspInfo, 0);
      br.m_bid_id = "745695d9-5d70-4dff-a07f-db13729290b4";
      br.m_bidHash = "12345678901234567890";
      br.m_tmax = 200;
      br.m_restrictions.AddRestriction(IABCategory(8, 18));
      br.m_restrictions.AddRestriction(IABCategory(23));
      br.m_restrictions.AddRestriction(IABCategory(26));
      br.m_restrictions.AddRestriction(IABCategory(25));
      br.m_restrictions.AddRestriction(IABCategory(24));
      br.m_restrictions.AddRestriction(IABCategory(8, 5));
      br.m_restrictions.AddRestriction(IABCategory(14, 3));
      br.m_restrictions.AddRestriction(IABCategory(15, 1));
      br.m_restrictions.AddRestriction(IABCategory(14, 1));
      br.m_restrictions.AddRestriction(IABCategory(11, 4));
      br.m_restrictions.AddRestriction(IABCategory(15, 5));
      br.m_restrictions.AddRestriction(IABCategory(14, 5));
      br.m_restrictions.AddRestriction(IABCategory(9, 9));
      br.m_ipRetFuncIds.insert(1);
      br.m_ipRetFuncIds.insert(2);
      br.m_ipRetFuncIds.insert(3);
      br.m_applicableDPs.insert(DEMAND_PARTNER_MOBFOX);
      br.m_applicableDPs.insert(DEMAND_PARTNER_PUBMATIC);
      br.m_blisPublisherId = "123123123123123123123123123abcabcabcabdd";

      {
         std::unique_ptr<Impression> impression(new Impression());
         impression->setSSPDefaults(br.m_primarySSP);
         impression->m_impression_id = "745695d9-5d70-4dff-a07f-db13729290b4-1";
         impression->setOMPBidFloor(1.5f);
         impression->m_allowed_dimensions.emplace_back(std::make_pair(320, 50));

         impression->addAllowedAppAdType(BLIS_AD_TYPE_TEXT);
         impression->addAllowedAppAdType(BLIS_AD_TYPE_IMAGE);
         impression->addAllowedAppAdType(BLIS_AD_TYPE_JAVASCRIPT);
         impression->addAllowedAppAdType(BLIS_AD_TYPE_MRAID);

         impression->addAllowedSiteAdType(BLIS_AD_TYPE_TEXT);
         impression->addAllowedSiteAdType(BLIS_AD_TYPE_IMAGE);
         impression->addAllowedSiteAdType(BLIS_AD_TYPE_JAVASCRIPT);
         impression->m_blisOptimisationId = "12345678901234567890";

         impression->m_privateDeals.emplace_back(Impression::PrivateDeal("1426863662579673123", 5.0f));
         impression->m_privateDeals.emplace_back(Impression::PrivateDeal("1465936954262481687", 4.0f));

         br.addImpression(std::move(impression));

      }

      br.m_app.reset(new App());
      br.m_app->m_app_name = "Score Mobile, Inc.-theScore Android (SDK)";
      br.m_app->m_app_domain_name = "thescore.com";
      br.m_app->m_bundle = "com.fivemobile.thescore";
      br.m_app->m_appStoreURL = "https://play.google.com/store/apps/details?id=com.fivemobile.thescore&hl=en";
      br.m_app->AddIABCategory(IABCategory(12));
      br.m_app->AddIABCategory(IABCategory(17));

      br.m_device.reset(new Device());
      br.m_device->m_device_dpid = "59a13b3b-6a78-4541-968e-6785c214291b";
      br.m_device->m_connectionType = BLIS_CONNECTION_TYPE_WIFI;
      br.m_device->m_carrier = "WIFI";
      br.m_device->m_device_os = "android";
      br.m_device->m_device_type = Device::DeviceType::MOBILE;
      br.m_device->m_device_user_agent = "Mozilla/5.0 (Linux; Android 5.1.1; SAMSUNG-SM-G530AZ Build/LMY48B; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/55.0.2883.91 Mobile Safari/537.36";
      br.m_device->m_device_ip = "98.4.203.182";
      br.m_device->m_regionalInfoSSP.countryAlpha3 = "USA";
      br.m_device->m_regionalInfoSSP.regionOrState = "NY";
      br.m_device->m_regionalInfoSSP.city = "BUFFALO";
      br.m_device->m_regionalInfoSSP.zip = "14201";
      br.m_device->m_regionalInfoSSP.metro = "514";

      br.m_user.reset(new User());
      br.m_user->m_uid = "59a13b3b-6a78-4541-968e-6785c214291b";

      return br;
   }
}