#pragma once

#include <stdint.h>
#include <limits>
#include <fstream>
#include "Util.h"

//
// Contains information about a customer that is unlikely to change.
// This information can be read from a cache file.
//
class IspPersonInfoStatic
{
public:

   enum
   {
      PARTIAL_ID_LENGTH = 16,
      DEFAULT_AGE = std::numeric_limits<uint8_t>::max()
   };

   IspPersonInfoStatic();

   unsigned int GetAge() const;
   void SetAge(uint8_t age);
   
   char GetGender() const;
   void SetGender(uint8_t gender);

   friend std::ifstream& operator>>(std::ifstream& stream, IspPersonInfoStatic& info);
   friend std::ostream& operator<<(std::ostream& stream, const IspPersonInfoStatic& info);

   uint8_t m_Age;
   uint8_t m_Gender;
};


inline std::ifstream& operator>>(std::ifstream& stream, IspPersonInfoStatic& info)
{
   blis::ReadBinary(stream, &info.m_Age);
   blis::ReadBinary(stream, &info.m_Gender);
   
   return stream;
}


inline std::ostream& operator<<(std::ostream& stream, const IspPersonInfoStatic& info)
{
   stream << "|" << (int)info.m_Age;
   stream << "|" << info.m_Gender;
   
   return stream;
}

