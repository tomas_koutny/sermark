#include "BidOption.h"

std::vector<PMPSeat> buildSeats(const std::vector<std::string>& seatIds)
{
   std::vector<PMPSeat> seats;
   seats.emplace_back("SEAT1");
   seats.emplace_back("SEAT2");
   return seats;
}


std::unique_ptr<BidOption> BidOption::buildPMPBidOption(const PMPDealParsedParams& parsedParams)
{
   std::unique_ptr<BidOption> result;

   if (parsedParams.isValid())
   {
      PMPDeal pmpDeal("DEAL_ID", SSP_NEXAGE_23, PMPDeal::Type::WIN_NOT_GUARANTEED, PMPDeal::Targeting::TARGETING_BE_ONLY);

      std::vector<PMPSeat> seats = buildSeats(parsedParams.getSeatIds());

      result.reset(new BidOption(
         parsedParams.getBidFloor(),
         parsedParams.getAuctionType(),
         std::move(pmpDeal),
         std::move(seats)
      ));
   }

   return result;
}


std::unique_ptr<BidOption> BidOption::buildOMPBidOption(float bidFloor, const std::string& dspSeatId)
{
   std::unique_ptr<BidOption> result;

   PMPSeat dspSeat;

   return std::unique_ptr<BidOption>(new BidOption(bidFloor, std::move(dspSeat)));
}
