#pragma once

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <boost/serialization/string.hpp>
#include <boost/serialization/set.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/optional.hpp>
#include <boost/serialization/variant.hpp>
#include <boost/serialization/array.hpp>

#include "../request/BidRequestParams.h"
#include "../response/BidResponse.h"
#include "../VersionedAdType.h"

BOOST_CLASS_VERSION(BidRequestParams, 0)
BOOST_CLASS_VERSION(App, 0)
BOOST_CLASS_VERSION(Site, 0)
BOOST_CLASS_VERSION(Device, 0)
BOOST_CLASS_VERSION(User, 0)
BOOST_CLASS_VERSION(OptionalCoordinates, 0)
BOOST_CLASS_VERSION(Impression, 0)
BOOST_CLASS_VERSION(Restrictions, 0)
BOOST_CLASS_VERSION(IABCategory, 0)
BOOST_CLASS_VERSION(Impression::PrivateDeal, 0)
BOOST_CLASS_VERSION(blis::Video, 0)
BOOST_CLASS_VERSION(SSPInfo, 0)
BOOST_CLASS_VERSION(Regulations, 0)
BOOST_CLASS_VERSION(NativeImageAsset, 0)
BOOST_CLASS_VERSION(NativeDataAsset, 0)
BOOST_CLASS_VERSION(NativeResponse::AssetResponse, 0)
BOOST_CLASS_VERSION(NativeResponse, 0)
BOOST_CLASS_VERSION(ImpressionResponse, 0)
BOOST_CLASS_VERSION(BidResponse, 0)
BOOST_CLASS_VERSION(BlisIdSHA1, 0)
BOOST_CLASS_VERSION(RegionalInfo, 0)

#ifdef __clang__
   #pragma clang diagnostic push
   #pragma clang diagnostic ignored "-Wtautological-compare"
#endif

namespace boost
{
   namespace serialization 
   {
      template<class Archive, class T>
      inline void save(Archive& ar, const std::unique_ptr<T>& t, const unsigned int /* version */)
      {
         // only the raw pointer has to be saved
         const T * const base_pointer = t.get();
         ar & base_pointer;
      }

      template<class Archive, class T>
      inline void load(Archive& ar, std::unique_ptr<T>& t, const unsigned int /* version */)
      {
         T *base_pointer;
         ar & base_pointer;
         t.reset(base_pointer);
      }

      template<class Archive, class T>
      inline void serialize(Archive& ar, std::unique_ptr<T>& t, const unsigned int version)
      {
         boost::serialization::split_free(ar, t, version);
      }

      /* --- Reference code that allows for serializing std::vector<std::unique_ptr>
         --- It uses std::move instead of the default boost vector serialization that invokes
         --- the copy constructor */
      template<class Archive, class Allocator, class T>
      inline void save(Archive& ar, const std::vector<std::unique_ptr<T>, Allocator>& t, const unsigned int)
      { 
         collection_size_type count (t.size());
         ar << count;
         for(const auto& element: t) 
         {
            ar << element;
         }
      }

      template<class Archive, class Allocator, class T>
      inline void load(Archive& ar, std::vector<std::unique_ptr<T>, Allocator>& t, const unsigned int)
      {
         collection_size_type count;
         ar >> count;
         t.clear();
         while(count-- > 0)
         {
            std::unique_ptr<T> element;
            ar >> element;
            t.push_back(std::move(element));// move object
         }
      }

      template<class Archive, class Allocator, class T>
      inline void serialize(Archive & ar, std::vector<std::unique_ptr<T>, Allocator>& t, const unsigned int version) 
      {
         boost::serialization::split_free(ar, t, version);
      }

      template <class Archive>
      void serialize(Archive& ar, BidRequestParams& params, const unsigned int /*version*/)
      {
         ar & params.m_primarySSP; // done
         ar & params.m_bid_id; // done
         ar & params.m_bidHash; // done
         ar & params.m_app; // done
         ar & params.m_site; // TODO - not present
         ar & params.m_device; // done
         ar & params.m_user; // done
         ar & params.m_blocked_advertisers; // done - not present
         ar & params.m_impressions; // done
         ar & params.m_restrictions; // done
         ar & params.m_staticIpHitExpected; // done
         ar & params.m_blisPublisherId; // done
         ar & params.m_tmax; // done
         ar & params.m_sspInfo; // done
         ar & params.m_video;  // done - not present
         ar & params.m_smartPinConfidence; // done
         ar & params.getRegulations(); // done
         ar & params.m_pathRetFuncIds; // done
         ar & params.m_pathRetargetingQueryDone; // done
         ar & params.m_publisherGloballyBlacklisted; // done
         ar & params.m_auctioneerUrl; // done - empty
         ar & params.m_applicableDPs; // done
         ar & params.m_connId; // done - empty
      }

      template <class Archive>
      void serialize(Archive& ar, App& app, const unsigned int /*version*/)
      {
         ar & app.m_app_name; // done
         ar & app.m_app_domain_name; // done
         ar & app.m_bundle; // done
         ar & app.m_iab_categories; // done
         ar & app.m_appStoreURL; // done
         ar & app.m_version; // done - not present
         ar & app.m_paid; // done
      }

      template <class Archive>
      void serialize(Archive& ar, Site& site, const unsigned int /*version*/)
      {
         ar & site.m_site_name;
         ar & site.m_domain_name;
         ar & site.m_publisher_name;
         ar & site.m_publisher_domain;
         ar & site.m_extracted_domain;
         ar & site.m_iab_categories;
         ar & site.m_pageURL;
      }

      template <class Archive>
      void serialize(Archive& ar, Device& device, const unsigned int /*version*/)
      {
         ar & device.m_device_id; // done - empty
         ar & device.m_device_ip; // done
         ar & device.m_regionalInfoSSP; // done
         ar & device.m_device_user_agent; // done
         ar & device.m_device_dpid; // done
         ar & device.m_deviceIDType; // done - default
         ar & device.m_deviceIDHashMethod; // done - default
         ar & device.m_connectionType; // done
         ar & device.m_carrier; // done
         ar & device.m_jsSupported; // done - default
         ar & device.m_device_os; // done
         ar & device.m_device_type; // done
         ar & device.m_coordinates; // done - default
         ar & device.m_ip2GeoCoordinates; // done - default
      }

      template <class Archive>
      void serialize(Archive& ar, User& user, const unsigned int /*version*/)
      {
         ar & user.m_uid;
      }

      template <class Archive>
      void serialize(Archive& ar, OptionalCoordinates& coordinates, const unsigned int /*version*/)
      {
         ar & coordinates.m_latitude;
         ar & coordinates.m_longitude;
         ar & coordinates.m_source;
         ar & coordinates.m_isSet;
      }

      template <class Archive>
      void serialize(Archive& ar, Impression::PrivateDeal& deal, const unsigned int /*version*/)
      {
         ar & deal.m_id;
         ar & deal.m_bidFloor;
      }

      template <class Archive>
      void serialize(Archive& ar, Impression& impression, const unsigned int /*version*/)
      {
         ar & impression.m_impression_id; // done
         ar & impression.m_allowed_dimensions; // done
         ar & impression.m_blockedAdTypes; // done
         ar & impression.m_blockedCreativeAttributes; // done
         ar & impression.m_bid_floor; // done
         ar & impression.m_allowedAppAdTypes; // done
         ar & impression.m_allowedSiteAdTypes; // done
         ar & impression.m_blisOptimisationId; // done
         ar & impression.m_onlySSL; // done
         ar & impression.m_privateDeals; // done
         ar & impression.m_privateDealIndex; // done
         ar & impression.m_isInterstitial; // done
      }

      template <class Archive>
      void serialize(Archive& ar, Restrictions& restrictions, const unsigned int /*version*/)
      {
         ar & restrictions.m_blocked_iab_categories;
      }

      template <class Archive>
      void serialize(Archive& ar, IABCategory& category, const unsigned int /*version*/)
      {
         ar & category.m_bitset;
      }

      template <class Archive>
      void serialize(Archive& ar, blis::Video& video, const unsigned int /*version*/)
      {
         ar & video.m_min_duration;
         ar & video.m_max_duration;
         ar & video.m_max_skippable_duration;
         ar & video.m_start_delay;
         ar & video.m_skip_options;
         ar & video.m_allowedVASTProtocolsBitSet;
         ar & video.m_playbackMethods;
         ar & video.m_width;
         ar & video.m_height;
         ar & video.m_position;
         ar & video.m_linearity;
      }

      template <class Archive>
      void serialize(Archive& ar, SSPInfo& sspInfo, const unsigned int /*version*/)
      {
         ar & sspInfo.m_id;
      }
      
      template <typename Archive>
      void serialize(Archive& ar, Regulations& regs, const unsigned int /*version*/)
      {
         ar & regs.m_regulations;
      }
      
      template <typename Archive>
      void serialize(Archive& ar, ExpandableCapabilities& caps, const unsigned int /*version*/)
      {
         ar & caps.m_capabilities;
      }

      template <typename Archive>
      void serialize(Archive& ar, NativeImageAsset& nia, const unsigned int /*version*/)
      {
         ar & nia.m_type;
         ar & nia.m_mimeType;
         ar & nia.m_width;
         ar & nia.m_height;
         ar & nia.m_url;
      }

      template <typename Archive>
      void serialize(Archive& ar, NativeDataAsset& nda, const unsigned int /*version*/)
      {
         ar & nda.m_type;
         ar & nda.m_value;
      }

      template <typename Archive>
      void serialize(Archive& ar, NativeResponse::AssetResponse& a, const unsigned int /*version*/)
      {
         ar & a.m_id;
         ar & a.m_detail;
      }

      template <typename Archive>
      void serialize(Archive& ar, NativeResponse& nr, const unsigned int /*version*/)
      {
         ar & nr.m_assetResponses;
      }

      template <typename Archive>
      void serialize(Archive& ar, ImpressionResponse& ir, const unsigned int /*version*/)
      {
         ar & ir.m_bidId;
         ar & ir.m_impressionId;
         ar & ir.m_demandPartnerId;
         ar & ir.m_connId;
         ar & ir.m_nbr;
         ar & ir.m_impressionIndex;
         
         if (ir.m_nbr == BLIS_NBR_NONE)
         {
            ar & ir.m_timestamp;
            ar & ir.m_billingId;
            ar & ir.m_exchangeBidPrice;
            ar & ir.m_buyMultiplier;
            ar & ir.m_pmpDealId;
            ar & ir.m_seatId;
            ar & ir.m_isVideo;
            ar & ir.m_campaignId;
            ar & ir.m_blisExchangeCampaignId;
            ar & ir.m_blisExchangeCreativeId;
            ar & ir.m_creativeId;
            ar & ir.m_creativeType;
            ar & ir.m_creativeTargetUrl;
            ar & ir.m_creativeWidth;
            ar & ir.m_creativeHeight;
            ar & ir.m_creativeAttributes;
            ar & ir.m_creativeIABCategories;
            ar & ir.m_creativeExpandableCaps;
            ar & ir.m_creativeSampleImageUrl;
            ar & ir.m_creativeVideoDuration;
            ar & ir.m_creativeLanguage;
            ar & ir.m_creativeNativeResponse;
            ar & ir.m_creativeAdvertiserDomain;
            ar & ir.m_creativeAdTag;
            ar & ir.m_impTrackerUrls;
            ar & ir.m_winHost;
            ar & ir.m_winPort;
            ar & ir.m_hasPostClickTracking;
            ar & ir.m_creativeBeaconUrls;
            ar & ir.m_creativeAdServerType;
         }
      }
      
      template <typename Archive>
      void serialize(Archive& ar, BidResponse& br, const unsigned int /*version*/)
      {
         ar & br.m_vecIRs;
         ar & br.m_responseBody;
      }
      
      template <typename Archive>
      void serialize(Archive& ar, BlisIdSHA1& id, const unsigned int /*version*/)
      {
         ar & id.m_id;
      }
      
      template <typename Archive, typename T, std::size_t N>
      void serialize(Archive& ar, std::array<T, N>& arr, const unsigned int /*version*/)
      {
         ar & boost::serialization::make_array(arr.data(), arr.size());
      }

      template <typename Archive>
      void serialize(Archive& ar, RegionalInfo& info, const unsigned int /*version*/)
      {
         ar & info.countryAlpha3; // done
         ar & info.regionOrState; // done
         ar & info.city; // done
         ar & info.zip; // done
         ar & info.metro; // done
      }

      template <typename Archive>
      void serialize(Archive& ar, Impression::MatchingCampaign& mc, const unsigned int /*version*/)
      {
         ar & std::get<0>(mc);
         ar & std::get<1>(mc);
         ar & std::get<2>(mc);
      }
   }
}

#ifdef __clang__
   #pragma clang diagnostic pop
#endif

