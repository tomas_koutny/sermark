#pragma once

#include "IABCategory.h"

class Restrictions
{
public:
   typedef std::set<IABCategory> RestrictionsContainer;

   void AddRestriction(const IABCategory& restricted)
   {
      m_blocked_iab_categories.insert(restricted);
   }

   const RestrictionsContainer& GetRestrictions() const
   {
      return m_blocked_iab_categories;
   }

   bool HasRestrictions() const
   {
      return !m_blocked_iab_categories.empty();
   }

   bool Contains(const IABCategory& category) const
   {
      for (const IABCategory& cat: m_blocked_iab_categories)
      {
         if (cat.contains(category))
         {
            return true;
         }
      }
      return false;
   }
   
   void toString(std::string& output) const
   {
      if (!m_blocked_iab_categories.empty())
      {
         for (const auto& bcat : m_blocked_iab_categories)
         {
            output.append(bcat.toString());
            output.push_back(',');
         }
         
         output.pop_back();
      }
   }

   RestrictionsContainer m_blocked_iab_categories;
};
