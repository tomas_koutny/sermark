#pragma once

#include "Util.h"

class User
{
public:
   static constexpr std::size_t MAX_USER_ID_LENGTH = 200;

   std::string m_uid; // User identifier
   std::string m_gender; // User's Gender
   std::string m_zipcode; // User's zipcode - Not used
   std::string m_home_country; // USer's home country - Not used
   std::string m_age; // User's age (String - so that empty string = unknown age)
   std::string m_cookieData;

   void fixFields()
   { 
      if (m_uid.length() > MAX_USER_ID_LENGTH)
      {
         m_uid.clear();
      }
    
      blis::toUppercase(m_uid);

      if (m_uid == "0")
      {
         m_uid.clear();
      }
   }

   const std::string& getLookupId() const
   {
      return m_uid;
   }
};
