#pragma once

#include <string>
#include <set>

#include "VersionInfo.h"
#include "RegionalInfo.h"
#include "BlisTypes.h"
#include "Util.h"
#include "Coordinates.h"

/**
 * @ingroup BidRequestMembers_Group
 * @brief Represents the device within the bid request <br>
 */
class Device
{
public:

   static constexpr const char* DEVICE_OS_IOS = "ios";
   static constexpr const char* DEVICE_OS_ANDROID = "android";
   static constexpr std::size_t MAX_DEVICE_ID_LENGTH = 200;

   enum DeviceType : uint32_t
   {
      NONE = 0,
      MOBILE = 1,
      TABLET = 2,
      PERSONAL_COMPUTER = 4,
      CONNECTED_TV = 8, // Set Top Box
      GAME_CONSOLE = 16, // Connected Device

      MOBILE_TABLET = MOBILE | TABLET,
      ALL = 0xffffffffu // Consistant with Gencache
   }; 

   typedef uint32_t DeviceTypeBitField;

   enum DeviceIDType : uint16_t
   {
      ID_UNKNOWN = 0,
      ID_IDFA = 1,
      ID_UDID = 2, 
      ID_ANDROID_ID = 3,
      ID_ANDROID_ADVERTISING_ID = 4,
      ID_OTHER = 5,
      ID_WPID = 6
   };

   enum DeviceIDHashMethod : uint16_t
   {
      HASH_UNKNOWN = 0,
      HASH_RAW = 1,
      HASH_SHA1 = 2, 
      HASH_MD5 = 3
   };

   Device(bool location_info_available = true)
   :  m_connectionType(BLIS_CONNECTION_TYPE_UNKNOWN),
      m_device_type(NONE),
      m_location_info_available(location_info_available),
      m_ipAddrVal(0),
      m_IPRangeType(BLIS_IPRANGE_TYPE_UNRESTRICTED),
      m_deviceIDType(ID_UNKNOWN),
      m_deviceIDHashMethod(HASH_UNKNOWN),
      m_jsSupported(0)
   {
   }

   std::string m_device_id;
   std::string m_device_ip;
   std::string m_device_ipv6;
   std::string m_device_user_agent;
   std::string m_device_make;
   std::string m_device_model;
   std::string m_device_os;
   std::string m_device_latlong;
   std::string m_device_dpid; // SHA1 Platform Specific Device Identifier.
   BlisConnectionType m_connectionType;

   OptionalCoordinates m_coordinates; 
   OptionalCoordinates m_ip2GeoCoordinates;
   OptionalCoordinates m_realtimeCoordinates;

   DeviceType m_device_type;
   bool m_location_info_available;
   IP4Address m_ipAddrVal;    // The numeric value of m_device_ip.
   BlisIPRangeType m_IPRangeType;
   VersionInfo m_versionInfo;
   std::set<std::string> m_languages;
   DeviceIDType m_deviceIDType;
   DeviceIDHashMethod m_deviceIDHashMethod;
   std::string m_carrier;
   unsigned int m_jsSupported; // 1 if JS is supported, 0 otherwise
   
   RegionalInfo m_regionalInfoSSP;
   
   void fixFields()
   {
      if (m_device_id.length() > MAX_DEVICE_ID_LENGTH)
      {
         m_device_id.clear();
      }

      if (blis::isInvalidId(m_device_id))
      {
         m_device_id.clear();
      }

      if (m_device_dpid.length() > MAX_DEVICE_ID_LENGTH)
      {
          m_device_dpid.clear();
      }
      
      if (blis::isInvalidId(m_device_dpid))
      {
         m_device_dpid.clear();
      }
      
      blis::toUppercase(m_device_id);
      blis::toUppercase(m_device_dpid);
   }

   const std::string& getLookupId() const
   {
      return m_device_dpid; 
   }

   std::string getDeviceOS(bool stripSpaces = false) const
   {
      std::string os = m_device_os;
      if (stripSpaces)
      {
         blis::ReplaceAll(os, " ", "");
      }
      return os;
   }

   std::string getDeviceOSVersionString() const
   {
      if (m_versionInfo.isSet())
      {
         return m_versionInfo.toString();
      }

      static const std::string EMPTY_STRING;
      return EMPTY_STRING;
   }

   const std::string& getDeviceMake() const
   {
      return m_device_make;
   }

   const std::string& getDeviceModel() const
   {
      return m_device_model;
   }

   const std::string& getDeviceIdForMacroReplacement() const
   {
      static const std::string NO_DEVICE_ID = "";

      if (!m_device_dpid.empty())
      {
         return m_device_dpid;
      }

      if (!m_device_id.empty())
      {
         return m_device_id;
      }

      return NO_DEVICE_ID;
   }
};
