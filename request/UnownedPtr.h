#pragma once

//
// A wrapper around a pointer that prevents that pointer from being deleted
// without the overhead of shared_ptr's reference counting. It also allows
// for nullptr values, and checking for those, unlike reference_wrapper.
//


template <typename T>
struct UnownedPtr
{
   UnownedPtr() noexcept
   :  m_ptr(nullptr)
   {
   }
   
   UnownedPtr(const T* ptr) noexcept
   :  m_ptr(ptr)
   {
   }
   
   UnownedPtr(const T& obj) noexcept
   :  m_ptr(&obj)
   {
   }
   
   const T& operator*() const noexcept
   {
      return *m_ptr;
   }
   
   const T* operator->() const noexcept
   {
      return m_ptr;
   }
   
   explicit operator bool() const
   {
      return (m_ptr != nullptr);
   }
   
   bool operator==(const UnownedPtr& other) const noexcept
   {
      return (m_ptr == other.m_ptr);
   }
   
   bool operator==(const T& other) const noexcept
   {
      return (m_ptr == &other);
   }

   bool operator!=(const UnownedPtr& other) const noexcept
   {
      return !(this->operator ==(other));
   }

   bool operator!=(const T& other) const noexcept
   {
      return !(this->operator ==(other));
   }

   const T* get() const noexcept
   {
      return m_ptr;
   }

   void reset(const T* ptr = nullptr) noexcept
   {
      m_ptr = ptr;
   }
   
   void reset(const T& obj) noexcept
   {
      m_ptr = &obj;
   }
   
private:
   const T* m_ptr;
};
