#include <stdexcept>

#include "IspPersonInfoDynamic.h"

   //  7 6 5 4 3 2 1 0
   // +-+-+-+-+-+-+-+-+
   // |x|x|E|E|D|C|B|A|
   // +-+-+-+-+-+-+-+-+
   //
   // A: 1 if latitude is set.
   // B: 1 if longitude is set.
   // C: 1 if venue id is set.
   // D: 1 if venue owner id is set.
   // E: Number of entries in the location group ids vector.
   // x: Not used. Zeroed.

enum
{
   FLAG_LATITUDE =  (1 << 0),
   FLAG_LONGITUDE = (1 << 1),
   FLAG_VENUE_ID =  (1 << 2),
   FLAG_VENUE_OWNER_ID = (1 << 3)
};


template <typename T>
inline bool IsSet(T bits, int flag)
{
   return ((bits & flag) != 0);
}


template <typename T>
inline void Set(T& bits, int flag)
{
   bits |= flag;
}


IspPersonInfoDynamic::IspPersonInfoDynamic()
:  m_Latitude(0.0),
   m_Longitude(0.0),
   m_Flags(0),
   m_VenueId(0),
   m_VenueOwnerId(0)
{
}


bool IspPersonInfoDynamic::HasVenueId() const
{
   return IsSet(m_Flags, FLAG_VENUE_ID);
}


unsigned int IspPersonInfoDynamic::GetVenueId() const
{
   return m_VenueId;
}


void IspPersonInfoDynamic::SetVenueId(uint32_t venueId)
{
   m_VenueId = venueId;
   Set(m_Flags, FLAG_VENUE_ID);
}


bool IspPersonInfoDynamic::HasVenueOwnerId() const
{
   return IsSet(m_Flags, FLAG_VENUE_OWNER_ID);
}


unsigned int IspPersonInfoDynamic::GetVenueOwnerId() const
{
   return m_VenueOwnerId;
}


void IspPersonInfoDynamic::SetVenueOwnerId(uint32_t venueOwnerId)
{
   m_VenueOwnerId = venueOwnerId;
   Set(m_Flags, FLAG_VENUE_OWNER_ID);
}


bool IspPersonInfoDynamic::HasLocationInfo() const
{
   return IsSet(m_Flags, FLAG_LATITUDE) && IsSet(m_Flags, FLAG_LONGITUDE);
}


double IspPersonInfoDynamic::GetLatitude() const
{
   return m_Latitude;
}


void IspPersonInfoDynamic::SetLatitude(double latitude)
{
   m_Latitude = latitude;
   Set(m_Flags, FLAG_LATITUDE);
}


double IspPersonInfoDynamic::GetLongitude() const
{
   return m_Longitude;
}


void IspPersonInfoDynamic::SetLongitude(double longitude)
{
   m_Longitude = longitude;
   Set(m_Flags, FLAG_LONGITUDE);
}


void IspPersonInfoDynamic::Reset()
{
   m_Flags = 0;
}
