#pragma once

#include <utility>
#include <cstdint>
#include <array>
#include <limits>
#include <map>
#include <vector>
#include <atomic>
#include <boost/unordered_set.hpp>
#include "CPU.h"

#include "StringRef.h"

enum IspCustomerFlags
{
   // If 0 the values of the BMID related flags are meaningless.
   // The Hosted Match flags may still be valid.
   FLAG_CACHE_AVAILABLE =              (1 << 0),
   
   // Set to 1 if the UserAgent contained a BMID.
   FLAG_BMID_AVAILABLE =               (1 << 1),
   
   // Set to 1 if the bid request contained Hosted Match data.
   FLAG_HOSTED_MATCH_AVAILABLE =       (1 << 2),
   
   // Only valid if FLAG_CACHE_AVAILABLE is set.
   FLAG_BMID_LOOKUP_DONE =             (1 << 3),
   
   // Only valid if FLAG_BMID_LOOKUP_DONE is set.
   FLAG_BMID_HIT =                     (1 << 4),
   
   // The flag is set if the HM data is itself demo data and not a key to a cache. 
   FLAG_HOSTED_MATCH_IS_DEMO_DATA =    (1 << 5),
   
   // Only valid if the FLAG_HOSTED_MATCH_AVAILABLE is
   // set and FLAG_HOSTED_MATCH_IS_DEMO_DATA is not set.
   FLAG_HOSTED_MATCH_LOOKUP_DONE =     (1 << 6),
   
   // Only valid if FLAG_HOSTED_MATCH_AVAILABLE is set.
   // Will be set if FLAG_HOSTED_MATCH_IS_DEMO_DATA is set.
   FLAG_HOSTED_MATCH_HIT =             (1 << 7),
   
   // Only valid if FLAG_HOSTED_MATCH_AVAILABLE is set.
   FLAG_HOSTED_MATCH_UNKNOWN_SOURCE =  (1 << 8),
   
   // The data in the Hosted Match field isn't decryptable.
   FLAG_HOSTED_MATCH_CORRUPT =         (1 << 9),
   
   // Only valid if FLAG_BMID_HIT is set or FLAG_HOSTED_MATCH_HIT is set.
   FLAG_DATA_PARSE_ERROR =             (1 << 10), 

   // Retargeting table lookup done
   FLAG_RETARGETING_TABLE_LOOKUP_DONE = (1 << 11),

   // Retargeting table hit
   FLAG_RETARGETING_TABLE_HIT =        (1 << 12),

   // Bid request device contained an IP address.
   FLAG_IP_ADDRESS_AVAILABLE =         (1 << 13),

   // Only valid if FLAG_CACHE_AVAILABLE is set.
   FLAG_IP_ADDRESS_LOOKUP_DONE =       (1 << 14),

   // Only valid if FLAG_IP_ADDRESS_LOOKUP_DONE is set.
   FLAG_IP_ADDRESS_HIT =               (1 << 15)
};

enum ImageMimeType : uint16_t
{
   IMAGE_MIME_TYPE_NONE       = 0,
   IMAGE_MIME_TYPE_GIF        = (1 << 0),
   IMAGE_MIME_TYPE_JPG        = (1 << 1),
   IMAGE_MIME_TYPE_PNG        = (1 << 2)
};

typedef uint16_t ImageMimeTypeBitField;

enum BlisNativeImageAssetType
{
   BLIS_NATIVE_IMAGE_ASSET_TYPE_UNKNOWN = 0,
   BLIS_NATIVE_IMAGE_ASSET_TYPE_ICON = 1,
   BLIS_NATIVE_IMAGE_ASSET_TYPE_LOGO = 2,
   BLIS_NATIVE_IMAGE_ASSET_TYPE_MAIN = 3
};


enum BlisNativeDataAssetType
{
   BLIS_NATIVE_DATA_ASSET_TYPE_UNKNOWN = 0,
   BLIS_NATIVE_DATA_ASSET_TYPE_TITLE = 1,
   BLIS_NATIVE_DATA_ASSET_TYPE_DESCRIPTION = 2,
   BLIS_NATIVE_DATA_ASSET_TYPE_CALL_TO_ACTION = 3,
   BLIS_NATIVE_DATA_ASSET_TYPE_RATING = 4
};

enum BlisAdType // mime_type defined in Infinity schema
{
   BLIS_AD_TYPE_UNKNOWN = 0,  // Not in the schema.
   BLIS_AD_TYPE_TEXT = 1,
   BLIS_AD_TYPE_IMAGE = 2,
   BLIS_AD_TYPE_JAVASCRIPT = 3,
   BLIS_AD_TYPE_MM4RM_DEPRECATED = 4,
   BLIS_AD_TYPE_MRAID = 5,
   BLIS_AD_TYPE_ORMMA = 6,
   BLIS_AD_TYPE_VPAID = 7,
   BLIS_AD_TYPE_VAST = 9,
   BLIS_AD_TYPE_NATIVE = 10,
   BLIS_AD_TYPE_VPAID_FLASH = 11,
   BLIS_AD_TYPE_VPAID_JS = 12
};


namespace blis
{
   inline bool isVastOrVpaid(BlisAdType type)
   {
      return type == BLIS_AD_TYPE_VAST ||
             type == BLIS_AD_TYPE_VPAID_FLASH ||
             type == BLIS_AD_TYPE_VPAID_JS;
   }
}

enum BlisBannerFrameTargeting // frame_targeting defined in Infinity schema
{
   BLIS_BANNER_FRAME_TARGETING_UNKNOWN = 0,  // Not in the schema.
   BLIS_BANNER_FRAME_TARGETING_ANY = 1,
   BLIS_BANNER_FRAME_TARGETING_TOPFRAME = 2,
   BLIS_BANNER_FRAME_TARGETING_IFRAME = 3
};


//
// Store creative attributes as a bitfield instead of an int vector.
//
enum CreativeAttributeFlag
{
   CA_FLAG_UNSUPPORTED_ATTRIBUTE = 0,
   CA_FLAG_EXPANDABLE_AUTOMATIC =               (1 << 0),
   CA_FLAG_EXPANDABLE_USER_INITIATED_ROLLOVER = (1 << 1),
   CA_FLAG_EXPANDABLE_USER_INITIATED_CLICK =    (1 << 2),
   CA_FLAG_FLASH_CREATIVE =                     (1 << 3),
   CA_FLAG_SSL_CREATIVE =                       (1 << 4),
   CA_FLAG_VIDEO_AUTO_PLAY =                    (1 << 5),
   CA_FLAG_AUTO_REFRESH =                       (1 << 6),
   CA_FLAG_POP =                                (1 << 7),
   CA_FLAG_WINDOWS_DIALOG_ALERT =               (1 << 8),
   CA_FLAG_IFRAME_CREATIVE =                    (1 << 9),
   CA_FLAG_IMAGE =                              (1 << 10),
   CA_FLAG_INTERSTITIAL_CREATIVE =              (1 << 11),
   CA_FLAG_MOBILE_WEB_OPTIMIZED_CREATIVE =      (1 << 12),
   CA_FLAG_VIDEO_USER_INITIATED =               (1 << 13),
   CA_FLAG_SKIPPABLE =                          (1 << 14),
   
   // Helper definitions.
   CA_IS_EXPANDABLE = CA_FLAG_EXPANDABLE_AUTOMATIC | CA_FLAG_EXPANDABLE_USER_INITIATED_CLICK | CA_FLAG_EXPANDABLE_USER_INITIATED_ROLLOVER,

   CA_OPENRTB_INCOMPATIBLE_FLAGS =
      CA_FLAG_SSL_CREATIVE |
      CA_FLAG_IFRAME_CREATIVE |
      CA_FLAG_IMAGE |
      CA_FLAG_INTERSTITIAL_CREATIVE |
      CA_FLAG_MOBILE_WEB_OPTIMIZED_CREATIVE
};

typedef uint16_t CreativeAttributeBitField;

//
// Do not change, you will probably break the ad server.
//
enum AdServerType
{
   ADSERVER_UNKNOWN = -1,
   ADSERVER_IN_HOUSE = 0,
   ADSERVER_CELTRA = 1,
   ADSERVER_DOUBLECLICK = 2,
   ADSERVER_SIMPLYTICS = 3,
   ADSERVER_FLASHTALKING = 4,
   ADSERVER_CELTRA_V4 = 5,
   ADSERVER_SPONGECELL = 6,
   ADSERVER_BLIS_TRACKER = 7,
   ADSERVER_BLIS_EXCHANGE = 8,
   ADSERVER_BLIS_EXCHANGE_V2 = 9
};


//
// The values are taken from the ssp_info table.
// Do not add entries without adding them to that table too.
// If you remove an entry you're probably wrong.
//
enum SSPId
{
   SSP_UNKNOWN = 0,
   SSP_NEXAGE_DEPRECATED = 23,
   SSP_DEMO = 25,
   SSP_ADMELD = 28,
   SSP_ADX = 29,
   SSP_OPENX = 30,
   SSP_MOBCLIX = 37,
   SSP_AMOBEE = 38,
   SSP_NEXAGE_21 = 41,
   SSP_RUBICON_21 = 42,
   SSP_BIDSWITCH = 44,
   SSP_BIDSWITCH_ADAPTV = 45,
   SSP_BIDSWITCH_LIVERAIL = 46,
   SSP_BIDSWITCH_OPENX = 47,
   SSP_BIDSWITCH_PUBMATIC = 48,
   SSP_BIDSWITCH_PULSEPOINT = 49,
   SSP_BIDSWITCH_AOL = 50,
   SSP_BIDSWITCH_BRIGHTROLL = 51,
   SSP_BIDSWITCH_INNERACTIVE = 52,
   SSP_BIDSWITCH_OPERA = 53,
   SSP_BIDSWITCH_SPOTXCHANGE = 54,
   SSP_BIDSWITCH_UNRULY = 55,
   SSP_BIDSWITCH_YUME = 56,
   SSP_BIDSWITCH_AERSERV = 57,
   SSP_BIDSWITCH_MOBFOX = 58,
   SSP_BIDSWITCH_ADFORM = 59,
   SSP_NEXAGE_23 = 60,
   SSP_LIVERAIL = 61,
   SSP_BIDSWITCH_FALK_TECHNOLOGIES = 63,
   SSP_BIDSWITCH_MADS = 64,
   SSP_BIDSWITCH_RHYTHMONE = 65,
   SSP_BIDSWITCH_YIELDLAB = 66,
   SSP_MOPUB_23 = 67,
   SSP_BIDSWITCH_AMOBEE = 68,
   SSP_BIDSWITCH_TEADS = 69,
   SSP_BIDSWITCH_COULL = 70,
   SSP_BIDSWITCH_TREMOR_VIDEO = 71,
   SSP_BIDSWITCH_VDOPIA = 72,
   SSP_INNERACTIVE = 74,
   SSP_BIDSWITCH_AXONIX = 75,
   SSP_BIDSWITCH_SWITCHCONCEPTS = 76,
   SSP_PUBMATIC_23 = 78,
   SSP_BIDSWITCH_VRTCAL = 79,
   SSP_BIDSWITCH_POWERLINKS = 80,
   SSP_BIDSWITCH_LKQD = 81,
   SSP_BIDSWITCH_KIOSKED = 82,
   SSP_BIDSWITCH_STICKYADS_TV = 83,
   SSP_BIDSWITCH_SMARTADSERVER = 84,
   SSP_BIDSWITCH_OPTIMATIC = 85,
   SSP_SMAATO_HTML = 86
};


enum BlisHttpCode
{
   BLIS_HTTP_OK = 200,
   BLIS_HTTP_NO_CONTENT = 204,
   BLIS_HTTP_INTERNAL_SERVER_ERROR = 500
};


//
// Do not change these, ever.
//
enum HostedMatchDataSource
{
   HOSTED_MATCH_DATA_SOURCE_UNKNOWN = 0,
   HOSTED_MATCH_DATA_SOURCE_BSKYB = 0x82,
   HOSTED_MATCH_DATA_SOURCE_BSKYB_TEST = 0x83,
   HOSTED_MATCH_DATA_SOURCE_LOCATION = 0x84
};


enum NoBidReason
{
   BLIS_NBR_NONE = 0,   // It's a bid
   BLIS_NBR_UNKNOWN = 1,
   BLIS_NBR_NO_MORE_CAMPAIGNS = 2,
   BLIS_NBR_CACHE_UNAVAILABLE = 3,
   BLIS_NBR_TIMEOUT_EXCEEDED = 4,
   BLIS_NBR_ILLEGAL_REQUEST = 5,
   BLIS_NBR_NO_BUDGET = 6,
   BLIS_NBR_ADD_TO_BIDS_FAILED = 7,
   BLIS_NBR_DEDUCT_BUDGET_FAILED = 8,
   BLIS_NBR_UNMATCHED_IMPRESSION = 9,
   BLIS_NBR_NO_REQUEST_COUNTRY = 10,
   BLIS_NBR_REDIS_ERROR_FREQUENCY_CAPPING = 11,
   BLIS_NBR_UNSUPPORTED_DISPLAYMANAGER = 12,
   BLIS_NBR_TOO_MANY_BADV = 13,
   BLIS_NBR_NIL_AGENCY_BID_PRICE = 14,
   BLIS_NBR_NIL_OR_NEGATIVE_EXCHANGE_BID_PRICE = 15,
   BLIS_NBR_UNIDENTIFIED_PUBLISHER = 16,
   BLIS_NBR_CURRENCY_NOT_USD = 17,
   BLIS_NBR_NOT_SECOND_PRICE_AUCTION = 18,
   BLIS_NBR_DOUBLE_AUCTION_CANDIDATE = 19,
   BLIS_NBR_BE_NO_CREATIVE = 20,
   BLIS_NBR_DOUBLE_AUCTION_INITIATE_FETCH_CREATIVE = 21,
   BLIS_NBR_BE_INVALID_CREATIVE = 22,
   BLIS_NBR_UNKNOWN_SSP = 23,
   BLIS_NBR_NOT_ALLOWED = 24,
   BLIS_NBR_UNKNOWN_COUNTRY_CODE = 25,
   BLIS_NBR_DOUBLE_AUCTION_BID_FLOOR_MISMATCH = 26,
   BLIS_NBR_GETADTAG_FAILED = 27,
   BLIS_NBR_RESELLER_REQUEST = 28,
   BLIS_NBR_SSP_DISABLED = 29,
   BLIS_NBR_NO_BANNER_CHOSEN = 30,
   BLIS_NBR_REQUEST_INCOMPLETE = 31,
   BLIS_NBR_UNKNOWN_CREATIVE_ATTRIBUTE = 32,
   BLIS_NBR_UNSUPPORTED_FEATURE = 33,
   BLIS_NBR_NO_BID_OPTIONS = 34,
   BLIS_NBR_POTENTIAL_DA_INITIATE_FETCH_CREATIVE = 35,
   BLIS_NBR_BE_SINGLEAUCTION_BROKEN = 36,
   BLIS_NBR_REGULATED_REQUEST = 37,
   BLIS_NBR_AGE_UNDER_13 = 38,
   BLIS_NBR_BLACKLISTED_SOURCE_IP = 39,
   BLIS_NBR_DESERIALISER_FULL  = 40,
   BLIS_NBR_NOT_WHITELISTED = 41,
   BLIS_NBR_TRANSLATION_FAILED = 42,
   BLIS_NBR_NO_BUYER = 43,
   BLIS_NBR_SEND_AUCTION_FAILED = 44,
   BLIS_NBR_DP_AUCTION_QUEUE_FULL = 45,
   BLIS_NBR_DP_RESPONSE_MISSING_ADOMAIN = 46,
   BLIS_NBR_DP_RESPONSE_BLOCKED_ADVERTISER = 47,
   BLIS_NBR_DP_NO_CONNECTION_AVAILABLE = 48,
   BLIS_NBR_DP_COMMS_FAILURE = 49,
   BLIS_NBR_BUILD_REQUEST_EXCEPTION = 50,
   BLIS_NBR_SEND_AUCTION_EXCEPTION = 51,
   BLIS_NBR_INSUFFICIENT_TMAX = 52,
   BLIS_NBR_RANDOMLY_THROTTLED = 53,
   BLIS_NBR_MISSING_IP = 54
};


enum
{
   MICROS_PER_HOUR = 1000000LL * 60LL * 60LL
};

#pragma pack(push, 1)

struct BSkyBPayload
{
   uint8_t m_gender;
   uint8_t m_age;
   uint32_t m_venueId;     // Big endian.
   uint64_t m_timestamp;   // Big endian. Hours since epoch.
   uint8_t m_nonce[2];

   int64_t getTimestampMicros() const
   {
      return CPU::toLittleEndian(m_timestamp) * MICROS_PER_HOUR;
   }
};

#pragma pack(pop)

enum LocationMatchMethod
{
   LOCATION_MATCH_MISMATCH = -1,
   LOCATION_MATCH_UNKNOWN = 0,
   LOCATION_MATCH_IPRANGE = 1,
   LOCATION_MATCH_IP_TO_GEO = 2,
   LOCATION_MATCH_GEO = 3,
   //LOCATION_MATCH_ISP_VENUE_MATCH = 4,
   LOCATION_MATCH_ISP_GEO_MATCH = 5,
   LOCATION_MATCH_ADX_COOKIE_MATCHING_IP_MATCH = 6,
   LOCATION_MATCH_ADX_COOKIE_MATCHING_IP2GEO_MATCH = 7,
   LOCATION_MATCH_ADX_COOKIE_MATCHING_GEO_MATCH = 8,
   LOCATION_MATCH_REALTIME_REDIS = 9
};

//
// If more than eight value are required the DB schema needs
// to be changed from TINYINT to unsigned int.
//
enum BlisImpressionPosition
{
   BLIS_POSITION_ABOVE_THE_FOLD = (1 << 0),
   BLIS_POSITION_BELOW_THE_FOLD = (1 << 1),
   BLIS_POSITION_UNKNOWN = (1 << 2)
};

enum BlisImpressionFrame
{
   BLIS_IMPRESSION_FRAME_UNKNOWN = 0,
   BLIS_IMPRESSION_FRAME_TOPFRAME = 1,
   BLIS_IMPRESSION_FRAME_IFRAME = 2,
};

enum
{
   SHA1_DIGEST_SIZE = 20
};

typedef std::array<uint8_t, SHA1_DIGEST_SIZE> SHA1Digest;


enum BlisConnectionType
{
   BLIS_CONNECTION_TYPE_UNKNOWN = -1,
   BLIS_CONNECTION_TYPE_WIFI = 1,
   BLIS_CONNECTION_TYPE_ETHERNET = 2,
   BLIS_CONNECTION_TYPE_CELLULAR_UNKNOWN = 3,
   BLIS_CONNECTION_TYPE_CELLULAR_2G = 4,
   BLIS_CONNECTION_TYPE_CELLULAR_3G = 5,
   BLIS_CONNECTION_TYPE_CELLULAR_4G = 6
};


//////////////////////////////////////////////////////
// Meaning of bits
// ===============
// Bit 0 -> Mobile
// Bit 1 -> Uncategorised Wifi
// Bit 2 -> Residential Wifi
// Bit 3 -> Business WiFi
// Bit 4 -> Commercial WiFi
// Bit 7 -> Blis Location Item 
// =========================
// Supported Interpretations
// =========================
// 00000000 - 0 -> Unrestricted
// 00000001 - 1 -> Mobile Operator IP Range
// 00000010 - 2 -> Uncategorised WiFi IP Range
// 00000100 - 4 -> Residential WiFi IP Range
// 00001000 - 8 -> Business WiFi IP Range
// 00010000 - 16 -> Commercial WiFi
// 00001100 - 12 -> Residential Business WiFi IP Range
// 00011110 - 30 -> All WiFi
// 01000000 - 64 -> Blis Location Item IP Range VE Campaign
// 10000000 - 128 -> Blis Location Item IP Range Real Campaign
// 11000000 - 192 -> Blis Location Item IP Range (Any)
//////////////////////////////////////////////////////
enum BlisIPRangeType
{
   BLIS_IPRANGE_TYPE_UNRESTRICTED = 0,
   BLIS_OPERATOR_IPRANGE_TYPE_MOBILE = 1,
   BLIS_OPERATOR_IPRANGE_TYPE_UNCATEGORISED_WIFI = 2,
   BLIS_OPERATOR_IPRANGE_TYPE_RESIDENTIAL_WIFI = 4,
   BLIS_OPERATOR_IPRANGE_TYPE_BUSINESS_WIFI = 8,
   BLIS_OPERATOR_IPRANGE_TYPE_RESIDENTIAL_BUSINESS_WIFI = 12,
   BLIS_OPERATOR_IPRANGE_TYPE_COMMERCIAL_WIFI = 16,
   BLIS_OPERATOR_IPRANGE_TYPE_ALL_WIFI = 30,
   BLIS_IPRANGE_TYPE_LOCATION_ITEM_VE_CAMPAIGN = 64,
   BLIS_IPRANGE_TYPE_LOCATION_ITEM_REAL_CAMPAIGN = 128,
   BLIS_IPRANGE_TYPE_LOCATION_ITEM = BLIS_IPRANGE_TYPE_LOCATION_ITEM_VE_CAMPAIGN | BLIS_IPRANGE_TYPE_LOCATION_ITEM_REAL_CAMPAIGN
};

enum DebugRequestType
{
   DEBUG_REQUEST_ALL   = 0,
   DEBUG_REQUEST_VIDEO = 1
};

enum BlisISPTargets
{
   BLIS_ISP_TARGET_NONE = 0,
   BLIS_ISP_TARGET_BSKYB = 2,
   BLIS_ISP_TARGET_ALL = 3
};


enum ExpandingDirectionFlag
{
   EXPANDING_FLAG_NONE =               0,
   EXPANDING_FLAG_UP =                 (1 << 0),   // Up only.
   EXPANDING_FLAG_DOWN =               (1 << 1),   // Down only.
   EXPANDING_FLAG_LEFT =               (1 << 2),   // Left only.
   EXPANDING_FLAG_RIGHT =              (1 << 3),   // Right only.
   EXPANDING_FLAG_UP_LEFT =            (1 << 4),   // Up and Left simultaneously.
   EXPANDING_FLAG_UP_RIGHT =           (1 << 5),   // Up and Right simultaneously.
   EXPANDING_FLAG_DOWN_LEFT =          (1 << 6),   // Down and Left simultaneously.
   EXPANDING_FLAG_DOWN_RIGHT =         (1 << 7),   // Down and Right simultaneously.
   EXPANDING_FLAG_UP_OR_DOWN =         (1 << 8),   // Up or down, decided at render time.
   EXPANDING_FLAG_LEFT_OR_RIGHT =      (1 << 9),   // Left or right, decided at render time.
   EXPANDING_FLAG_ANY_DIAGONAL =       (1 << 10),  // Any diagonal direction, decided at render time.
   
   // Helper definitions.
   EXPANDING_ALL_LEFT = EXPANDING_FLAG_LEFT | EXPANDING_FLAG_UP_LEFT | EXPANDING_FLAG_DOWN_LEFT,
   EXPANDING_ALL_RIGHT = EXPANDING_FLAG_RIGHT | EXPANDING_FLAG_UP_RIGHT | EXPANDING_FLAG_DOWN_RIGHT,
   EXPANDING_ALL_UP = EXPANDING_FLAG_UP | EXPANDING_FLAG_UP_LEFT | EXPANDING_FLAG_UP_RIGHT,
   EXPANDING_ALL_DOWN = EXPANDING_FLAG_DOWN | EXPANDING_FLAG_DOWN_LEFT | EXPANDING_FLAG_DOWN_RIGHT,
   
   EXPANDING_ALL_VERTICAL = EXPANDING_FLAG_UP | EXPANDING_FLAG_DOWN | EXPANDING_FLAG_UP_OR_DOWN,
   EXPANDING_ALL_HORIZONTAL = EXPANDING_FLAG_LEFT | EXPANDING_FLAG_RIGHT | EXPANDING_FLAG_LEFT_OR_RIGHT,
   EXPANDING_ALL_DIAGONAL = EXPANDING_FLAG_UP_LEFT | EXPANDING_FLAG_UP_RIGHT | EXPANDING_FLAG_DOWN_LEFT | EXPANDING_FLAG_DOWN_RIGHT | EXPANDING_FLAG_ANY_DIAGONAL,
      
   EXPANDING_DEFAULT = EXPANDING_ALL_VERTICAL | EXPANDING_ALL_HORIZONTAL | EXPANDING_ALL_DIAGONAL
};

typedef uint16_t ExpandingDirectionBitField;


enum CookieMatchingOptions
{
   CM_OPTIONS_NONE = 0,
   CM_OPTIONS_ADX = (1 << 0)
};

typedef uint32_t LocationItemId;
const LocationItemId UNKNOWN_LOCATION_ITEM_ID = 0;

typedef uint32_t IP4Address;

typedef std::vector<LocationItemId> LocationItemIdsVector;
typedef boost::unordered_set<LocationItemId> LocationItemIdsSet;

using CampaignId = unsigned int;

using DistMeters = unsigned int;

struct LocationInfo
{
   LocationInfo() = default;

   LocationInfo(LocationItemId liid, std::vector<CampaignId>&& cids)
      : m_locationItemId(liid),
        m_campaignIds(std::move(cids))
   {
   }

   LocationItemId m_locationItemId;
   std::vector<CampaignId> m_campaignIds;
};

enum BlisAuctionType
{
   BLIS_AUCTION_TYPE_UNKNOWN = 0,
   BLIS_AUCTION_TYPE_FIRST_PRICE = 1,
   BLIS_AUCTION_TYPE_SECOND_PRICE = 2,
   BLIS_AUCTION_TYPE_FIXED_PRICE = 3
};


enum CampaignType : uint16_t
{
   CAMPAIGN_TYPE_UNKNOWN = 0,
   CAMPAIGN_TYPE_STANDARD = (1 << 0),
   CAMPAIGN_TYPE_ADDED_VALUE = (1 << 1),
   CAMPAIGN_TYPE_TRADING_DEAL = (1 << 2),
   CAMPAIGN_TYPE_MARKETING = (1 << 3),
   CAMPAIGN_TYPE_TEST = (1 << 4),
   CAMPAIGN_TYPE_GHOST = (1 << 5),
   CAMPAIGN_TYPE_BLIS_EXCHANGE_PMP = (1 << 6),
   CAMPAIGN_TYPE_BLIS_EXCHANGE_OMP = (1 << 7),

   // Helpers.
   CAMPAIGN_TYPE_REAL = CAMPAIGN_TYPE_STANDARD | CAMPAIGN_TYPE_ADDED_VALUE | CAMPAIGN_TYPE_TRADING_DEAL | CAMPAIGN_TYPE_MARKETING | CAMPAIGN_TYPE_TEST,
   CAMPAIGN_TYPE_BLIS_EXCHANGE_ANY = CAMPAIGN_TYPE_BLIS_EXCHANGE_OMP | CAMPAIGN_TYPE_BLIS_EXCHANGE_PMP,
   CAMPAIGN_TYPE_ALL = CAMPAIGN_TYPE_BLIS_EXCHANGE_ANY | CAMPAIGN_TYPE_REAL | CAMPAIGN_TYPE_GHOST
};


inline CampaignType campaignTypeFromString(StringRef ct)
{
   if (ct == "standard") return CAMPAIGN_TYPE_STANDARD;
   if (ct == "added value") return CAMPAIGN_TYPE_ADDED_VALUE;
   if (ct == "trading deal") return CAMPAIGN_TYPE_TRADING_DEAL;
   if (ct == "marketing") return CAMPAIGN_TYPE_MARKETING;
   if (ct == "ghost") return CAMPAIGN_TYPE_GHOST;
   if (ct == "test") return CAMPAIGN_TYPE_TEST;
   if (ct == "demand open market") return CAMPAIGN_TYPE_BLIS_EXCHANGE_OMP;
   if (ct == "demand audience pmp") return CAMPAIGN_TYPE_BLIS_EXCHANGE_PMP;

   return CAMPAIGN_TYPE_UNKNOWN;
}

namespace std
{

inline StringRef to_string(CampaignType ct)
{
   switch (ct)
   {
   case CAMPAIGN_TYPE_STANDARD: return "standard";
   case CAMPAIGN_TYPE_ADDED_VALUE: return "added value";
   case CAMPAIGN_TYPE_TRADING_DEAL: return "trading deal";
   case CAMPAIGN_TYPE_MARKETING: return "marketing";
   case CAMPAIGN_TYPE_GHOST: return "ghost";
   case CAMPAIGN_TYPE_TEST: return "test";
   case CAMPAIGN_TYPE_BLIS_EXCHANGE_OMP: return "demand open market";
   case CAMPAIGN_TYPE_BLIS_EXCHANGE_PMP: return "demand audience pmp";

   case CAMPAIGN_TYPE_UNKNOWN:
   case CAMPAIGN_TYPE_REAL:
   case CAMPAIGN_TYPE_BLIS_EXCHANGE_ANY:
   case CAMPAIGN_TYPE_ALL:
      // A campaign should never be set to these values.
      break;
   }

   return "*** Invalid CampaignType value ***";
}

}  // namespace std

using CampaignTypeBitField = std::underlying_type<CampaignType>::type;

enum
{
   NO_CHOSEN_INDEX = static_cast<std::size_t>(-1)
};


enum CreativeSource
{
   CREATIVE_SOURCE_UNKNOWN = 0,
   CREATIVE_SOURCE_BLIS = 1,
   CREATIVE_SOURCE_DA = 2,
   CREATIVE_SOURCE_SA = 3
};


enum VideoOrientation
{
   VIDEO_ORIENTATION_UNKNOWN,
   VIDEO_ORIENTATION_LANDSCAPE,
   VIDEO_ORIENTATION_PORTRAIT,
   VIDEO_ORIENTATION_BOTH
};


namespace std
{

inline const char* to_string(CreativeSource cs)
{
   switch (cs)
   {
   case CREATIVE_SOURCE_UNKNOWN: return "UNKNOWN";
   case CREATIVE_SOURCE_BLIS: return "BLIS";
   case CREATIVE_SOURCE_DA: return "DA";
   case CREATIVE_SOURCE_SA: return "SA";
   }
   
   return "UNKNOWN";
}

}  // namespace std

enum RequestTarget
{
   RT_NONE,
   RT_DSP_ONLY,
   RT_BE_PMP_ONLY,
   RT_DSP_PMP_THEN_BE_PMP,
   RT_DSP_PMP_THEN_BE_OMP,
   RT_DSP_OMP_THEN_BE_OMP
};

namespace std
{

inline const char* to_string(RequestTarget rt)
{
   switch (rt)
   {
   case RT_NONE: return "RT_NONE";
   case RT_DSP_ONLY: return "RT_DSP_ONLY";
   case RT_BE_PMP_ONLY: return "RT_BE_PMP_ONLY";
   case RT_DSP_PMP_THEN_BE_PMP: return "RT_DSP_PMP_THEN_BE_PMP";
   case RT_DSP_PMP_THEN_BE_OMP: return "RT_DSP_PMP_THEN_BE_OMP";
   case RT_DSP_OMP_THEN_BE_OMP: return "RT_DSP_OMP_THEN_BE_OMP";
   }
   
   return "RT_UNKNOWN";
}

}  // namespace std

using RetargetingFunctionId = int32_t; // This matches what Cassandra uses.
using BookingId = unsigned int;
using EligibleCampaigns = std::vector<CampaignId>; 
using BookingIdSet = std::set<BookingId>;


enum DemandPartnerId
{
   DEMAND_PARTNER_UNKNOWN = 0,
   DEMAND_PARTNER_BLIS = 1,
   DEMAND_PARTNER_PUBMATIC = 2,
   DEMAND_PARTNER_MOBFOX = 3
};

inline BlisAdType convertToBlisAdType(StringRef mimeStr)
{
   static const std::pair<StringRef, BlisAdType> mimes[] = {
      { "image",       BLIS_AD_TYPE_IMAGE },
      { "mraid",       BLIS_AD_TYPE_MRAID },
      { "javascript",  BLIS_AD_TYPE_JAVASCRIPT },
      { "html",        BLIS_AD_TYPE_TEXT },
      { "vast",        BLIS_AD_TYPE_VAST },
      { "native",      BLIS_AD_TYPE_NATIVE },
      { "vpaid-flash", BLIS_AD_TYPE_VPAID_FLASH },
      { "vpaid-js",    BLIS_AD_TYPE_VPAID_JS }
   };

   for (const auto& mime : mimes)
   {
      if (mime.first == mimeStr)
      {
         return mime.second;
      }
   }

   return BLIS_AD_TYPE_UNKNOWN;
}

