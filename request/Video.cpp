#include "Video.h"

namespace blis
{

Video::Video()
:  m_min_duration(0),
   m_max_duration(0),
   m_max_skippable_duration(0),
   m_start_delay(0),
   m_skip_options(BLIS_SKIPPABLE_UNKNOWN),
   m_allowedVASTProtocolsBitSet(0),
   m_width(0),
   m_height(0),
   m_position(Video::VideoPosition::BLIS_VIDEO_POSITION_UNKNOWN),
   m_minBitRate(0),
   m_maxBitRate(0),
   m_linearity(Video::VideoLinearity::BLIS_VIDEO_LINEARITY_UNKNOWN),
   m_rewarded(false)
{
}
   
//
// Set from our namespace.
//
void Video::setSkipOptions(blis::Video::VideoSkipOptions options)
{
   m_skip_options = options;
}

void Video::addAllowedResponseVASTProtocol(VideoBidResponseProtocol protocol)
{
   m_allowedVASTProtocolsBitSet |= 1 << protocol;
}

bool Video::isAllowedResponseVASTProtocol(VideoBidResponseProtocol protocol) const
{
   return ((m_allowedVASTProtocolsBitSet >> protocol) & 1);
}

bool Video::isProtocolInfoAvailable() const
{
   return m_allowedVASTProtocolsBitSet > 0;
}

bool Video::isVersionAllowed(const VersionInfo& version) const
{
   bool rc = false;
   if (isAllowedResponseVASTProtocol(BLIS_VAST_1_0) || 
       isAllowedResponseVASTProtocol(BLIS_VAST_1_0_WRAPPER))
   {
      VersionInfo to_compare(1, 0, 0);
      rc = (version <= to_compare);
   }

   if (!rc &&
       (isAllowedResponseVASTProtocol(BLIS_VAST_2_0) ||
        isAllowedResponseVASTProtocol(BLIS_VAST_2_0_WRAPPER)))
   {
      VersionInfo to_compare(2, 0, 0); 
      rc = (version <= to_compare);
   }

   if (!rc &&
       (isAllowedResponseVASTProtocol(BLIS_VAST_3_0) ||
        isAllowedResponseVASTProtocol(BLIS_VAST_3_0_WRAPPER)))
   {
      VersionInfo to_compare(3, 0, 0);
      rc = (version <= to_compare);
   }
 
   return rc;
}
 
void Video::addPlaybackMethod(VideoPlaybackMethod method)
{
   if (method != Video::VideoPlaybackMethod::BLIS_UNKNOWN_PLAYBACK_METHOD)
   {
      m_playbackMethods.push_back(method);
   }
}

}  // namespace blis


