#pragma once

#include "AdX.h"
#include "BidSwitch.h"
#include "BlisTypes.h"

class ExpandableCapabilities
{
public:
   
   ExpandableCapabilities()
   :  m_capabilities(EXPANDING_DEFAULT)
   {
   }
   
   void block(AdX::CreativeAttributes direction)
   {
      switch (direction)
      {
      case AdX::ExpandingDirection_Up:
         return unset(EXPANDING_FLAG_UP);
                  
      case AdX::ExpandingDirection_Down:
         return unset(EXPANDING_FLAG_DOWN);
         
      case AdX::ExpandingDirection_Left:
         return unset(EXPANDING_FLAG_LEFT);
         
      case AdX::ExpandingDirection_Right:
         return unset(EXPANDING_FLAG_RIGHT);
         
      case AdX::ExpandingDirection_UpLeft:
         return unset(EXPANDING_FLAG_UP_LEFT);
         
      case AdX::ExpandingDirection_UpRight:
         return unset(EXPANDING_FLAG_UP_RIGHT);
         
      case AdX::ExpandingDirection_DownLeft:
         return unset(EXPANDING_FLAG_DOWN_LEFT);
         
      case AdX::ExpandingDirection_DownRight:
         return unset(EXPANDING_FLAG_DOWN_RIGHT);
         
      case AdX::ExpandingDirection_UpOrDown:
         return unset(EXPANDING_ALL_VERTICAL);
         
      case AdX::ExpandingDirection_LeftOrRight:
         return unset(EXPANDING_ALL_HORIZONTAL);
         
      case AdX::ExpandingDirection_AnyDiagonal:
         return unset(EXPANDING_ALL_DIAGONAL);
         
      default:
         return;
      }
   }
   
   
   void block(BidSwitch::ExpandingDirection direction)
   {
      switch (direction)
      {
      case BidSwitch::EXPANDING_LEFT:
         return unset(EXPANDING_ALL_LEFT);
         
      case BidSwitch::EXPANDING_RIGHT:
         return unset(EXPANDING_ALL_RIGHT);
         
      case BidSwitch::EXPANDING_UP:
         return unset(EXPANDING_ALL_UP);
         
      case BidSwitch::EXPANDING_DOWN:
         return unset(EXPANDING_ALL_DOWN);
         
      case BidSwitch::EXPANDING_FULLSCREEN:
         return unset(EXPANDING_ALL_DIAGONAL);
         
      default:
         return;
      }
   }


   bool isBlocked(ExpandingDirectionFlag direction) const
   {
      return ((m_capabilities & direction) == 0);
   }

public:

   void unset(ExpandingDirectionFlag direction)
   {
      m_capabilities &= ~direction;
   }

   ExpandingDirectionBitField m_capabilities;
};
