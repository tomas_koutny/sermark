#pragma once

#include <vector>

#include "BidOption.h"
#include "ExpandableCapabilities.h"
#include "Native.h"

#include "BlisTypes.h"
#include "UnownedPtr.h"

/**
 * @brief Represents a single impression within the bid request
 */
class Impression
{
public:

   // keeping this just because of specific usage in the Publisher
   struct PrivateDeal
   {
      PrivateDeal()
      : m_bidFloor(0.0f)
      {
      }

      PrivateDeal(const std::string& id, float bidFloor)
      : m_bidFloor(bidFloor),
        m_id(id)
      {
      }

      float m_bidFloor;
      std::string m_id;
   };

   Impression()
   :  m_blockedCreativeAttributes(0),
      m_privateDealIndex(NO_CHOSEN_INDEX),
      m_pmpOnly(false),
      m_bid_floor(0.0f),
      m_billing_id(0),
      m_onlySSL(false),
      m_nativeBrowserClick(false),
      m_isInterstitial(false),
      m_lowestAvailableBidFloor(0.0f),
      m_position(BLIS_POSITION_UNKNOWN),
      m_frame(BLIS_IMPRESSION_FRAME_UNKNOWN),
      m_index(std::numeric_limits<std::size_t>::max())
   {
   }

   void setSSPDefaults(SSPId sspId)
   {
      switch (sspId)
      {
      case SSP_MOPUB_23:
         addBlockedCreativeAttribute(CA_FLAG_POP);
         addBlockedCreativeAttribute(CA_FLAG_WINDOWS_DIALOG_ALERT);
         break;

      default:
         break;
      }
   }

   std::string getBlockedAdTypes() const
   {
      return blis::getCommaSeparatedString(m_blockedAdTypes);
   }

   std::string getAllowedAndNotBlockedAdTypes(bool isApp) const
   {
      const std::set<BlisAdType>& allowedAdTypes = isApp ? m_allowedAppAdTypes : m_allowedSiteAdTypes;
      std::set<BlisAdType> diff;
      std::set_difference(allowedAdTypes.begin(), allowedAdTypes.end(),
                          m_blockedAdTypes.begin(), m_blockedAdTypes.end(),
                          std::inserter(diff, diff.begin()));
      return blis::getCommaSeparatedString(diff);
   }

   void addBlockedAdType(BlisAdType blockedAdType)
   {
      m_blockedAdTypes.insert(blockedAdType);
   }
   
   void addBlockedCreativeAttribute(CreativeAttributeFlag attribute)
   {
      m_blockedCreativeAttributes |= attribute;
   }
   
   bool hasBlockedCreativeAttribute(CreativeAttributeBitField attribute) const
   {
      return (m_blockedCreativeAttributes & attribute) != 0;
   }
   
   void addAllowedAppAdType(BlisAdType adType)
   {
      m_allowedAppAdTypes.insert(adType);
   }

   void addAllowedSiteAdType(BlisAdType adType)
   {
      m_allowedSiteAdTypes.insert(adType);
   }

   bool isBlockedAdType(BlisAdType mime_type) const
   {
      return (m_blockedAdTypes.find(mime_type) != m_blockedAdTypes.end());
   }

   bool isAdTypeAllowed(BlisAdType adType, bool isApp) const
   {
      const std::set<BlisAdType>& allowedAdTypes = isApp ? m_allowedAppAdTypes : m_allowedSiteAdTypes;
      return (allowedAdTypes.find(adType) != allowedAdTypes.end());
   }

   int64_t getBillingId() const
   {
      return m_billing_id;
   }

   const std::string& getId() const
   {
      return m_impression_id;
   }

   void setDisplayManager(const std::string& displayManager)
   {
      m_displayManager = displayManager;
   }

   const std::string& getDisplayManager() const
   {
      return m_displayManager;
   }
   
   void setDisplayManagerVersion(const std::string& version)
   {
      m_displayManagerVersion = version;
   }

   const VersionInfo& getDisplayManagerVersion() const
   {
      return m_displayManagerVersion;
   }
   
   void setPosition(BlisImpressionPosition position)
   {
      m_position = position;
   }


   BlisImpressionPosition getPosition() const
   {
      return m_position;
   }

   bool hasOMPBidOption() const
   {
      bool result = false;

      if (!m_bidOptions.empty() && m_bidOptions.back().isOMP())
      {
         result = true;
      }

      return result;
   }

   void setOMPBidFloor(float bidFloor)
   {
      // hate this, but it prevents invariants from being broken when someone
      // calls this function after bid options were created
      auto ompBO = findOMPBidOption();
      if (ompBO != nullptr)
      {
         BidOption& ompbo = const_cast<BidOption&>(*ompBO);
         ompbo.setBidFloor(bidFloor);
      }

      m_bid_floor = bidFloor;

      updateLowestAvailableBidFloor(m_bid_floor);
   }

   float getOMPBidFloor() const
   {
      return m_bid_floor;
   }

   float getLowestAvailableBidFloor() const
   {
      return m_lowestAvailableBidFloor;
   }

   const std::string getReceivedDealIds() const
   {
      auto pmpDealIdsSerializer = [](const std::vector<BidOption>& bidOptions) -> std::string {
         auto length = std::accumulate(bidOptions.begin(), bidOptions.end(), std::string::size_type(0),
            [](std::string::size_type n, const BidOption& bidOption) {
               return (bidOption.isPMP() ? (n + bidOption.getPMPDealId().size() + 1) : n);
            }
         );

         std::string serialized;
         serialized.reserve(length);
 
         for (const auto& bidOption : bidOptions)
         {
            if (bidOption.isPMP())
            {
               if (serialized.empty())
               {
                  serialized.append(bidOption.getPMPDealId());
               }
               else
               {
                  serialized.append(",");
                  serialized.append(bidOption.getPMPDealId());
               }
            }
         }
         return serialized;
      }; 
      
      return pmpDealIdsSerializer(getBidOptions());
   }

   std::string m_impression_id;
   std::vector<std::pair<unsigned int, unsigned int>> m_allowed_dimensions;
   std::set<BlisAdType> m_blockedAdTypes;
   CreativeAttributeBitField m_blockedCreativeAttributes;
   std::set<BlisAdType> m_allowedAppAdTypes;
   std::set<BlisAdType> m_allowedSiteAdTypes;
   std::string m_blisOptimisationId;

   ////////////////////////////////////////////////////////////////////////////
   // ! m_privateDeals and m_privateDealIndex are public just because of
   // ! boost::serialization. It is ok to read them but NEVER modify them
   // ! directly - use addBidOption and  setChosenBidOptionIndex instead.
   ////////////////////////////////////////////////////////////////////////////

   // this is legacy member, only legal to use in the Publisher
   std::vector<PrivateDeal> m_privateDeals;
   // Impression object shouldn't really know what deal was chosen
   std::size_t m_privateDealIndex;

   // keep it public for now because it has nothing to do in this class in the long run anyway
   std::vector<PMPDealParsedParams> m_pmpDealsParsedParams;

   bool m_pmpOnly;

   // NEVER modify this field directly, use setOMPBidFloor instead.
   // The only reason it stays public is boost::serialization.
   float m_bid_floor; 

   int64_t m_billing_id; // AdX specific.

   bool m_onlySSL;

   bool m_nativeBrowserClick;
   bool m_isInterstitial;

   bool hasNativeBrowserClick() const
   {
      return m_nativeBrowserClick;
   }

   void setNativeBrowserClick(bool flag)
   { 
      m_nativeBrowserClick = flag;
   }

   bool isInterstitial() const
   {
      return m_isInterstitial;
   }

   void setInterstitial(bool flag = true)
   {
      m_isInterstitial = flag;
   }

   void setOnlySSL()
   {
      m_onlySSL = true;
   }

   bool onlySSL() const
   {
      return m_onlySSL;
   }

   void setFrame(BlisImpressionFrame frame)
   {
      m_frame = frame;
   }

   BlisImpressionFrame getFrame() const
   {
      return m_frame;
   }

   void blockExpandingDirection(AdX::CreativeAttributes direction)
   {
      m_expandableCaps.block(direction);
   }
   
   void blockExpandingDirection(BidSwitch::ExpandingDirection direction)
   {
      m_expandableCaps.block(direction);
   }

   bool isBlockedExpandingDirection(ExpandingDirectionFlag direction) const
   {
      return m_expandableCaps.isBlocked(direction);
   }

   const ExpandableCapabilities& getExpandableCapabilities() const
   {
      return m_expandableCaps;
   }

   float getBidFloor() const
   {
      return getOMPBidFloor();
   }

   void generateBlisOptimisationId(const std::string& blisPublisherId, const std::string& country)
   {
      // Make sure all fields used for id generation are in the bid/nobid logs.

      std::string key;
      
      key.reserve(1024);
      
      key += blisPublisherId;             // BidLogField: 50, NoBidLogField: 43
      key += country;                     // BidLogField: 5, NoBidLogField: 5

      if (!m_allowed_dimensions.empty())
      {
         const std::pair<unsigned int, unsigned int>& size = m_allowed_dimensions[0];
         
         key += std::to_string(size.first);  // BidLogField: 9, NoBidLogField: 9
         key += std::to_string(size.second); // BigLogField: 10, NoBidLogField: 10
      }
            
      m_blisOptimisationId = blis::SHA1(key);
   }


   const std::string& getBlisOptimisationId() const
   {
      if (m_blisOptimisationId.empty())
      {
         throw std::runtime_error("BlisImpressionId has not been generated");
      }
      
      return m_blisOptimisationId;
   }
   
   
   bool hasBlisOptimisationId() const noexcept
   {
      return !m_blisOptimisationId.empty();
   }

   void setPMPOnly(bool pmpOnly)
   {
      m_pmpOnly = pmpOnly;
   }

   bool isPMPOnly() const
   {
      return m_pmpOnly;
   }

   // note no exception safety here - but this is fine as whole bid request
   // processing is cancelled when this function throws

   void addBidOption(std::unique_ptr<BidOption>&& bo)
   {
      // TODO get rid of legacy vector and introduce BO ordering, this indexing is root of evil
      if (bo != nullptr)
      {
         if (bo->isOMP() && hasOMPBidOption())
         {
            throw std::runtime_error(
               "Impression::addBidOption: OMP BO already present in this Impression object."
            );
         }

         float bidFloor = bo->getBidFloor();

         if (hasOMPBidOption())
         {
            m_privateDeals.insert(m_privateDeals.end() - 1, PrivateDeal(bo->getPMPDealId(), bo->getBidFloor()));
            m_bidOptions.insert(m_bidOptions.end() - 1, std::move(*bo));
         }
         else
         {
            // we need to keep the same indexing in this legacy m_privateDeals vector
            m_privateDeals.push_back(PrivateDeal(bo->getPMPDealId(), bo->getBidFloor()));
            m_bidOptions.push_back(std::move(*bo));
         }

         updateLowestAvailableBidFloor(bidFloor);

         UnownedPtr<BidOption> ompBO = findOMPBidOption();
         if (ompBO != nullptr)
         {
            m_bid_floor = ompBO->getBidFloor();
         }
      }
   }

   bool isOMPEnabled() const
   {
      return !isPMPOnly();
   }

   std::size_t getPMPBidOptionsCount() const
   {
      std::size_t pmpBOCount = 0;
      auto boCount = getBidOptionsCount();

      if (boCount > 0)
      {
         pmpBOCount = (boCount - (hasOMPBidOption() ? 1 : 0));
      }

      return pmpBOCount;
   }

   bool hasPMPBidOptions() const
   {
      return (getPMPBidOptionsCount() > 0);
   }

   unsigned int getBidOptionsCount() const
   {
      return m_bidOptions.size();
   }

   bool hasBidOptions() const
   {
      return (getBidOptionsCount() > 0);
   }

   const std::vector<BidOption>& getBidOptions() const
   {
      return m_bidOptions;
   }
   
   // As commented on m_privateDealIndex, this shouldn't really be here
   void setChosenBidOptionIndex(std::size_t index)
   {
      m_privateDealIndex = index;
   }

   // As commented on m_privateDealIndex, this shouldn't really be here
   UnownedPtr<BidOption> getChosenBidOption() const
   {
      UnownedPtr<BidOption> chosen;

      if (m_privateDealIndex != NO_CHOSEN_INDEX)
      {
         chosen.reset(&(m_bidOptions[m_privateDealIndex]));
      }

      return chosen;
   }

   UnownedPtr<BidOption> findOMPBidOption() const
   {
      UnownedPtr<BidOption> result;

      if (hasOMPBidOption())
      {
         result.reset(&(m_bidOptions.back()));
      }

      return result;
   }
   
   float getFloorToUse() const
   {
      UnownedPtr<BidOption> chosen = getChosenBidOption();
      if (chosen)
      {
         return chosen->getBidFloor();
      }
      
      return m_bid_floor;
   }

   bool isNative() const
   {
      return !m_nativeAssetSlots.empty();
   }

   const std::vector<NativeAssetSlot>& getNativeAssetSlots() const
   {
      return m_nativeAssetSlots;
   }

   void addNativeAssetSlot(NativeAssetSlot&& slot)
   {
      auto existIt = std::find_if(
         m_nativeAssetSlots.cbegin(),
         m_nativeAssetSlots.cend(),
         [&slot](const NativeAssetSlot& existing) {
            return existing.getId() == slot.getId();
         }
      );

      if (existIt != m_nativeAssetSlots.cend())
      {
         throw std::runtime_error("Native asset rewrite attempted");
      }

      m_nativeAssetSlots.push_back(std::move(slot));
   }

   void setIndex(std::size_t index)
   {
      m_index = index;
   }

   std::size_t getIndex() const
   {
      return m_index;
   }

   void buildBidOptions(const std::string& dspSeat = "")
   {
      for (const auto& params : m_pmpDealsParsedParams)
      {
         std::unique_ptr<BidOption> bidOption = BidOption::buildPMPBidOption(params);

         if (bidOption != nullptr)
         {
            addBidOption(std::move(bidOption));
         }
      }

      m_pmpDealsParsedParams.clear();

      if (!isPMPOnly())
      {
         addBidOption(BidOption::buildOMPBidOption(getOMPBidFloor(), dspSeat));
      }
   }

private:
   void updateLowestAvailableBidFloor(float candidate)
   {
      if (m_lowestAvailableBidFloor == 0.0f || m_lowestAvailableBidFloor > candidate)
      {
         m_lowestAvailableBidFloor = candidate;
      }
   }
   
   float m_lowestAvailableBidFloor;

   std::string m_displayManager;
   VersionInfo m_displayManagerVersion;

   BlisImpressionPosition m_position;
   BlisImpressionFrame m_frame;
   ExpandableCapabilities m_expandableCaps;
   std::vector<BidOption> m_bidOptions;
   std::vector<NativeAssetSlot> m_nativeAssetSlots;
   std::size_t m_index;
};
