#pragma once

#include <string>

struct RegionalInfo
{
   std::string countryAlpha3;
   std::string regionOrState;
   std::string city;
   std::string zip;
   std::string metro;
};
