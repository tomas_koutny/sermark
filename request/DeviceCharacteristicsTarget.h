#pragma once

#include <string>
#include "VersionInfo.h"
#include "Device.h"

class DeviceCharacteristicsTarget
{
public:
   enum VersionOperator
   {
      OP_NONE = 0,
      OP_EQUAL_TO = 1,
      OP_LESS_THAN = 2,
      OP_GREATER_THAN = 3,
      OP_LESS_THAN_OR_EQUAL = 4,
      OP_GREATER_THAN_OR_EQUAL = 5
   };

   DeviceCharacteristicsTarget();
   
   DeviceCharacteristicsTarget(
      const std::string& make,
      const std::string& model,
      const std::string& os,
      const std::string& osVersion,
      const std::string& osVersionOperator,
      Device::DeviceTypeBitField deviceType = Device::DeviceType::ALL
   );
   
   bool hasTargets() const;
   bool matchesMake(const std::string& make) const;
   bool matchesModel(const std::string& model) const;
   bool matchesOS(const std::string& os) const;
   bool matchesOSVersion(const std::string& osVersion) const;
   bool matchesOSVersion(const VersionInfo& versionInfo) const;
   bool matchesDeviceType(Device::DeviceType deviceType) const;

   static VersionOperator toOperator(const std::string& op);
   
private:   
   bool compareTarget(const std::string& field, const std::string& value) const;
   
   bool m_hasTargets;
   bool m_hasMakeAndModel;

   std::string m_make;
   std::string m_model;
   std::string m_os;
   VersionInfo m_osVersion;
   VersionOperator m_versionOperator;
   Device::DeviceTypeBitField m_deviceTypeBitField;
};
