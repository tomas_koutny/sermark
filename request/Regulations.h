#pragma once

#include "Util.h"

class Regulations
{
public:

   using RegulationsBitField = uint8_t;
   
   //
   // These are serialised so do not change any existing values.
   //
   enum Regulation : RegulationsBitField
   {
      NONE = 0,
      COPPA = 1 << 0,
      LMT = 1 << 1,
      DNT = 1 << 2
   };
   
 
   Regulations() noexcept
   :  m_regulations(NONE) 
   {
   }


   Regulations(RegulationsBitField regulations) noexcept
   :  m_regulations(regulations)
   {
   }


   RegulationsBitField get() const noexcept
   {
      return m_regulations;
   }


   bool operator==(Regulation regulation) const noexcept
   {
      return m_regulations == regulation;
   }


   bool operator!=(Regulation regulation) const noexcept
   {
      return !this->operator==(regulation);
   }


   void add(Regulation regulation) noexcept
   {
      m_regulations |= regulation;
   }


   bool empty() const noexcept
   {
      return m_regulations == NONE;
   }


   bool hasAny() const noexcept
   {
      return !empty();
   }
   
   
   bool has(Regulation regulation) const noexcept
   {
      return blis::isInBitField(m_regulations, regulation);
   }


   bool hasCOPPA() const noexcept
   {
      return blis::isInBitField(m_regulations, COPPA);
   }


   bool hasDNT() const noexcept
   {
      return blis::isInBitField(m_regulations, DNT);
   }


   bool hasLMT() const noexcept
   {
      return blis::isInBitField(m_regulations, LMT);
   }
   
public:
   RegulationsBitField m_regulations;  // Needs to be public for serialization.
};

