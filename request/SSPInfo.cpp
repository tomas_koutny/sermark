#include "SSPInfo.h"


SSPInfo::SSPInfo(SSPId id, const std::string& name)
:  m_id(id),
   m_name(name),
   m_tmax(0),
   m_useFloorInBidHash(false)
{
   reset();
}


SSPInfo::SSPInfo()
:  SSPInfo(SSP_UNKNOWN, "")
{
}


SSPId SSPInfo::getId() const
{
   return m_id;
}


void SSPInfo::setId(SSPId id, bool useDefaults)
{
   m_id = id;
   if (useDefaults)
   {
      reset();
   }
}


const std::string& SSPInfo::getName() const
{
   return m_name;
}


void SSPInfo::setName(const std::string& name)
{
   m_name = name;
}


BlisHttpCode SSPInfo::getNoBidHttpCode() const
{
   return m_noBidHttpCode;
}


void SSPInfo::setNoBidHttpCode(BlisHttpCode code)
{
   m_noBidHttpCode = code;
}


void SSPInfo::setNoBidResponseBody(const std::string& body)
{
   m_noBidResponseBody = body;
}


const std::string& SSPInfo::getNoBidResponseBody() const
{
   return m_noBidResponseBody;
}


void SSPInfo::setImpressionIsWin(bool impressionIsWin)
{
   m_impressionIsWin = impressionIsWin;
}


bool SSPInfo::impressionIsWin() const
{
   return m_impressionIsWin;
}


void SSPInfo::setSeatId(const std::string& seatId)
{
   m_seatId = seatId;
}


const std::string& SSPInfo::getSeatId() const
{
   return m_seatId;
}


bool SSPInfo::hasSeatId() const
{
   return !m_seatId.empty();
}


void SSPInfo::setBlisExchangeAllowed(bool allowed)
{
   m_blisExchangeAllowed = allowed;
}


bool SSPInfo::isBlisExchangeAllowed() const
{
   return m_blisExchangeAllowed;
}


void SSPInfo::setActive(bool active)
{
   m_active = active;
}


bool SSPInfo::isActive() const
{
   return m_active;
}


void SSPInfo::setTMax(int tmax)
{
   m_tmax = tmax;
}


int SSPInfo::getTMax() const
{
   return m_tmax;
}

void SSPInfo::setUseFloorInBidHash(bool value)
{
   m_useFloorInBidHash = value;
}

bool SSPInfo::isSetFloorInBidHash() const
{
   return m_useFloorInBidHash;
}
//
//  This function is required for the unit tests.
//
void SSPInfo::reset()
{
   switch (m_id)
   {
   case SSP_SMAATO_HTML:
      setNoBidHttpCode(BLIS_HTTP_NO_CONTENT);
      setNoBidResponseBody("");
      setImpressionIsWin(false);
      setBlisExchangeAllowed(true);
      setActive(true);
      break;
      
   case SSP_ADX:
      setNoBidHttpCode(BLIS_HTTP_OK);
      setNoBidResponseBody(std::string("\x20\x01", 2));
      setImpressionIsWin(true);
      setBlisExchangeAllowed(false);
      setActive(true);
      break;
      
   case SSP_MOPUB_23:
      setNoBidHttpCode(BLIS_HTTP_NO_CONTENT);
      setNoBidResponseBody("");
      setImpressionIsWin(false);
      setBlisExchangeAllowed(false);
      setActive(true);
      break;
      
   case SSP_NEXAGE_21:
   case SSP_NEXAGE_23:
      setNoBidHttpCode(BLIS_HTTP_OK);
      setNoBidResponseBody("");
      setImpressionIsWin(false);
      setBlisExchangeAllowed(true);
      setActive(true);
      break;
      
   case SSP_RUBICON_21:
   case SSP_INNERACTIVE:
      setNoBidHttpCode(BLIS_HTTP_NO_CONTENT);
      setNoBidResponseBody("");
      setImpressionIsWin(true);
      setBlisExchangeAllowed(true);
      setActive(true);
      break;
      
   case SSP_PUBMATIC_23:
      setNoBidHttpCode(BLIS_HTTP_NO_CONTENT);
      setNoBidResponseBody("");
      setImpressionIsWin(true);
      setBlisExchangeAllowed(false);
      setActive(true);
      break;
      
   case SSP_BIDSWITCH:
   case SSP_BIDSWITCH_ADAPTV:
   case SSP_BIDSWITCH_LIVERAIL:
   case SSP_BIDSWITCH_OPENX:
   case SSP_BIDSWITCH_PUBMATIC:
   case SSP_BIDSWITCH_PULSEPOINT:
   case SSP_BIDSWITCH_AOL:
   case SSP_BIDSWITCH_BRIGHTROLL:
   case SSP_BIDSWITCH_INNERACTIVE:
   case SSP_BIDSWITCH_OPERA:
   case SSP_BIDSWITCH_SPOTXCHANGE:
   case SSP_BIDSWITCH_UNRULY:
   case SSP_BIDSWITCH_YUME:
   case SSP_BIDSWITCH_AERSERV:
   case SSP_BIDSWITCH_MOBFOX:
   case SSP_BIDSWITCH_ADFORM:
   case SSP_BIDSWITCH_FALK_TECHNOLOGIES:
   case SSP_BIDSWITCH_MADS:
   case SSP_BIDSWITCH_RHYTHMONE:
   case SSP_BIDSWITCH_YIELDLAB:
   case SSP_BIDSWITCH_AMOBEE:
   case SSP_BIDSWITCH_TEADS:
   case SSP_BIDSWITCH_COULL:
   case SSP_BIDSWITCH_TREMOR_VIDEO:
   case SSP_BIDSWITCH_VDOPIA:
   case SSP_BIDSWITCH_AXONIX:
   case SSP_BIDSWITCH_SWITCHCONCEPTS:
   case SSP_BIDSWITCH_VRTCAL:
   case SSP_BIDSWITCH_POWERLINKS:
   case SSP_BIDSWITCH_LKQD:
   case SSP_BIDSWITCH_KIOSKED:
   case SSP_BIDSWITCH_STICKYADS_TV:
   case SSP_BIDSWITCH_SMARTADSERVER:
   case SSP_BIDSWITCH_OPTIMATIC:
      setNoBidHttpCode(BLIS_HTTP_NO_CONTENT);
      setNoBidResponseBody("");
      setImpressionIsWin(false);
      setSeatId("86");
      setBlisExchangeAllowed(false);
      setActive(true);
      break;
      
   default:
      // Best guesses.
      setNoBidHttpCode(BLIS_HTTP_INTERNAL_SERVER_ERROR);
      setNoBidResponseBody("");
      setImpressionIsWin(true);
      setBlisExchangeAllowed(false);
      setActive(false);
   }
}
