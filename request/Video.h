#pragma once

#include <stdexcept>
#include <vector>
#include "VersionInfo.h"

namespace blis
{

class Video
{
public:
   // This should be in BlisTypes.h
   enum VideoSkipOptions
   {
      BLIS_SKIPPABLE_UNKNOWN = 0,
      BLIS_ALLOW_SKIPPABLE = 1,
      BLIS_REQUIRE_SKIPPABLE = 2,
      BLIS_BLOCK_SKIPPABLE = 3
   };

   enum VideoBidResponseProtocol
   {
      BLIS_UNDEFINED_VIDEO_PROTOCOL = 0,
      BLIS_VAST_1_0 = 1,
      BLIS_VAST_2_0 = 2,
      BLIS_VAST_3_0 = 3,
      BLIS_VAST_1_0_WRAPPER = 4,
      BLIS_VAST_2_0_WRAPPER = 5,
      BLIS_VAST_3_0_WRAPPER = 6
   }; 

   enum VideoPlaybackMethod
   {
      BLIS_UNKNOWN_PLAYBACK_METHOD = 0,
      BLIS_AUTO_PLAY_SOUND_ON = 1,
      BLIS_AUTO_PLAY_SOUND_OFF = 2,
      BLIS_CLICK_TO_PLAY = 3,
      BLIS_MOUSE_OVER = 4
   };

   enum VideoPosition
   {
      BLIS_VIDEO_POSITION_UNKNOWN = -1,
      BLIS_VIDEO_PRE_ROLL = 0,
      BLIS_VIDEO_MID_ROLL = 1,
      BLIS_VIDEO_POST_ROLL = 2
   };

   enum VideoLinearity
   {
      BLIS_VIDEO_LINEARITY_UNKNOWN = -1,
      BLIS_VIDEO_LINEARITY_ANY = 0,
      BLIS_VIDEO_IN_STREAM = 1,
      BLIS_VIDEO_OVERLAY = 2
   };

   Video();
   void setSkipOptions(blis::Video::VideoSkipOptions options);
   
   void addAllowedResponseVASTProtocol(VideoBidResponseProtocol protocol);

   bool isAllowedResponseVASTProtocol(VideoBidResponseProtocol protocol) const;
   bool isVersionAllowed(const VersionInfo& version) const;
   bool isProtocolInfoAvailable() const;

   void addPlaybackMethod(VideoPlaybackMethod method);

   void setStartDelay(unsigned int startDelay) 
   { 
      m_start_delay = startDelay; 
   }

   void setVideoPosition(VideoPosition position)
   {
      m_position = position;
   }

   void setLinearity(VideoLinearity linearity)
   {
      m_linearity = linearity;
   }

   bool isPortrait() const
   {
      return (m_width < m_height);
   }

   bool isLandscape() const
   {
      return (m_width > m_height);
   }
   
   unsigned int m_min_duration;
   unsigned int m_max_duration;
   unsigned int m_max_skippable_duration;
   int m_start_delay;
   VideoSkipOptions m_skip_options;
   uint8_t m_allowedVASTProtocolsBitSet;

   unsigned int m_width; // Width of video player
   unsigned int m_height; // Height of video player
   std::vector<VideoPlaybackMethod> m_playbackMethods;   
   VideoPosition m_position;
   unsigned int m_minBitRate;
   unsigned int m_maxBitRate;
   VideoLinearity m_linearity;
   bool m_rewarded;
};

}  // namespace blis

