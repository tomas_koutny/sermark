#pragma once

#include <boost/regex.hpp>

namespace util
{
inline void getDomainFromURI(const std::string& url, std::string& domain)
{
   if (url.empty())
   {
      return;
   }

   static const boost::regex re("^(?:[^/]+://)?([^:/]+)[/:]?");
   boost::smatch what;

   if (boost::regex_search(url, what, re) && (what.size() == 2))
   {
      domain = what[1];
   }
}
}

