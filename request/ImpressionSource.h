#pragma once

#include "Url.h"
#include "IABCategory.h"

class ImpressionSource
{
public:
   void AddIABCategory(const IABCategory& category)
   {
      m_iab_categories.insert(category);
   }
   
   const std::set<IABCategory>& GetIABCategories() const
   {
      return m_iab_categories;
   }
   
   void GetCategoriesAsString(std::string& out) const
   {
      if (m_iab_categories.empty())
      {
         return;  // Nothing to do.
      }
      
      bool added = false;
      for (const auto& cat : m_iab_categories)
      {
         out.append(cat.toString());
         out.append(",");
         added = true;
      }
      
      // TODO: c++11 supports std::string::pop_back
      
      if (added)
      {
         // Remove the trailing ","
         out.resize(out.size() - 1);
      }
   }

   std::set<IABCategory> m_iab_categories;
};


/**
 * @ingroup BidRequestMembers_Group
 * @brief Represents a site within the bid request <br>
 */
class Site : public ImpressionSource
{
public:
   enum MobileOptimized
   {
      MOBILE_OPTIMIZED_UNKNOWN,
      MOBILE_OPTIMIZED_YES,
      MOBILE_OPTIMIZED_NO
   };

   std::string m_site_id;
   std::string m_site_name;  // Name of the site
   std::string m_domain_name; // Domain name of the site
   std::string m_publisherId; // Exchange Publisher ID
   std::string m_publisher_name; // Site's publisher name
   std::string m_publisher_domain; // Site's publisher domain
   std::string m_extracted_domain; // Domain extracted from the URL in m_domain_name
   std::string m_pageURL; // URL of the page where the impression will be shown
   std::string m_refURL; // URL of the page that caused navigation to the page
   MobileOptimized m_mobileOptimized = MOBILE_OPTIMIZED_UNKNOWN;
   
   void fixFields()
   {
      blis::untab(m_domain_name);
      blis::trim(m_domain_name);

      blis::untab(m_pageURL);
      blis::trim(m_pageURL);

      blis::untab(m_refURL);
      blis::trim(m_refURL);
   }
   
   void extractDomain()
   {
      std::string& domain = m_domain_name.empty() ? m_pageURL : m_domain_name;
      util::getDomainFromURI(domain, m_extracted_domain);
   }
};

/**
 * @ingroup BidRequestMembers_Group
 * @brief Represents an app within the bid request <br>
 *
 * @detailed Holds information about the app that is initiating the request for ad
 * through the SSP. Note that a bid request can only either Site or App object
 */
class App : public ImpressionSource
{
public:
   std::string m_app_id;
   std::string m_app_name; // Name of the app, or it's SSP assigned identifier.
   std::string m_app_domain_name; // Domain name of the app
   std::string m_publisherId; // Exchange Publisher ID
   std::string m_publisher_name; // App's publisher name
   std::string m_publisher_domain_name; // App's publisher domain
   std::string m_nexage_sdk_version; // Nexage SDK version used by the app
   std::string m_bundle;   // App's Android package name or iTunes ID.
   std::string m_appStoreURL;
   std::string m_version; 
   unsigned int m_paid; // 1 if paid, 0 otherwise

   void fixFields()
   {
      blis::untab(m_app_name);
      blis::trim(m_app_name);
      
      blis::untab(m_bundle);
      blis::trim(m_bundle);
   }
   
   void extractBundle()
   {      
      static std::pair<const char*, const char*> delimiters[] = {
         std::make_pair("/id", "?"),   // https://itunes.apple.com/us/app/accuweather-weather-for-life/id300048137?mt=8
         std::make_pair("id=", "&")    // https://play.google.com/store/apps/details?id=com.company.app&hl=en_GB
      };
      
      // Don't overwrite the bundle if we've already read something.
      if (!m_bundle.empty() || m_appStoreURL.empty())
      {
         return;
      }

      for (const auto& delimiter : delimiters)
      {
         const char* header = delimiter.first;
         const char* footer = delimiter.second;
         
         std::size_t start = m_appStoreURL.find(header);
         if (start != std::string::npos)
         {
            start += strlen(header);
            std::size_t end = m_appStoreURL.find(footer, start);
            std::size_t length = (end == std::string::npos ? end : end - start);
            
            m_bundle = m_appStoreURL.substr(start, length);
            return;
         }
      }
   }
};

