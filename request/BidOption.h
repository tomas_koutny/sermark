#pragma once

#include <vector>
#include <boost/serialization/access.hpp>

#include "BlisTypes.h"
#include "UnownedPtr.h"
#include "PMP.h"

class PMPDealParsedParams
{
public:

   PMPDealParsedParams& setBidFloor(float bidFloor)
   {
      m_bidFloor = bidFloor;
      return *this;
   }

   PMPDealParsedParams& setPMPDealId(std::string&& pmpDealId)
   {
      m_pmpDealId = std::move(pmpDealId);
      return *this;
   }

   PMPDealParsedParams& setSSPId(SSPId sspId)
   {
      m_sspId = sspId;
      return *this;
   }

   PMPDealParsedParams& setAuctionType(BlisAuctionType auctionType)
   {
      m_auctionType = auctionType;
      return *this;
   }

   PMPDealParsedParams& addSeatId(std::string&& seatId)
   {
      m_seatIds.push_back(std::move(seatId));
      return *this;
   }

   bool isValid() const
   {
      return ((m_sspId != SSP_UNKNOWN) && !m_pmpDealId.empty());
   }

   float getBidFloor() const
   {
      return m_bidFloor;
   }

   const std::string& getPMPDealId() const
   {
      return m_pmpDealId;
   }

   SSPId getSSPId() const
   {
      return m_sspId;
   }

   BlisAuctionType getAuctionType() const
   {
      return m_auctionType;
   }

   const std::vector<std::string>& getSeatIds() const
   {
      return m_seatIds;
   }

   bool hasSeatIds() const
   {
      return !m_seatIds.empty();
   }

private:

   std::string m_pmpDealId;
   SSPId m_sspId = SSP_UNKNOWN;
   std::vector<std::string> m_seatIds;

   float m_bidFloor = 0.0f;
   BlisAuctionType m_auctionType = BLIS_AUCTION_TYPE_UNKNOWN;
};

/**
 * Represents one of impression's bid option. It can be either open market
 * or private deal bid option.
 *
 * It is usually constructed via Builder object.
 */
class BidOption
{
public:

   enum class Type
   {
      OMP,
      PMP
   };

   /**
    * Tries to build a BidOption object from passed PMPDealParsedParams object.
    * Caller is encouraged to check returned pointer for validity before
    * dereferencing it.
    * @post Passed PMPDealParsedParams object is in valid destructible, but
    * unspecified state. No further work should be done with it.
    * @return std::unique_ptr to newly constructed BidOption object in the
    * case of success, otherwise empty std::unique_ptr.
    */
   static std::unique_ptr<BidOption> buildPMPBidOption(const PMPDealParsedParams&);

   static std::unique_ptr<BidOption> buildOMPBidOption(float bidFloor, const std::string& dspSeatId = "");

   /**
    * Checks whether this BidOption object represents an open market bid
    * option.
    * @return true when BidOption is open market, false otherwise.
    */
   bool isOMP() const
   {
      return m_type == Type::OMP;
   }

   /**
    * Checks whether this BidOption object represents a PMP deal bid
    * option.
    * @return true when BidOption is PMP deal, false otherwise.
    */
   bool isPMP() const
   {
      return m_type == Type::PMP;
   }

   /**
    * Returns bid floor applicable for this BidOption object.
    * @return float bid floor.
    */
   float getBidFloor() const
   {
      return m_bidFloor;
   }

   /**
    * Checks whether this BidOption object has any seat related availability
    * restrictions.
    * @return true for for private deal BOs with seat targeting, false
    * otherwise. Implies that for open market BidOption, this function always
    * returns false.
    */
   bool hasSeats() const
   {
      return !m_eligibleSeats.empty();
   }

   /**
    * @return Reference to std::vector of pointers to Seat objects. This vector
    * will be always empty for open market BidOption object.
    */
   const std::vector<PMPSeat>& getEligibleSeats() const
   {
      return m_eligibleSeats;
   }

   /**
    * @return Reference to deal ID string, which will be empty in the case of
    * open market BidOption object.
    */
   const std::string& getPMPDealId() const
   {
      switch (m_type)
      {
      case Type::PMP:
         return m_pmpDeal.getId();
      case Type::OMP:
         {
            static const std::string empty("");
            return empty;
         }
      }

      throw std::runtime_error("BidOption::getPMPDealId wrong enumerator");
   }

   /**
    * Note that there is no point in calling this function on open market type
    * BidOption as there is nothinhg like DSP / Exchange targeting for this
    * type of BidOption.
    */
   bool isExchangeTargeted() const
   {
      switch (m_type)
      {
      case Type::PMP:
         return m_pmpDeal.isExchangeTargeted();
      case Type::OMP:
         return true;
      }

      throw std::runtime_error("BidOption::isExchangeTargeted wrong enumerator");
   }

   /**
    * Note that there is no point in calling this function on open market type
    * BidOption as there is nothinhg like DSP / Exchange targeting for this
    * type of BidOption.
    */
   bool isDSPTargeted() const
   {
      switch (m_type)
      {
      case Type::PMP:
         return m_pmpDeal.isDSPTargeted();
      case Type::OMP:
         return true;
      }

      throw std::runtime_error("BidOption::isDSPTargeted wrong enumerator");
   }

   /**
    * Checks whether given BO is always guaranteed to win (i.e. PMPG deal).
    */
   bool isGuaranteedToWin() const
   {
      switch (m_type)
      {
      case Type::PMP:
         return m_pmpDeal.isGuaranteedToWin();
      case Type::OMP:
         return false;
      }

      throw std::runtime_error("BidOption::isGuaranteedToWin wrong enumerator");
   }

   /**
    * Checks whether given BO's bid floor represents fixed price,
    * i.e. potential bid will not compete in price based auction.
    * @return For private deal BO this function returns true if it works as
    * fixed price and false otherwise. For open market BO this function always
    * returns false.
    */
   bool hasFixedPrice() const
   {
      switch (m_type)
      {
      case Type::PMP:
         {
            if (m_auctionType != BLIS_AUCTION_TYPE_UNKNOWN)
            {
               return m_auctionType == BLIS_AUCTION_TYPE_FIXED_PRICE;
            }
            else
            {
               return m_pmpDeal.isSupposedlyFixedPrice();
            }
         }
      case Type::OMP:
         return m_auctionType == BLIS_AUCTION_TYPE_FIXED_PRICE;
      }

      throw std::runtime_error("BidOption::hasFixedPrice wrong enumerator");
   }

   void setBidFloor(float bidFloor)
   {
      m_bidFloor = bidFloor;
   }

   /**
    * OMP BidOption constructor
    * @param bidFloor
    */
   BidOption(float bidFloor, PMPSeat&& dspSeat)
   : m_type(Type::OMP),
     m_bidFloor(bidFloor),
     m_auctionType(BLIS_AUCTION_TYPE_UNKNOWN) // we don't use it with OM anyway
   {
      if (dspSeat.isValid())
      {
         m_eligibleSeats.push_back(std::move(dspSeat));
      }
   }

   /**
    * PMP BidOption constructor
    * @param bidFloor
    * @param auctionType
    * @param pmpDealId
    * @param pmpDeal
    * @param eligibleSeats
    */
   BidOption(
      float bidFloor,
      BlisAuctionType auctionType,
      PMPDeal&& pmpDeal,
      std::vector<PMPSeat>&& eligibleSeats
   )
   : m_type(Type::PMP),
     m_bidFloor(bidFloor),
     m_auctionType(auctionType),
     m_pmpDeal(std::move(pmpDeal)),
     m_eligibleSeats(std::move(eligibleSeats))
   {
      if (!m_pmpDeal.isValid())
      {
         throw std::runtime_error("BidOption::BidOption: Invalid PMPDeal passed for PMP BO construction.");
      }
   }

private:
   Type m_type;
   float m_bidFloor;
   BlisAuctionType m_auctionType;
   PMPDeal m_pmpDeal;
   std::vector<PMPSeat> m_eligibleSeats;
};
